﻿namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    public enum UnionLeaveReason : byte
    {
        SelfLeave = 1,
        Kicked = 2,
    }

    //--------------------------------------------------------------------------
}
