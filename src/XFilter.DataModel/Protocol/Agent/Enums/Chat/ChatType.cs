﻿namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    public enum ChatType : byte
    {
        /// <summary>
        /// General, visible to nearby players.
        /// </summary>
        Normal = 1,

        /// <summary>
        /// Private, different packet structure.
        /// </summary>
        Private = 2,

        /// <summary>
        /// Game master pink, visible to nearby players.
        /// </summary>
        NormalGm = 3,

        /// <summary>
        /// Party green.
        /// </summary>
        Party = 4,

        Guild = 5,

        /// <summary>
        /// Global yellow.
        /// </summary>
        Global = 6,

        /// <summary>
        /// Pink notice on top.
        /// </summary>
        Notice = 7,

        /// <summary>
        /// Stall chat.
        /// </summary>
        Stall = 9,

        /// <summary>
        /// Union chat.
        /// </summary>
        Union = 11,

        /// <summary>
        /// Only used for quest?.
        /// </summary>
        NPC = 13,

        /// <summary>
        /// Academy chat.
        /// </summary>
        Academy = 16,
    }

    //--------------------------------------------------------------------------
}
