﻿namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    public enum MovementAngleAction : byte
    {
        Obsolete = 0,
        Forward = 1,
    }

    //--------------------------------------------------------------------------
}
