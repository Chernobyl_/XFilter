﻿namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    public enum WeatherType : byte
    {
        Clear = 1,
        Rain = 2,
        Snow = 3,
    }

    //--------------------------------------------------------------------------
}
