﻿namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    public enum AuthErrorCode : byte
    {
        /// <summary>
        /// UIO_MSG_ERROR_SERVER_BUSY_CONNECT_IMPOSSIBILE
        /// The server is full, please try again later.
        /// </summary>
        ServerIsFull = 4,

        /// <summary>
        /// Cannot connect to the server because access to the current IP has exceeded its limit.
        /// </summary>
        IPLimit = 5,
    }

    //--------------------------------------------------------------------------
}
