﻿namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    public enum ObjectActionFlag : byte
    {
        Start = 1,
        Cancel = 2,
    }

    //--------------------------------------------------------------------------
}
