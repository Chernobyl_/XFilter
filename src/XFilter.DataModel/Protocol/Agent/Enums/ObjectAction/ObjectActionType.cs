﻿namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    public enum ObjectActionType : byte
    {
        NormalAttack = 1,
        ItemPick = 2,
        Trace = 3,
        UseSkill = 4,
    }

    //--------------------------------------------------------------------------
}
