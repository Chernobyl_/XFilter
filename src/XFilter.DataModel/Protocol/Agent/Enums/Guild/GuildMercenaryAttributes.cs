﻿using System;

namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    [Flags]
    public enum GuildMercenaryAttributes : byte
    {
        None = 0,
        Defense = 1,
        Attack = 2,
        Hit = 4,
        Health = 8,
        Tolerance = 12
    }

    //--------------------------------------------------------------------------
}
