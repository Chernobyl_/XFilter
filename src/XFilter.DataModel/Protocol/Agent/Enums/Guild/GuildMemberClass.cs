﻿namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    public enum GuildMemberClass : byte
    {
        Master = 0,
        ViceMaster = 5,
        Member = 10
    }

    //--------------------------------------------------------------------------
}
