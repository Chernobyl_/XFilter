﻿namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    public enum GuildMemberOnlineStatus : byte
    {
        Offline = 0,
        Online = 1,
    }

    //--------------------------------------------------------------------------
}
