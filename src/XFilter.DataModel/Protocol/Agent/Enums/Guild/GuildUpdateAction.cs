﻿namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    public enum GuildUpdateAction : byte
    {
        AddMember = 2,
        RemoveMemberById = 3,
        UpdateMember = 6,
        MasterUpdate = 5,
        UnionLeave = 18,
        RemoveAuthority = 20,
        AddAuthority = 22,
    }

    //--------------------------------------------------------------------------
}
