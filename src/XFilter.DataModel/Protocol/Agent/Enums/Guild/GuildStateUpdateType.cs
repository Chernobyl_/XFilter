﻿namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    public enum GuildStateUpdateType : byte
    {
        GuildPointsUpdate = 8,
        TitleAndNotice = 16,
    }

    //--------------------------------------------------------------------------
}
