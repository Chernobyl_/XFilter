﻿using System;

namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    [Flags]
    public enum GuildPermission : uint
    {
        None = 0,
        Join = 1,
        Withdraw = 2,
        Union = 4,
        Storage = 8,
        Notice = 16,
        All = 0xFFFFFFFF,
    }

    //--------------------------------------------------------------------------
}
