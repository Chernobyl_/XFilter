﻿using System;

namespace XFilter.DataModel.Protocol.Agent
{
    [Flags]
    public enum PartySettingsFlag : byte
    {
        None = 0,
        ExpAutoShare = 1,
        ItemAutoShare = 2,
        InviteWithoutMaster = 4,
    }
}
