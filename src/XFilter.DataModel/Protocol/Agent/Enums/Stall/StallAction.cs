﻿namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    public enum StallAction : byte
    {
        Leave = 1,
        Enter = 2,
        Buy = 3,
    }

    //--------------------------------------------------------------------------
}
