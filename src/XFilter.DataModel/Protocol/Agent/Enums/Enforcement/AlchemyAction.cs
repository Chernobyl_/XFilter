﻿namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    public enum AlchemyAction : byte
    {
        Cancel = 1,
        Fusing = 2,
    }

    //--------------------------------------------------------------------------
}
