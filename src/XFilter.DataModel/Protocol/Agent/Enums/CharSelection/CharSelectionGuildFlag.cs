﻿namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    public enum CharSelectionGuildFlag : byte
    {
        None = 0x00,
        Member = 0x01,
        Master = 0x02,
    }

    //--------------------------------------------------------------------------
}
