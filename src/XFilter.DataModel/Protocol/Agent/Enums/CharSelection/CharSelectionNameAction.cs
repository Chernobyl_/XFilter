namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    public enum CharSelectionNameAction : byte
    {
        CharRename = 1,
        GuildRename = 2,
        GuildNameCheck = 3,
    }

    //--------------------------------------------------------------------------
}