namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    public enum CharSelectionMemberClass : byte
    {
        Member = 1,
        Master = 2,
    }

    //--------------------------------------------------------------------------
}