namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    public enum CharSelectionAction : byte
    {
        Create = 1,
        List = 2,
        Delete = 3,
        CheckName = 4,
        Restore = 5
    }

    //--------------------------------------------------------------------------
}