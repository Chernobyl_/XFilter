﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_ENVIRONMENT_CELESTIAL_POSITION = 0x3020
    /// TESTME
    /// </summary>
    public class WorldCelestialPositionEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public uint CharUniqueID
        { get; set; }

        public ushort Moonphase
        { get; set; }

        public byte Hour
        { get; set; }

        public byte Minute
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

    
        public WorldCelestialPositionEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.CharUniqueID = packet.ReadValue<uint>();
            this.Moonphase = packet.ReadValue<ushort>();
            this.Hour = packet.ReadValue<byte>();
            this.Minute = packet.ReadValue<byte>();
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_ENVIRONMENT_CELESTIAL_POSITION);

            packet.WriteValue<uint>(this.CharUniqueID);
            packet.WriteValue<ushort>(this.Moonphase);
            packet.WriteValue<byte>(this.Hour);
            packet.WriteValue<byte>(this.Minute);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
