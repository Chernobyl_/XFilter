﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_ENVIRONMENT_CELESTIAL_UPDATE = 0x3027
    /// TESTME
    /// </summary>
    public class WorldCelestialUpdateEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public ushort MoonPhase
        { get; set; }

        public byte Hour
        { get; set; }

        public byte Minute
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public WorldCelestialUpdateEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.MoonPhase = packet.ReadValue<ushort>();
            this.Hour = packet.ReadValue<byte>();
            this.Minute = packet.ReadValue<byte>();

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_ENVIRONMENT_CELESTIAL_UPDATE);

            packet.WriteValue<ushort>(this.MoonPhase);
            packet.WriteValue<byte>(this.Hour);
            packet.WriteValue<byte>(this.Minute);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
