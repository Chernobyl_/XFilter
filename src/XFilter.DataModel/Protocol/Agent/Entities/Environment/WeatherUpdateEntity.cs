﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_ENVIRONMENT_WEATHER_UPDATE = 0x3809
    /// TESTME
    /// </summary>
    public class WeatherUpdateEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public WeatherType WeatherType
        { get; set; }

        public byte Intensity
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public WeatherUpdateEntity(WeatherType weatherType, byte intensity)
        {
            this.WeatherType = weatherType;
            this.Intensity = intensity;
        }

        public WeatherUpdateEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.WeatherType = packet.ReadEnum<WeatherType>();
            this.Intensity = packet.ReadValue<byte>();

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_ENVIRONMENT_WEATHER_UPDATE);

            packet.WriteEnum<WeatherType>(this.WeatherType);
            packet.WriteValue<byte>(this.Intensity);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
