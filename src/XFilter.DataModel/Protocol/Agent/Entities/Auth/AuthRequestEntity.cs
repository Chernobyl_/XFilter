﻿namespace XFilter.DataModel.Protocol.Agent
{
    using NLog;
    using System;
    using XFilter.Shared.Helpers;
    using XFilter.Shared.Network;

    /// <summary>
    /// C->S CLIENT_AGENT_AUTH_REQUEST = 0x6103
    /// TESTME
    /// </summary>
    public class AuthRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public uint Token
        { get; set; }

        public string Username
        { get; set; }

        public string Password
        { get; set; }

        public SrOperationType OperationType
        { get; set; }

        public byte[] MacBytes
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public AuthRequestEntity(
            uint token, string username, string password,
            SrOperationType operationType, byte[] mac)
        {
            if (mac.Length != 6)
                throw new ArgumentException("Mac address must contain 6 bytes.");

            this.Token = token;
            this.Username = username;
            this.Password = password;

            //TESTME
            this.OperationType = operationType + 1;
            this.MacBytes = mac;
        }

        public AuthRequestEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region Public propertes & wrapper

        public string GetMacString() => NetHelper.GetMacString(this.MacBytes);

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Token = packet.ReadValue<uint>();
            this.Username = packet.ReadValue<string>();
            this.Password = packet.ReadValue<string>();

            this.OperationType = packet.ReadEnum<SrOperationType>();

            this.MacBytes = new byte[6];
            //Mac address has 6 bytes, lol.
            for (int i = 0; i < 6; i++)
            {
                this.MacBytes[i] = packet.ReadValue<byte>();
            }
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_AUTH_REQUEST, true);
            packet.WriteValue<uint>(this.Token);
            packet.WriteValue<string>(this.Username);
            packet.WriteValue<string>(this.Password);
            packet.WriteEnum<SrOperationType>(this.OperationType);
            packet.WriteByteArray(this.MacBytes);
            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}