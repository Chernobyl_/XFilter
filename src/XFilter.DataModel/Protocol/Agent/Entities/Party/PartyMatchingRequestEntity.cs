﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_PARTY_MATCHING_REQUEST = 0x706C
    /// TESTME
    /// </summary>
    public class PartyMatchingRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & constants

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public byte PageIndex
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public PartyMatchingRequestEntity(byte pageIndex)
        {
            this.PageIndex = pageIndex;
        }

        public PartyMatchingRequestEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public bool ReadFromPacket(XPacket packet)
        {
            this.PageIndex = packet.ReadValue<byte>();

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_PARTY_MATCHING_REQUEST);

            packet.WriteValue<byte>(this.PageIndex);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
