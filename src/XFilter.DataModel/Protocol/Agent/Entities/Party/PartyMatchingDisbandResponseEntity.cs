﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_PARTY_MATCHING_DISBAND_RESPONSE = 0xB06B
    /// TESTME
    /// </summary>
    public class PartyMatchingDisbandResponseEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public PacketResultCode Code
        { get; private set; }

        public uint PartyId
        { get; private set; }

        public PartyErrorCode ErrorCode
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public PartyMatchingDisbandResponseEntity(uint partyId)
        {
            this.Code = PacketResultCode.Success;
            this.PartyId = partyId;
        }

        public PartyMatchingDisbandResponseEntity(PartyErrorCode errorCode)
        {
            this.Code = PacketResultCode.Error;
            this.ErrorCode = errorCode;
        }

        public PartyMatchingDisbandResponseEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Code = packet.ReadEnum<PacketResultCode>();

            if(this.Code == PacketResultCode.Success)
            {
                this.PartyId = packet.ReadValue<uint>();
                return true;
            }

            if(this.Code == PacketResultCode.Error)
            {
                this.ErrorCode = packet.ReadEnum<PartyErrorCode>();

                return true;
            }

            return false;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_PARTY_MATCHING_DISBAND_RESPONSE);

            packet.WriteEnum<PacketResultCode>(this.Code);

            if (this.Code == PacketResultCode.Error)
            {
                packet.WriteEnum<PartyErrorCode>(this.ErrorCode);
            }
            else packet.WriteValue<uint>(this.PartyId);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
