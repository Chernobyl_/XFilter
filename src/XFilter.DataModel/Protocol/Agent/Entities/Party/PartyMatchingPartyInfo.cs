﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_PARTY_MATCHING_PAGE_RESPONSE = 0xB06C
    /// TESTME
    /// </summary>
    public class PartyMatchingPartyInfo : IPacketInnerEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Pubic properties

        public uint PartyId
        { get; private set; }

        public uint LeaderId
        { get; private set; }

        public string LeaderName
        { get; private set; }

        //0
        public CharRace Race
        { get; private set; }

        public byte MemberCount
        { get; set; }

        public PartySettingsFlag Settings
        { get; set; }

        public PartyType PartyType
        { get; set; }

        public byte MinLevel
        { get; set; }

        public byte MaxLevel
        { get; set; }

        public string Title
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.PartyId = packet.ReadValue<uint>();
            this.LeaderId = packet.ReadValue<uint>();
            this.LeaderName = packet.ReadValue<string>();

            this.Race = packet.ReadEnum<CharRace>();

            this.MemberCount = packet.ReadValue<byte>();
            this.Settings = packet.ReadEnum<PartySettingsFlag>();
            this.PartyType = packet.ReadEnum<PartyType>();

            this.MinLevel = packet.ReadValue<byte>();
            this.MaxLevel = packet.ReadValue<byte>();
            this.Title = packet.ReadValue<string>();

            return true;
        }

        public bool AppendToPacket(XPacket packet)
        {
            packet.WriteValue<uint>(this.PartyId);
            packet.WriteValue<uint>(this.LeaderId);
            packet.WriteValue<string>(this.LeaderName);
            packet.WriteEnum<CharRace>(this.Race);
            packet.WriteValue<byte>(this.MemberCount);
            packet.WriteEnum<PartySettingsFlag>(this.Settings);
            packet.WriteEnum<PartyType>(this.PartyType);
            packet.WriteValue<byte>(this.MinLevel);
            packet.WriteValue<byte>(this.MaxLevel);
            packet.WriteValue<string>(this.Title);

            return true;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
