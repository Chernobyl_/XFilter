﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_PARTY_LEAVE_REQUEST = 0x7061
    /// TESTME
    /// </summary>
    public class PartyLeaveRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_PARTY_LEAVE_REQUEST);
            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
