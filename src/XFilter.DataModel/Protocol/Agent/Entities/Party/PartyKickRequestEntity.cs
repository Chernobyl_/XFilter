﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_PARTY_KICK_REQUEST = 0x7063
    /// TESTME
    /// </summary>
    public class PartyKickRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        /// <summary>
        /// NOTE: Used internally ? This is not the "index" or unique obj id.
        /// Mby just ActionIndex.
        /// </summary>
        public uint Index
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public PartyKickRequestEntity(uint index)
        {
            this.Index = index;
        }

        public PartyKickRequestEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Index = packet.ReadValue<uint>();
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_PARTY_KICK_REQUEST);

            packet.WriteValue<uint>(this.Index);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
