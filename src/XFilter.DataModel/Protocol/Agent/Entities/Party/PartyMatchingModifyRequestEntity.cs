﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_PARTY_MATCHING_MODIFY_REQUEST = 0x706A
    /// TESTME
    /// </summary>
    public class PartyMatchingModifyRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public PartyMatchingPartyModifyInfo PartyInfo
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors
        
        public PartyMatchingModifyRequestEntity(PartyMatchingPartyModifyInfo info)
        {
            this.PartyInfo = info;
        }

        public PartyMatchingModifyRequestEntity()
        {
            this.PartyInfo = new PartyMatchingPartyModifyInfo();
        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet) => this.PartyInfo.ReadFromPacket(packet);

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_PARTY_MATCHING_MODIFY_REQUEST);

            this.PartyInfo.AppendToPacket(packet);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
