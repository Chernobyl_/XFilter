﻿using NLog;
using System.Collections.Generic;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_PARTY_MATCHING_PAGE_RESPONSE = 0xB06C
    /// TESTME
    /// </summary>
    public class PartyMatchingPageEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private List<PartyMatchingPartyInfo> _parties;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties
            
        public PacketResultCode Code
        { get; private set; }

        public PartyErrorCode ErrorCode
        { get; private set; }

        //1
        public byte TotalPages
        { get; private set; }

        //0
        public byte CurrentPage
        { get; private set; }

        public byte PartyCount { get => (byte)_parties.Count; set { } }

  
        #endregion

        //--------------------------------------------------------------------------

        #region Constructors
        
        public PartyMatchingPageEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            _parties = new List<PartyMatchingPartyInfo>();
            this.Code = packet.ReadEnum<PacketResultCode>();

            if (this.Code == PacketResultCode.Success)
            {
                this.TotalPages = packet.ReadValue<byte>();
                this.CurrentPage = packet.ReadValue<byte>();

                //NOTE: Temporal var. We use => for calc of member count.
                var ptCount = packet.ReadValue<byte>();

                for (int i = 0; i < ptCount; i++)
                {
                    var item = new PartyMatchingPartyInfo();
                    item.ReadFromPacket(packet);
                    _parties.Add(item);
                }

                return true;
            }

            if(this.Code == PacketResultCode.Error)
            {
                this.ErrorCode = packet.ReadEnum<PartyErrorCode>();
                return true;
            }

            return false;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_PARTY_MATCHING_PAGE_RESPONSE);


            packet.WriteEnum<PacketResultCode>(this.Code);

            packet.WriteValue<byte>(this.TotalPages);
            packet.WriteValue<byte>(this.CurrentPage);

            packet.WriteValue<byte>(this.PartyCount);

            foreach (var item in _parties)
                item.AppendToPacket(packet);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
