﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    public class PartySettings : IPacketInnerEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public PartySettingsFlag Flag
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public PartySettings(PartySettingsFlag flag)
        {
            this.Flag = flag;
        }


        public PartySettings()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketInnerEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Flag = packet.ReadEnum<PartySettingsFlag>();
            return true;
        }

        public bool AppendToPacket(XPacket packet)
        {
            packet.WriteEnum<PartySettingsFlag>(this.Flag);
            return true;
        }


        #endregion

        //--------------------------------------------------------------------------
    }
}
