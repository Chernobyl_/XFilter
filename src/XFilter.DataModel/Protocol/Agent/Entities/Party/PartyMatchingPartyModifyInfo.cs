﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_PARTY_MATCHING_MODIFY_RESPONSE = 0xB06A
    /// </summary>
    public class PartyMatchingPartyModifyInfo : IPacketInnerEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public uint PartyMatchingId
        { get; private set; }

        public uint PartyId
        { get; private set; }

        public PartySettingsFlag Settings
        { get; set; }

        public PartyType PartyType
        { get; set; }

        public byte MinLevel
        { get; set; }

        public byte MaxLevel
        { get; set; }

        public string Title
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketInnerEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.PartyMatchingId = packet.ReadValue<uint>();
            this.PartyId = packet.ReadValue<uint>();
            this.Settings = packet.ReadEnum<PartySettingsFlag>();
            this.PartyType = packet.ReadEnum<PartyType>();
            this.MinLevel = packet.ReadValue<byte>();
            this.MaxLevel = packet.ReadValue<byte>();
            this.Title = packet.ReadValue<string>();

            return true;
        }

        public bool AppendToPacket(XPacket packet)
        {
            packet.WriteValue<uint>(this.PartyMatchingId);
            packet.WriteValue<uint>(this.PartyId);
            packet.WriteEnum<PartySettingsFlag>(this.Settings);
            packet.WriteEnum<PartyType>(this.PartyType);
            packet.WriteValue<byte>(this.MinLevel);
            packet.WriteValue<byte>(this.MaxLevel);
            packet.WriteValue<string>(this.Title);

            return true;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
