﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    public class PartyMember : IPacketInnerEntity
    {
        #region Public properties

        public uint MemberId
        { get; private set; }
        
        public string Name
        { get; private set; }

        public uint ObjectId
        { get; private set; }

        public byte Level
        { get; private set; }

        //4 / 4 bits
        public byte HealthAndMana
        { get; private set; }

        
        public GamePosition Position
        { get; private set; }

        public ushort WorldId
        { get; private set; }

        public ushort LayerId
        { get; private set; }

        public string GuildName
        { get; private set; }

        public uint MasteryId1
        { get; private set; }

        public uint MasteryId2
        { get; private set; }

        #endregion

        public bool ReadFromPacket(XPacket packet)
        {
            //byte updateType?
            this.MemberId = packet.ReadValue<uint>();
            this.Name = packet.ReadValue<string>();
            this.ObjectId = packet.ReadValue<uint>();
            this.Level = packet.ReadValue<byte>();
            this.HealthAndMana = packet.ReadValue<byte>();

            this.Position = new GamePosition();
            this.Position.ReadFromPacket(packet);

            this.WorldId = packet.ReadValue<ushort>();
            this.LayerId = packet.ReadValue<ushort>();
            this.GuildName = packet.ReadValue<string>();

            //Unknown, always 4
            packet.ReadValue<byte>();

            this.MasteryId1 = packet.ReadValue<uint>();
            this.MasteryId2 = packet.ReadValue<uint>();

            return true;
        }

        public bool AppendToPacket(XPacket packet)
        {
            packet.WriteValue<uint>(this.MemberId);
            packet.WriteValue<string>(this.Name);
            packet.WriteValue<uint>(this.ObjectId);
            packet.WriteValue<byte>(this.Level);
            packet.WriteValue<byte>(this.HealthAndMana);

            this.Position.AppendToPacket(packet);

            packet.WriteValue<ushort>(this.WorldId);
            packet.WriteValue<ushort>(this.LayerId);
            packet.WriteValue<string>(this.GuildName);

            packet.WriteValue<byte>(4); //unk yet

            packet.WriteValue<uint>(this.MasteryId1);
            packet.WriteValue<uint>(this.MasteryId2);

            return true;
        }
    }
}
