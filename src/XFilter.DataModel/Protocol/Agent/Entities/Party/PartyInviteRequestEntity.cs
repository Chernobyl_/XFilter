﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_PARTY_INVITE_REQUEST = 0x7062
    /// TESTME
    /// </summary>
    public class PartyInviteRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public uint TargetId
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.TargetId = packet.ReadValue<uint>();
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_PARTY_INVITE_REQUEST);
            packet.WriteValue<uint>(this.TargetId);
            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
