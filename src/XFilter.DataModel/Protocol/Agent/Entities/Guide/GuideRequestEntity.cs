﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_GUIDE_REQUEST = 0x70EA
    /// TESTME
    /// </summary>
    public class GuideRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties
        public GuideFlag GuideFlag
        { get; set; }


        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public GuideRequestEntity(GuideFlag flag)
        {
            this.GuideFlag = flag;
        }

        public GuideRequestEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.GuideFlag = packet.ReadEnum<GuideFlag>();

            return true;
        }


        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_GUIDE_REQUEST);

            packet.WriteEnum<GuideFlag>(this.GuideFlag);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
