﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_GUIDE_RESPONSE = 0xB0EA
    /// TESTME
    /// </summary>
    public class GuideResponseEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public bool Success
        { get; set; }

        public GuideFlag Flag
        { get; set; }

        public GuideErrorCode ErrorCode
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public GuideResponseEntity(GuideErrorCode errorCode)
        {
            this.Success = false;
            this.ErrorCode = errorCode;
        }

        public GuideResponseEntity(GuideFlag flag)
        {
            this.Success = true;
            this.Flag = flag;
        }

        public GuideResponseEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Success = packet.ReadValue<bool>();
            if (this.Success)
            {
                this.Flag = packet.ReadEnum<GuideFlag>();
            }
            else
            {
                this.Flag = GuideFlag.None;
                this.ErrorCode = packet.ReadEnum<GuideErrorCode>();
            }

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_GUIDE_RESPONSE);

            packet.WriteValue<bool>(this.Success);

            if (this.Success)
                packet.WriteEnum<GuideFlag>(this.Flag);
            else
                packet.WriteEnum<GuideErrorCode>(this.ErrorCode);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
