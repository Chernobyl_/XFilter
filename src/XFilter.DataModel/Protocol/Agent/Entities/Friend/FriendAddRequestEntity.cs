﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_FRIEND_ADD_REQUEST = 0x7302
    /// TESTME
    /// </summary>
    public class FriendAddRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public string Name
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public FriendAddRequestEntity(string name)
        {
            this.Name = name;
        }

        public FriendAddRequestEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Name = packet.ReadValue<string>();
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_FRIEND_ADD_REQUEST);

            packet.WriteValue<string>(this.Name);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
