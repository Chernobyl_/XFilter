﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C CLIENT_AGENT_FRIEND_ADD_REQUEST = 0x7302
    /// TESTME
    /// </summary>
    public class FriendAddRequestResponseEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public string Name
        { get; private set; }

        public uint Token
        { get; private set; }

        public uint Unk
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public FriendAddRequestResponseEntity(uint token, uint unk, string name)
        {
            this.Token = token;
            this.Unk = unk;
            this.Name = name;
        }

        public FriendAddRequestResponseEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Token = packet.ReadValue<uint>();
            this.Unk = packet.ReadValue<uint>();
            this.Name = packet.ReadValue<string>();
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_FRIEND_ADD_REQUEST_RESPONSE);

            packet.WriteValue<uint>(this.Token);
            packet.WriteValue<uint>(this.Unk);
            packet.WriteValue<string>(this.Name);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
