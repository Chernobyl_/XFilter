﻿using NLog;
using System;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_FRIEND_ADD_RESULT_RESPONSE = 0xB302
    /// TESTME
    /// </summary>
    public class FriendAddResultResponseEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public PacketResultCode Code
        { get; private set; }


        public FriendAddResultCode ResultCode
        { get; private set; }

        //Accepted & friend name only for 
        //FriendAddResultCode.UIIT_MSG_FRIENDERR_REFUSED_OR_ACCEPTED ??

        public bool Accepted
        { get; private set; }

  
        public string FriendName
        { get; private set; }


        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public FriendAddResultResponseEntity(FriendAddResultCode resultCode)
        {
            if (resultCode == FriendAddResultCode.UIIT_MSG_FRIENDERR_REFUSED_OR_ACCEPTED)
                throw new ArgumentException("Refused or accepted must be specified. Use overloaded ctor.");

            this.Code = PacketResultCode.Error;
            this.ResultCode = resultCode;
        }

        public FriendAddResultResponseEntity(bool accepted, string friendName)
        {
            this.Code = PacketResultCode.Error;
            this.ResultCode = FriendAddResultCode.UIIT_MSG_FRIENDERR_REFUSED_OR_ACCEPTED;
            this.Accepted = accepted;
            this.FriendName = friendName;
        }

        public FriendAddResultResponseEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Code = packet.ReadEnum<PacketResultCode>();

            if (this.Code == PacketResultCode.Error)
            {
                this.ResultCode = packet.ReadEnum<FriendAddResultCode>();

                if (this.ResultCode == FriendAddResultCode.UIIT_MSG_FRIENDERR_REFUSED_OR_ACCEPTED)
                {
                    this.Accepted = packet.ReadValue<bool>();
                    this.FriendName = packet.ReadValue<string>();
                }

                return true;
            }

            return false;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_FRIEND_ADD_RESULT_RESPONSE);

            packet.WriteEnum<PacketResultCode>(this.Code);

            if(this.Code == PacketResultCode.Error)
            {
                packet.WriteValue<FriendAddResultCode>(this.ResultCode);
                if(this.ResultCode == FriendAddResultCode.UIIT_MSG_FRIENDERR_REFUSED_OR_ACCEPTED)
                {
                    packet.WriteValue<bool>(this.Accepted);
                    packet.WriteValue<string>(this.FriendName);
                }
            }
            
            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
