﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_FRPVP_UPDATE_REQUEST = 0x7516
    /// TESTME
    /// </summary>
    public class FRPVPRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public FRPVPMode Mode
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public FRPVPRequestEntity(FRPVPMode mode)
        {
            this.Mode = mode;
        }

        public FRPVPRequestEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Mode = packet.ReadEnum<FRPVPMode>();

            return true;
        }


        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_FRPVP_UPDATE_REQUEST);

            packet.WriteEnum<FRPVPMode>(this.Mode);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
