﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    ///  C->S CLIENT_AGENT_ENFORCEMENT_REINFORCE_REQUEST  = 0x7150
    ///  S->C SERVER_AGENT_ENFORCEMENT_REINFORCE_RESPONSE = 0xB150
    ///  TESTME
    /// </summary>
    public class AlchemyMagParam : IPacketInnerEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties & statics

        public uint MagParamId
        { get; private set; }

        public uint MagParamValue
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketInnerEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.MagParamId = packet.ReadValue<uint>();
            this.MagParamValue = packet.ReadValue<uint>();
            return true;
        }

        public bool AppendToPacket(XPacket packet)
        {
            packet.WriteValue<uint>(this.MagParamId);
            packet.WriteValue<uint>(this.MagParamValue);
            return true;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
