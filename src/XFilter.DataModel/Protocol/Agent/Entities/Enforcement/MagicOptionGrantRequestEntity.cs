﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S  CLIENT_AGENT_ENFORCEMENT_MAGIC_OPTION_GRANT_REQUEST = 0x34A9
    /// TESTME
    /// </summary>
    public class MagicOptionGrantRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public byte InventorySlot
        { get; set; }

        public string CodeName128
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public MagicOptionGrantRequestEntity(byte slot, string codename)
        {
            this.InventorySlot = slot;
            this.CodeName128 = codename;
        }

        public MagicOptionGrantRequestEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.InventorySlot = packet.ReadValue<byte>();
            this.CodeName128 = packet.ReadValue<string>();
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_ENFORCEMENT_MAGIC_OPTION_GRANT_REQUEST);
            packet.WriteValue<byte>(this.InventorySlot);
            packet.WriteValue<string>(this.CodeName128);
            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
