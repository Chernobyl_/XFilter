﻿using NLog;
using System.Collections.Generic;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_ENFORCEMENT_REINFORCE_RESPONSE = 0xB150
    /// TESTME, INCOMPLETE
    /// </summary>
    public class AlchemyReinforceResponseEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private List<AlchemyMagParam> _magParams;

        private readonly static Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public PacketResultCode Code
        { get; private set; }

        public AlchemyAction Action
        { get; private set; }

        public bool Success
        { get; private set; }

        public byte Slot
        { get; private set; }

        public bool DestroyItem
        { get; private set; }

        public uint Unk
        { get; private set; }

        public uint ItemID
        { get; private set; }

        public byte Plus
        { get; private set; }

        public ulong Variance
        { get; private set; }

        public uint Durability
        { get; private set; }

        public uint UnkLast4Bytes
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public AlchemyReinforceResponseEntity()
        {

        }
        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            _magParams = new List<AlchemyMagParam>();

            this.Code = packet.ReadEnum<PacketResultCode>();

            //???
            if (this.Code == PacketResultCode.Error)
            {
                Logger.Error("Alchemy response error logic not yet implemented.");
                return false;
            }

            this.Action = packet.ReadEnum<AlchemyAction>();

            if (this.Action == AlchemyAction.Fusing)
            {
                this.Success = packet.ReadValue<bool>();
                this.Slot = packet.ReadValue<byte>();
                if (!this.Success)
                {
                    //TEMP, HACK, Something wrong if failure and using lucky powder
                    return false;
                    this.DestroyItem = packet.ReadValue<bool>();
   
                }

                this.Unk = packet.ReadValue<uint>();
                this.ItemID = packet.ReadValue<uint>();
                this.Plus = packet.ReadValue<byte>();
                this.Variance = packet.ReadValue<ulong>();
                this.Durability = packet.ReadValue<uint>();

                var magParamCount = packet.ReadValue<byte>();
                for (int i = 0; i < magParamCount; i++)
                {
                    var item = new AlchemyMagParam();
                    item.ReadFromPacket(packet);
                    _magParams.Add(item);
                }
            }
            //HACK: Temporal hack to prevent crash. Not implemented.
            else
            {
                Logger.Warn($"Alchemy action {this.Action} is known, but not implemented.");
                return false;

            }
            this.UnkLast4Bytes = packet.ReadValue<uint>();
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_ALCHEMY_REINFORCE_RESPONSE);

            packet.WriteEnum<PacketResultCode>(this.Code);

            packet.WriteEnum<AlchemyAction>(this.Action);

            if(this.Action == AlchemyAction.Fusing)
            {
                packet.WriteValue<bool>(this.Success);
                packet.WriteValue<byte>(this.Slot);
                if (!this.Success)
                {
                    packet.WriteValue<bool>(this.DestroyItem);
                    return packet;
                }

                packet.WriteValue<uint>(this.Unk);

                packet.WriteValue<uint>(this.ItemID);
                packet.WriteValue<byte>(this.Plus);
                packet.WriteValue<ulong>(this.Variance);
                packet.WriteValue<uint>(this.Durability);

                packet.WriteValue<byte>((byte)_magParams.Count);
                foreach (var magParam in _magParams)
                    magParam.AppendToPacket(packet);
            }

            packet.WriteValue<uint>(this.UnkLast4Bytes);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
