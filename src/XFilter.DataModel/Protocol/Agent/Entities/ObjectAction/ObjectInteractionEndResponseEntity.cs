﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_OBJECT_INTERACTION_END_RESPONSE = 0xB04B
    /// TESTME, INCOMPLET ERROR CODES
    /// </summary>
    public class ObjectInteractionEndResponseEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public PacketResultCode Code
        { get; private set; }

        public ObjectInteractionEndErrorCode ErrorCode
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public ObjectInteractionEndResponseEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Code = packet.ReadEnum<PacketResultCode>();

            if (this.Code == PacketResultCode.Success)
                return true;

            if(this.Code == PacketResultCode.Error)
            {
                this.ErrorCode = packet.ReadEnum<ObjectInteractionEndErrorCode>();
                return true;
            }

            //???
            return false;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_OBJECT_INTERACTION_END_RESPONSE);

            packet.WriteEnum<PacketResultCode>(this.Code);

            if (this.Code == PacketResultCode.Error)
                packet.WriteEnum<ObjectInteractionEndErrorCode>(this.ErrorCode);
            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
