﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_OBJECT_INTERACTION_END_REQUEST = 0x704B
    /// TESTME
    /// </summary>
    public class ObjectInteractonEndRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public uint UniqueId
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public ObjectInteractonEndRequestEntity(uint uniqueId)
        {
            this.UniqueId = uniqueId;
        }

        public ObjectInteractonEndRequestEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.UniqueId = packet.ReadValue<uint>();

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_OBJECT_INTERACTION_END_REQUEST);

            packet.WriteValue<uint>(this.UniqueId);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
