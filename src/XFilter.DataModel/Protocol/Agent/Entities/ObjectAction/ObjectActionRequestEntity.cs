﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_OBJECT_ACTION_REQUEST = 0x7074
    /// TESTME
    /// </summary>
    public class ObjectActionRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        /// <summary>
        /// Start or cancel.
        /// </summary>
        public ObjectActionFlag ActionFlag
        { get; private set; }

        public ObjectActionType ActionType
        { get; private set; }

        public bool Success
        { get; private set; }

        public uint TargetUniqueId
        { get; private set; }

        public uint SkillId
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.ActionFlag = packet.ReadEnum<ObjectActionFlag>();

            if (this.ActionFlag == ObjectActionFlag.Cancel)
                return false;

            this.ActionType = packet.ReadEnum<ObjectActionType>();

            
            //UseSkill has same structure except 4 bytes for skill id in front of target
            //unique id.
            if (this.ActionType == ObjectActionType.UseSkill)
                this.SkillId = packet.ReadValue<uint>();

            if (this.ActionType == ObjectActionType.NormalAttack ||
               this.ActionType == ObjectActionType.ItemPick || 
               this.ActionType == ObjectActionType.Trace || 
               this.ActionType == ObjectActionType.UseSkill)
            {
                this.Success = packet.ReadValue<bool>();
                if (this.Success)
                    this.TargetUniqueId = packet.ReadValue<uint>();

                //Logger.Warn($"Target Unique ID: {this.TargetUniqueId}.");

                return true;
            }

            //Not fully parsed
            return false;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_OBJECT_ACTION_REQUEST);

            packet.WriteEnum<ObjectActionFlag>(this.ActionFlag);
            packet.WriteEnum<ObjectActionType>(this.ActionType);

            //UseSkill has same structure except 4 bytes for skill id in front of target
            //unique id.
            if (this.ActionType == ObjectActionType.UseSkill)
                packet.WriteValue<uint>(this.SkillId);

            if (this.ActionType == ObjectActionType.NormalAttack ||
             this.ActionType == ObjectActionType.ItemPick ||
             this.ActionType == ObjectActionType.Trace ||
             this.ActionType == ObjectActionType.UseSkill)
            {
                packet.WriteValue<bool>(this.Success);
                if(this.Success)
                    packet.WriteValue<uint>(this.TargetUniqueId);
            }

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
