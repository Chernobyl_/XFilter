﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_UNION_INFO_RESPONSE = 0x3102
    /// TESTME
    /// </summary>
    public class UnionMember : IPacketInnerEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public uint ID
        { get; set; }

        public string Name
        { get; set; }

        public byte Level
        { get; set; }

        public string GuildLeader
        { get; set; }
        
        public byte Capacity
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public UnionMember()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketInnerEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.ID = packet.ReadValue<uint>();
            this.Name = packet.ReadValue<string>();
            this.Level = packet.ReadValue<byte>();
            this.GuildLeader = packet.ReadValue<string>();
            this.Capacity = packet.ReadValue<byte>();
            return true;
        }

        public bool AppendToPacket(XPacket packet)
        {
            packet.WriteValue<uint>(this.ID);
            packet.WriteValue<string>(this.Name);
            packet.WriteValue<byte>(this.Level);
            packet.WriteValue<string>(this.GuildLeader);
            packet.WriteValue<byte>(this.Capacity);
            return true;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
