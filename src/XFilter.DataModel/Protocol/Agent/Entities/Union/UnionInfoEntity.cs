﻿using System.Collections.Generic;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_UNION_INFO_RESPONSE = 0x3102
    /// TESTME
    /// </summary>
    public class UnionInfoEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public uint ID
        { get; set; }

        public uint CurCrestRev
        { get; set; }

        public uint LeaderGuildID
        { get; set; }

        public List<UnionMember> Members
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public UnionInfoEntity()
        {
            this.Members = new List<UnionMember>();
        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.ID = packet.ReadValue<uint>();
            this.CurCrestRev = packet.ReadValue<uint>();
            this.LeaderGuildID = packet.ReadValue<uint>();

            var count = packet.ReadValue<byte>();

            for (int i = 0; i < count; i++)
            {
                var member = new UnionMember();
                member.ReadFromPacket(packet);

                this.Members.Add(member);
            }

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_UNION_INFO_RESPONSE);
            packet.WriteValue<uint>(this.ID);
            packet.WriteValue<uint>(this.CurCrestRev);
            packet.WriteValue<uint>(this.LeaderGuildID);
            packet.WriteValue<byte>((byte)this.Members.Count);

            foreach (var member in this.Members)
                member.AppendToPacket(packet);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
