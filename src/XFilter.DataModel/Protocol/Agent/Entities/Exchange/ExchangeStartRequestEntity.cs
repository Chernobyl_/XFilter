﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_EXCHANGE_START = 0x7081
    /// TESTME
    /// </summary>
    public class ExchangeStartRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public uint UniqueSenderId
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public ExchangeStartRequestEntity(uint uniqueSenderId)
        {
            this.UniqueSenderId = uniqueSenderId;
        }

        public ExchangeStartRequestEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.UniqueSenderId = packet.ReadValue<uint>();
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_EXCHANGE_START);

            packet.WriteValue<uint>(this.UniqueSenderId);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
