﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_MOVEMENT_DESIGNATE_AS_RECALL_POINT_REQUEST = 0x7059
    /// TESTME
    /// </summary>
    public class DesignateRecallPointRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public uint TeleportId
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public DesignateRecallPointRequestEntity(uint teleportId)
        {
            this.TeleportId = teleportId;
        }

        public DesignateRecallPointRequestEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.TeleportId = packet.ReadValue<uint>();
            return true;
        }


        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_MOVEMENT_DESIGNATE_AS_RECALL_POINT_REQUEST);

            packet.WriteValue<uint>(this.TeleportId);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
