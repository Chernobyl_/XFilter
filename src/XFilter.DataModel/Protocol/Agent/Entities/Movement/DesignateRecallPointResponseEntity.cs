﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_MOVEMENT_DESIGNATE_AS_RECALL_POINT_RESPONSE = 0xB059
    /// TESTME
    /// </summary>
    public class DesignateRecallPointResponseEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public bool Success
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Success = packet.ReadValue<bool>();
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_MOVEMENT_DESIGNATE_AS_RECALL_POINT_RESPONSE);

            packet.WriteValue<bool>(this.Success);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
