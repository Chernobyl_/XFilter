﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_MOVEMENT_TELEPORT_REQUEST = 0x705A
    /// TESTME, INCOMPLETE (only normal tps supported atm).
    /// </summary>
    public class TeleportRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public uint SourceUniqueId
        { get; private set; }

        public TeleportType Type
        { get; private set; }

        /// <summary>
        /// Not the unique id. This comes from ref data/db.
        /// </summary>
        public uint TargetRefId
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public TeleportRequestEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.SourceUniqueId = packet.ReadValue<uint>();
            this.Type = packet.ReadEnum<TeleportType>();

            switch(this.Type)
            {
                case TeleportType.Normal:
                    {
                        this.TargetRefId = packet.ReadValue<uint>();
                    }
                    break;
            }

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_MOVEMENT_TELEPORT_REQUEST);
            packet.WriteValue<uint>(this.SourceUniqueId);
            packet.WriteEnum<TeleportType>(this.Type);

            switch(this.Type)
            {
                case TeleportType.Normal:
                    {
                        packet.WriteValue<uint>(this.TargetRefId);
                    }
                    break;
            }

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
