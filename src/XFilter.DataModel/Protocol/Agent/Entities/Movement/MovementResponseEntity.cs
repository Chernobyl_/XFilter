﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_MOVEMENT_RESPONSE = 0xB021
    /// TESTME
    /// </summary>
    public class MovementResponseEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties - dest/src related

        public uint TargetID
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties - dest pos

        public bool HasDestination
        { get; set; }

        public GamePosition DestPos
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties - source pos

        public bool HasSource
        { get; set; }

        public GamePosition SrcPos
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties - angle operations

        public MovementAngleAction AngleAction
        { get; set; }

        public ushort Angle
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public MovementResponseEntity(uint targetId, GamePosition destPos, GamePosition srcPos)
        {
            this.HasDestination = true;
            this.TargetID = targetId;
            this.DestPos = destPos;
            this.SrcPos = srcPos;
        }

        public MovementResponseEntity(MovementAngleAction angleAction, ushort angle)
        {
            this.HasDestination = false;
            this.AngleAction = angleAction;
            this.Angle = angle;
        }

        public MovementResponseEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.TargetID = packet.ReadValue<uint>();
            this.HasDestination = packet.ReadValue<bool>();


            if (this.HasDestination)
            {
                //Has destination
                this.DestPos = new GamePosition();
                this.DestPos.ReadFromPacket(packet);
            }
            else
            {
                //No destination
                this.AngleAction = packet.ReadEnum<MovementAngleAction>();
                this.Angle = packet.ReadValue<ushort>();
            }

            this.HasSource = packet.ReadValue<bool>();

            if (this.HasSource)
            {
                var regionId = packet.ReadValue<ushort>();

                float x, y, z;
                if (regionId < short.MaxValue)
                {
                    //Normal world
                    x = packet.ReadValue<short>() / 10;
                    y = packet.ReadValue<float>();
                    z = packet.ReadValue<short>() / 10;
                }
                else
                {
                    //Dungeon
                    x = packet.ReadValue<int>() / 10;
                    y = packet.ReadValue<float>();
                    z = packet.ReadValue<int>() / 10;
                }

                this.SrcPos = new GamePosition(regionId, x, y, z);
            }

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_MOVEMENT_RESPONSE);
            packet.WriteValue<uint>(this.TargetID);

            packet.WriteValue<bool>(this.HasDestination);

            if(this.HasDestination)
            {
                this.DestPos.AppendToPacket(packet);
            }
            else
            {
                packet.WriteEnum<MovementAngleAction>(this.AngleAction);
                packet.WriteValue<ushort>(this.Angle);
            }

            packet.WriteValue<bool>(this.HasSource);

            if(this.HasSource)
            {
                packet.WriteValue<ushort>(this.SrcPos.RegionID);

                if (this.SrcPos.RegionID < short.MaxValue)
                {
                    //Normal world
                    packet.WriteValue<short>(this.SrcPos.X * 10);
                    packet.WriteValue<float>(this.SrcPos.Y);
                    packet.WriteValue<short>(this.SrcPos.Z * 10);
                }
                else
                {
                    //Dungeon
                    packet.WriteValue<int>(this.SrcPos.X * 10);
                    packet.WriteValue<float>(this.SrcPos.Y);
                    packet.WriteValue<int>(this.SrcPos.Z * 10);
                }
            }

            return packet;
        }


        public void ChangeDestination(GamePosition destPos)
        {
            this.HasDestination = true;
            this.DestPos = destPos;
        }

        public void ChangeDestAndSrc(GamePosition destPos, GamePosition srcPos)
        {
            this.HasDestination = true;
            this.HasSource = true;

            this.DestPos = destPos;
            this.SrcPos = srcPos;
        }

        public void ChangeAngle(MovementAngleAction angleAction, ushort angle)
        {
            this.HasDestination = false;
            this.HasSource = true;
            this.AngleAction = angleAction;
            this.Angle = angle;
        }


        #endregion

        //--------------------------------------------------------------------------
    }
}
