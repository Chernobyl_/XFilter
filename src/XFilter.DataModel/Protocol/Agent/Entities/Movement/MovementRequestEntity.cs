﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S SERVER_AGENT_MOVEMENT_REQUEST = 0x7021
    /// TESTME
    /// </summary>
    public class MovementRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties - move to coordinates (has destination)

        public bool HasDestination
        { get; private set; }

        public GamePosition DestPos
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties - angle operations

        public MovementAngleAction AngleAction
        { get; private set; }

        public ushort Angle
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public MovementRequestEntity(GamePosition target)
        {
            this.HasDestination = true;
            this.DestPos = target;
        }

        public MovementRequestEntity(MovementAngleAction angleAction, GamePosition destPos)
        {
            this.HasDestination = false;
            this.DestPos = destPos;
        }


        public MovementRequestEntity()
        {

        }


        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.HasDestination = packet.ReadValue<bool>();

            //Has destination
            if (this.HasDestination)
            {
                this.DestPos = new GamePosition();
                this.DestPos.ReadFromPacket(packet);
            }
            else
            {
                //No destination
                this.AngleAction = (MovementAngleAction)packet.ReadValue<byte>();
                this.Angle = packet.ReadValue<ushort>();
            }

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_MOVEMENT_REQUEST);

            packet.WriteValue<bool>(this.HasDestination);

            if (this.HasDestination)
            {
                this.DestPos.AppendToPacket(packet);
            }
            else
            {
                packet.WriteValue<byte>((byte)this.AngleAction);
                packet.WriteValue<ushort>(this.Angle);
            }

            return packet;
        }

        public void ChangeDestination(GamePosition destPos)
        {
            this.HasDestination = true;
            this.DestPos = destPos;
        }

        public void ChangeAngle(MovementAngleAction angleAction, ushort angle)
        {
            this.HasDestination = false;
            this.AngleAction = angleAction;
            this.Angle = angle;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
