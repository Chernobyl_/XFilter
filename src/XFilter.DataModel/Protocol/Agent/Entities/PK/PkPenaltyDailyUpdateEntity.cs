﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_PK_UPDATE_DAILY = 0x30D3
    /// TESTME
    /// </summary>
    public class PkPenaltyDailyUpdateEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public byte PkCount
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public PkPenaltyDailyUpdateEntity(byte pkCount)
        {
            this.PkCount = pkCount;
        }

        public PkPenaltyDailyUpdateEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity
        
        public bool ReadFromPacket(XPacket packet)
        {
            this.PkCount = packet.ReadValue<byte>();
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_PK_UPDATE_DAILY);
            packet.WriteValue<byte>(this.PkCount);
            return packet;
        }


        #endregion

        //--------------------------------------------------------------------------
    }
}
