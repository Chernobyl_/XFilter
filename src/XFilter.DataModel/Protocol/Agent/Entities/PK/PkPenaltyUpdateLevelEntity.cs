﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_PK_UPDATE_LEVEL = 0x30D3
    /// TESTME
    /// </summary>
    public class PkPenaltyUpdateLevelEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public ushort Level
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public PkPenaltyUpdateLevelEntity(ushort level)
        {
            this.Level = level;
        }

        public PkPenaltyUpdateLevelEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Level = packet.ReadValue<ushort>();
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_PK_UPDATE_LEVEL);
            packet.WriteValue<ushort>(this.Level);
            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
