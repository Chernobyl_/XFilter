﻿using System.Collections.Generic;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_GUILD_INFO_DATA = 0x3101
    /// Actually is fired twice once original SERVER_AGENT_GUILD_INFO_END is received 
    /// and when FULL packet data is ready.
    /// TESTME
    /// </summary>
    public class GuildInfoEntity : IPacketEntity, ISplitPacket
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public List<GuildInfo> Guilds
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public GuildInfoEntity()
        {
            this.Guilds = new List<GuildInfo>();
        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity
        
        //TODO: Multiple packets?
        public bool ReadFromPacket(XPacket packet)
        {
            //Original end packet... nothing to do here !
            if (packet.Length == 0)
                return false;

            //End packet (replaced by SplitPacketHandler).
            while(packet.HasDataToRead)
            {
                var item = new GuildInfo();
                item.ReadFromPacket(packet);
                this.Guilds.Add(item);
            }
            return true;
        }

        //TODO: Can split into multiple data packets :).
        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_GUILD_INFO_DATA);
            foreach (var guild in this.Guilds)
                guild.AppendToPacket(packet);
            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------

        #region ISplitPacket

        public List<XPacket> ToPackets()
        {
            var packets = new List<XPacket>();
            var startPacket = new XPacket((ushort)GameOpcode.SERVER_AGENT_GUILD_INFO_BEGIN);

            //TODO: Can split into multiple data packets :).

            var dataPacket = new XPacket((ushort)GameOpcode.SERVER_AGENT_GUILD_INFO_DATA);

            foreach (var guild in this.Guilds)
                guild.AppendToPacket(dataPacket);

            var endPacket = new XPacket((ushort)GameOpcode.SERVER_AGENT_GUILD_INFO_END);

            packets.Add(startPacket);
            packets.Add(dataPacket);
            packets.Add(endPacket);

            return packets;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
