﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_GUILD_UPDATE_NOTICE = 0x70F9
    /// TESTME
    /// </summary>
    public class GuildUpdateNoticeRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public string Title
        { get; set; }

        public string Content
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors


        public GuildUpdateNoticeRequestEntity(string title, string content)
        {
            this.Title = title;
            this.Content = content;
        }

        public GuildUpdateNoticeRequestEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Title = packet.ReadValue<string>();
            this.Content = packet.ReadValue<string>();
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_GUILD_UPDATE_NOTICE);

            packet.WriteValue<string>(this.Title);
            packet.WriteValue<string>(this.Content);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
