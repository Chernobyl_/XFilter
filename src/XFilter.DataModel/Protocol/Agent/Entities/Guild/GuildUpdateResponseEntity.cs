﻿using NLog;
using XFilter.Shared.Network;


namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_GUILD_UPDATE_RESPONSE = 0x38F5
    /// TESTME
    /// </summary>
    public class GuildUpdateResponseEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public GuildUpdateAction Action
        { get; set; }


        public GuildMember Member
        { get; private set; }

        /// <summary>
        /// Only if Action == GuildUpdateAction.UpdateMember
        /// </summary>
        public GuildMemberUpdateType MemberUpdateType
        { get; set; }

        public uint GuildPoints
        { get; set; }

        public GuildStateUpdateType GuildStateUpdateType
        { get; set; }

        public string GuildNoticeTitle
        { get; set; }

        public string GuildNoticeContent
        { get; set; }


        /// <summary>
        /// Can be guild member id or union member char id,
        /// depending on GuildUpdateAction
        /// </summary>
        public uint MemberCharId
        { get; set; }

        /// <summary>
        /// If Action == GuildUpdateAction.UnionLeave
        /// </summary>
        public UnionLeaveReason UnionLeaveReason
        { get; set; }

        /// <summary>
        /// Guild member update - 0x00
        /// </summary>
        public GuildMemberOnlineStatus StatusFlag
        { get; set; }

        public GuildPermission GuildAuthority
        { get; set; }

        public GuildSiegeAuthority SiegeAuthority
        { get; set; }

        public byte Level
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Action = packet.ReadEnum<GuildUpdateAction>();

            //--------------------------------------------------------------------------

            if (this.Action == GuildUpdateAction.AddMember)
            {
                this.Member = new GuildMember();
                this.Member.ReadFromPacket(packet);
            }

            //--------------------------------------------------------------------------

            if (this.Action == GuildUpdateAction.RemoveMemberById)
            {
                this.MemberCharId = packet.ReadValue<uint>();
            }

            //--------------------------------------------------------------------------

            if (this.Action == GuildUpdateAction.UnionLeave)
            {
                this.UnionLeaveReason = packet.ReadEnum<UnionLeaveReason>();
            }

            //--------------------------------------------------------------------------

            if (this.Action == GuildUpdateAction.MasterUpdate)
            {
                this.GuildStateUpdateType = packet.ReadEnum<GuildStateUpdateType>();

                if(this.GuildStateUpdateType == GuildStateUpdateType.GuildPointsUpdate)
                {
                    this.GuildPoints = packet.ReadValue<uint>();
                }

                if(this.GuildStateUpdateType == GuildStateUpdateType.TitleAndNotice)
                {
                    this.GuildNoticeTitle = packet.ReadValue<string>();
                    this.GuildNoticeContent = packet.ReadValue<string>();
                }
            }

            //--------------------------------------------------------------------------

            if (this.Action == GuildUpdateAction.UpdateMember)
            {
                this.MemberCharId = packet.ReadValue<uint>();

                this.MemberUpdateType = packet.ReadEnum<GuildMemberUpdateType>();

                //--------------------------------------------------------------------------

                if(this.MemberUpdateType == GuildMemberUpdateType.Level)
                {
                    this.Level = packet.ReadValue<byte>();
                }

                if (this.MemberUpdateType == GuildMemberUpdateType.GuildPoints)
                {
                    this.GuildPoints = packet.ReadValue<uint>();
                }

                //--------------------------------------------------------------------------

                if (this.MemberUpdateType == GuildMemberUpdateType.State)
                {
                    //TODO: MAY BE WRONG (SWAP TYPES)
                    this.StatusFlag = packet.ReadEnum<GuildMemberOnlineStatus>();
                }

                if(this.MemberUpdateType == GuildMemberUpdateType.SiegeAuthority)
                {
                    this.SiegeAuthority = packet.ReadEnum<GuildSiegeAuthority>();
                }

            }

            //--------------------------------------------------------------------------

            if (this.Action == GuildUpdateAction.AddAuthority)
            {
                bool success = packet.ReadValue<bool>();
                Logger.Error($"AddAuthority unknown second byte (after bool appearently): {packet.ReadValue<byte>()}");
                Logger.Debug(XFilter.Shared.Helpers.DebugHelper.HexDump(packet.GetBytes()));
                this.MemberCharId = packet.ReadValue<uint>();
                this.GuildAuthority = packet.ReadEnum<GuildPermission>();
            }

            //--------------------------------------------------------------------------

            if (this.Action == GuildUpdateAction.RemoveAuthority)
            {
                bool success = packet.ReadValue<bool>();
                this.MemberCharId = packet.ReadValue<uint>();
                this.GuildAuthority = packet.ReadEnum<GuildPermission>();
            }

            //--------------------------------------------------------------------------
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_GUILD_UPDATE_RESPONSE);

            packet.WriteEnum<GuildUpdateAction>(this.Action);

            switch(this.Action)
            {
                //--------------------------------------------------------------------------
                case GuildUpdateAction.AddMember:
                    {
                        this.Member.AppendToPacket(packet);
                    }
                    break;
                //--------------------------------------------------------------------------
                case GuildUpdateAction.RemoveMemberById:
                    {
                        packet.WriteValue<uint>(this.MemberCharId);
                    }
                    break;
                //--------------------------------------------------------------------------
                case GuildUpdateAction.UnionLeave:
                    {
                        packet.WriteEnum<UnionLeaveReason>(this.UnionLeaveReason);
                        packet.WriteValue<uint>(this.MemberCharId);
                    }
                    break;
                //--------------------------------------------------------------------------
                case GuildUpdateAction.MasterUpdate:
                    {
                        packet.WriteEnum<GuildStateUpdateType>(this.GuildStateUpdateType);
                        if(this.GuildStateUpdateType == GuildStateUpdateType.TitleAndNotice)
                        {
                            packet.WriteValue<string>(this.GuildNoticeTitle);
                            packet.WriteValue<string>(this.GuildNoticeContent);
                        }

                        if(this.GuildStateUpdateType == GuildStateUpdateType.GuildPointsUpdate)
                        {
                            packet.WriteValue<uint>(this.GuildPoints);
                        }
                    }
                    break;
                //--------------------------------------------------------------------------
                case GuildUpdateAction.UpdateMember:
                    {
                        packet.WriteValue<uint>(this.MemberCharId);
                        packet.WriteEnum<GuildMemberUpdateType>(this.MemberUpdateType);
                        
                        if(this.MemberUpdateType == GuildMemberUpdateType.Level)
                        {
                            packet.WriteValue<byte>(this.Level);
                        }

                        if(this.MemberUpdateType == GuildMemberUpdateType.GuildPoints)
                        {
                            packet.WriteValue<uint>(this.GuildPoints);
                        }

                        if(this.MemberUpdateType == GuildMemberUpdateType.State)
                        {
                            packet.WriteEnum<GuildMemberOnlineStatus>(this.StatusFlag);
                            //Logger.Warn($"Flag: {this.StatusFlag}.");
                        }

                        if(this.MemberUpdateType == GuildMemberUpdateType.SiegeAuthority)
                        {
                            packet.WriteEnum<GuildSiegeAuthority>(this.SiegeAuthority);
                        }

                        
                    }
                    break;
                //--------------------------------------------------------------------------
                case GuildUpdateAction.AddAuthority:
                    {
                        packet.WriteValue<bool>(true);
                        //HACK, FIXME
                        packet.WriteValue<byte>(16);
                        packet.WriteValue<uint>(this.MemberCharId);
                        packet.WriteEnum<GuildPermission>(this.GuildAuthority);
                    }
                    break;
                //--------------------------------------------------------------------------
                case GuildUpdateAction.RemoveAuthority:
                    {
                        packet.WriteValue<bool>(true);
                        packet.WriteValue<uint>(this.MemberCharId);

                        packet.WriteEnum<GuildPermission>(this.GuildAuthority);
                    }
                    break;
                //--------------------------------------------------------------------------
                default:
                    {
                        Logger.Error($"Unimplemented guild update action {this.Action}.");
                    }
                    break;
                //--------------------------------------------------------------------------
            }

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
