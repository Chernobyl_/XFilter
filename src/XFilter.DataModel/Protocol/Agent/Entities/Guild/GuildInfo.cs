﻿using NLog;
using System.Collections.Generic;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_GUILD_INFO_DATA = 0x3101
    /// Actually is fired twice once original SERVER_AGENT_GUILD_INFO_END is received 
    /// and when FULL packet data is ready.
    /// TESTME
    /// </summary>
    public class GuildInfo : IPacketInnerEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public uint ID
        { get; set; }
    
        public string Name
        { get; set; }

        public byte Level
        { get; set; }

        public uint GatheredSP
        { get; set; }

        public string MasterCommentTitle
        { get; set; }


        public string MasterComment
        { get; set; }

        public uint CurCrestRev
        { get; set; }

        public GuildMercenaryAttributes MercenaryAttr
        { get; set; }

        public List<GuildMember> Members
        { get; private set; }

        public byte ukn
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public GuildInfo()
        {
            this.Members = new List<GuildMember>();
        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketInnerEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Members = new List<GuildMember>();

            this.ID = packet.ReadValue<uint>();
            this.Name = packet.ReadValue<string>();
            this.Level = packet.ReadValue<byte>();
            this.GatheredSP = packet.ReadValue<uint>();
            this.MasterCommentTitle = packet.ReadValue<string>();
            this.MasterComment = packet.ReadValue<string>();
            this.CurCrestRev = packet.ReadValue<uint>();

            this.MercenaryAttr = packet.ReadEnum<GuildMercenaryAttributes>();

            byte memberCount = packet.ReadValue<byte>();

            for (int i = 0; i < memberCount; i++)
            {
                var member = new GuildMember();
                member.ReadFromPacket(packet);

                this.Members.Add(member);
            }

            this.ukn = packet.ReadValue<byte>();

            return true;
        }

        public bool AppendToPacket(XPacket packet)
        {
            packet.WriteValue<uint>(this.ID);
            packet.WriteValue<string>(this.Name);
            packet.WriteValue<byte>(this.Level);
            packet.WriteValue<uint>(this.GatheredSP);
            packet.WriteValue<string>(this.MasterCommentTitle);
            packet.WriteValue<string>(this.MasterComment);

            packet.WriteValue<uint>(this.CurCrestRev);
            packet.WriteEnum<GuildMercenaryAttributes>(this.MercenaryAttr);


            packet.WriteValue<byte>((byte)this.Members.Count);
            for(int i = 0; i < this.Members.Count; i++)
            {
                var member = this.Members[i];
                member.AppendToPacket(packet);
            }

            packet.WriteValue<byte>(this.ukn);
            return true;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
