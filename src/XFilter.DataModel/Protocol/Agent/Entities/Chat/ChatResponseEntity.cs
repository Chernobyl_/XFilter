﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_CHAT_RESPONSE = 0xB025
    /// TESTME
    /// </summary>
    public class ChatResponseEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public PacketResultCode Code
        { get; set; }

        public ChatType ChatType
        { get; set; }

        public ChatErrorCode ErrorCode
        { get; set; }

        /// <summary>
        /// UNKNOWN
        /// </summary>
        public byte ChatIndex
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors


        /// <summary>
        /// Successful chat ctor.
        /// </summary>
        /// <param name="chatType">Chat type.</param>
        /// <param name="chatIndex">UNKNOWN</param>
        public ChatResponseEntity(ChatType chatType, byte chatIndex)
        {
            this.Code = PacketResultCode.Success;
            this.ChatType = chatType;
            this.ChatIndex = chatIndex;
        }

        /// <summary>
        /// Chat message error ctor.
        /// </summary>
        /// <param name="errorCode">Error code.</param>
        public ChatResponseEntity(ChatErrorCode errorCode)
        {
            this.ErrorCode = errorCode;
        }

        public ChatResponseEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Code = packet.ReadEnum<PacketResultCode>();

            if (this.Code == PacketResultCode.Success)
            {
                this.ChatType = packet.ReadEnum<ChatType>();
                this.ChatIndex = packet.ReadValue<byte>();
            }
            else
            {
                this.ErrorCode = packet.ReadEnum<ChatErrorCode>();
            }

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_CHAT_RESPONSE);

            packet.WriteEnum<PacketResultCode>(this.Code);

            if (this.Code == PacketResultCode.Success)
            {
                packet.WriteEnum<PacketResultCode>(this.Code);
                packet.WriteValue<byte>(this.ChatIndex);
            }
            else packet.WriteEnum<ChatErrorCode>(this.ErrorCode);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}