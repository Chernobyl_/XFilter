﻿using NLog;
using System;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_CHAT_UPDATE = 0x3026
    /// TESTME
    /// </summary>
    public class ChatUpdateEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public ChatType ChatType
        { get; set; }

        public uint UniqueID
        { get; set; }

        public string Sender
        { get; set; }

        public string Message
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// Local chat ctor.
        /// </summary>
        /// <param name="chatType">Chat type.</param>
        /// <param name="uniqueId">Unique nearby sender ID.</param>
        /// <param name="message">Message.</param>
        public ChatUpdateEntity(ChatType chatType, uint uniqueId, string message)
        {
            if (chatType != ChatType.Normal || chatType != ChatType.NormalGm || chatType != ChatType.NPC)
                throw new ArgumentException($"Chat type {chatType} is not local. Use overloaded ctor without unique id.");

            this.ChatType = chatType;
            this.UniqueID = uniqueId;
            this.Message = message;
        }

        public ChatUpdateEntity(ChatType chatType, string message)
        {
            this.ChatType = chatType;
            this.Message = message;
        }

        /// <summary>
        /// Global chat ctor (sender is far away).
        /// </summary>
        /// <param name="chatType">Chat type.</param>
        /// <param name="sender">Message sender name.</param>
        /// <param name="message">Message text.</param>
        public ChatUpdateEntity(ChatType chatType, string sender, string message)
        {
            if (chatType == ChatType.Normal || chatType == ChatType.NormalGm || chatType == ChatType.NPC)
                throw new ArgumentException($"Chat type {chatType} is local. Use overloaded ctor with unique id.");

            this.ChatType = chatType;
            this.Sender = sender;
            this.Message = message;
        }

        public ChatUpdateEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.ChatType = packet.ReadEnum<ChatType>();

            if (this.ChatType == ChatType.Normal || this.ChatType == ChatType.NormalGm || this.ChatType == ChatType.NPC)
            {
                this.UniqueID = packet.ReadValue<uint>();
                this.Message = packet.ReadValue<string>();
                return true;
            }

            if(this.ChatType == ChatType.Notice)
            {
                this.Message = packet.ReadValue<string>();
                return true;
            }

            this.Sender = packet.ReadValue<string>();
            this.Message = packet.ReadValue<string>();

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_CHAT_UPDATE);

            packet.WriteEnum<ChatType>(this.ChatType);

            if (this.ChatType == ChatType.Normal || this.ChatType == ChatType.NormalGm || this.ChatType == ChatType.NPC)
            {
                packet.WriteValue<uint>(this.UniqueID);
                packet.WriteValue<string>(this.Message);
                return packet;
            }
            
            if(this.ChatType == ChatType.Notice)
            {
                packet.WriteValue<string>(this.Message);
                return packet;
            }

            //Otherwise has sender.
            packet.WriteValue<string>(this.Sender);
            packet.WriteValue<string>(this.Message);
            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}