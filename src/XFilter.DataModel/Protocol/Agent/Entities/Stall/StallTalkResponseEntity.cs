﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_STALL_TALK_RESPONSE = 0xB0B3
    /// UNIMPL, partial data
    /// </summary>
    public class StallTalkResponseEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public PacketResultCode Code
        { get; set; }

        public StallErrorCode ErrorCode
        { get; set; }

        public uint UniqueId
        { get; set; }

        public string Message
        { get; set; }

        public bool IsOpen
        { get; set; }

        public byte FleaMarketMode
        { get; set; }

        public byte[] UnimplBytes
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

       
        public StallTalkResponseEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Code = packet.ReadEnum<PacketResultCode>();

            if (this.Code == PacketResultCode.Success)
            {
                this.UniqueId = packet.ReadValue<uint>();
                this.Message = packet.ReadValue<string>();
                this.IsOpen = packet.ReadValue<bool>();
                this.FleaMarketMode = packet.ReadValue<byte>();
                this.UnimplBytes = packet.ReadByteArray(packet.RemainRead);
            }

            if (this.Code == PacketResultCode.Error)
                this.ErrorCode = packet.ReadEnum<StallErrorCode>();
            

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_STALL_TALK_RESPONSE);

            packet.WriteEnum<PacketResultCode>(this.Code);

            if (this.Code == PacketResultCode.Success)
            {
                packet.WriteValue<uint>(this.UniqueId);
                packet.WriteValue<string>(this.Message);
                packet.WriteValue<bool>(this.IsOpen);
                packet.WriteValue<byte>(this.FleaMarketMode);
                packet.WriteByteArray(this.UnimplBytes);
            }
            else packet.WriteEnum<StallErrorCode>(this.ErrorCode);
            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
