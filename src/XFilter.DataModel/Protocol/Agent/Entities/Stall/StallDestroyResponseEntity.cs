﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_STALL_DESTROY_RESPONSE = 0xB0B2
    /// TESTME
    /// </summary>
    public class StallDestroyResponseEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public PacketResultCode Code
        { get; set; }

        public StallErrorCode ErrorCode
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// Success ctor.
        /// </summary>

        public StallDestroyResponseEntity()
        {
            this.Code = PacketResultCode.Success;
        }

        /// <summary>
        /// Error ctor.
        /// </summary>
        /// <param name="errorCode"></param>
        public StallDestroyResponseEntity(StallErrorCode errorCode)
        {
            this.Code = PacketResultCode.Error;
            this.ErrorCode = errorCode;
        }


        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Code = packet.ReadEnum<PacketResultCode>();

            if (this.Code == PacketResultCode.Error)
                this.ErrorCode = packet.ReadEnum<StallErrorCode>();
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_STALL_DESTROY_RESPONSE);

            packet.WriteEnum<PacketResultCode>(this.Code);

            if (this.Code == PacketResultCode.Error)
                packet.WriteEnum<StallErrorCode>(this.ErrorCode);
            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
