﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_STALL_CREATE_REQUEST = 0x70B1
    /// TESTME
    /// </summary>
    public class StallCreateRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public string Name
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public StallCreateRequestEntity(string name)
        {
            this.Name = name;
        }

        public StallCreateRequestEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Name = packet.ReadValue<string>();
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_STALL_CREATE_REQUEST);

            packet.WriteValue<string>(this.Name);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
