﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_STALL_DESTROY_REQUEST = 0x70B2
    /// TESTME
    /// </summary>
    public class StallDestroyRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Constructors

        public StallDestroyRequestEntity(XPacket packet)
        {

        }

        public StallDestroyRequestEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_STALL_DESTROY_REQUEST);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
