﻿using NLog;
using System;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_STALL_UPDATE_REQUEST = 0x70BA
    /// TESTME
    /// </summary>
    public class StallUpdateRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public StallUpdateType UpdateType
        { get; set; }

        public byte StallSlot
        { get; set; }

        public byte InventorySlot
        { get; set; }

        public ushort StackCount
        { get; set; }

        public ulong Price
        { get; set; }

        public ushort UnkShort
        { get; set; }


        public uint FleaMarketNetworkTidGroup
        { get; set; }

        public byte FleaMarketMode
        { get; set; }

        public bool IsOpen
        { get; set; }

        public StallNetworkResult StallNetResult
        { get; set; }

        public string Message
        { get; set; }

        public string Name
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public StallUpdateRequestEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.UpdateType = packet.ReadEnum<StallUpdateType>();

            //--------------------------------------------------------------------------

            if (this.UpdateType == StallUpdateType.UpdateItem)
            {
                this.StallSlot = packet.ReadValue<byte>();
                this.StackCount = packet.ReadValue<ushort>();
                this.Price = packet.ReadValue<ulong>();
                this.UnkShort = packet.ReadValue<ushort>();
            }

            //--------------------------------------------------------------------------

            if (this.UpdateType == StallUpdateType.AddItem)
            {
                this.StallSlot = packet.ReadValue<byte>();
                this.InventorySlot = packet.ReadValue<byte>();
                this.StackCount = packet.ReadValue<ushort>();
                this.Price = packet.ReadValue<ulong>();
                this.FleaMarketNetworkTidGroup = packet.ReadValue<uint>();
                this.UnkShort = packet.ReadValue<ushort>();
            }

            //--------------------------------------------------------------------------

            if (this.UpdateType == StallUpdateType.RemoveItem)
            {
                this.StallSlot = packet.ReadValue<byte>();
                this.UnkShort = packet.ReadValue<ushort>();
            }

            //--------------------------------------------------------------------------

            if (this.UpdateType == StallUpdateType.FleaMarketMode)
            {
                //Unknown
                this.FleaMarketMode = packet.ReadValue<byte>();
            }

            //--------------------------------------------------------------------------

            if (this.UpdateType == StallUpdateType.State)
            {
                this.IsOpen = packet.ReadValue<bool>();
                this.StallNetResult = packet.ReadEnum<StallNetworkResult>();
            }

            //--------------------------------------------------------------------------

            if (this.UpdateType == StallUpdateType.Message)
                this.Message = packet.ReadValue<string>();

            //--------------------------------------------------------------------------

            if (this.UpdateType == StallUpdateType.Name)
                this.Name = packet.ReadValue<string>();

            //--------------------------------------------------------------------------

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_STALL_UPDATE_REQUEST);

            packet.WriteEnum<StallUpdateType>(this.UpdateType);

            if(this.UpdateType == StallUpdateType.UpdateItem)
            {
                packet.WriteValue<byte>(this.StallSlot);
                packet.WriteValue<ushort>(this.StackCount);
                packet.WriteValue<ulong>(this.Price);
                packet.WriteValue<ushort>(this.UnkShort);
            }

            if(this.UpdateType == StallUpdateType.AddItem)
            {
                packet.WriteValue<byte>(this.StallSlot);
                packet.WriteValue<byte>(this.InventorySlot);
                packet.WriteValue<ushort>(this.StackCount);
                packet.WriteValue<ulong>(this.Price);
                packet.WriteValue<uint>(this.FleaMarketNetworkTidGroup);
                packet.WriteValue<ushort>(this.UnkShort);
            }

            if(this.UpdateType == StallUpdateType.RemoveItem)
            {
                packet.WriteValue<byte>(this.StallSlot);
                packet.WriteValue<ushort>(this.UnkShort);
            }

            if(this.UpdateType == StallUpdateType.FleaMarketMode)
            {
                packet.WriteValue<byte>(this.FleaMarketMode);
            }

            if(this.UpdateType == StallUpdateType.State)
            {
                packet.WriteValue<bool>(this.IsOpen);
                packet.WriteEnum<StallNetworkResult>(this.StallNetResult);
            }

            if(this.UpdateType == StallUpdateType.Message)
                packet.WriteValue<string>(this.Message);

            if(this.UpdateType == StallUpdateType.Name)
                packet.WriteValue<string>(this.Name);
          
            return packet;
        }


        #endregion

        //--------------------------------------------------------------------------
    }
}
