﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_STALL_UPDATE_RESPONSE = 0xB0BA
    /// UNIMPL, missing item data etc
    /// TESTME
    /// </summary>
    public class StallUpdateResponseEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties
        
        public PacketResultCode Code
        { get; set; }
        
        public StallUpdateType UpdateType
        { get; set; }

        public bool IsOpen
        { get; set; }

        public StallNetworkResult StallNetResult
        { get; set; }

        public string Message
        { get; set; }

        public byte[] AddRemoveUpdateTypeBytes
        { get; set; }

        //--------------------------------------------------------------------------

        #region UpdateItem
        public byte Slot
        { get; set; }

        public ushort StackCount
        { get; set; }

        public ulong Price
        { get; set; }

        public StallErrorCode ErrorCode
        { get; set; }

        #endregion

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public StallUpdateResponseEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Code = packet.ReadEnum<PacketResultCode>();

            if (this.Code == PacketResultCode.Success)
            {
                this.UpdateType = packet.ReadEnum<StallUpdateType>();

                if (this.UpdateType == StallUpdateType.AddItem || this.UpdateType == StallUpdateType.RemoveItem)
                {
                    Logger.Warn("Stall add / remove item unimpl. Using RAW data.");
                    /*
                     * GenericItemData ??? 
                     * SourceSlot
                     * StackCount
                     * Price
                     */
                    this.AddRemoveUpdateTypeBytes = packet.ReadByteArray(packet.RemainRead);
                }

                if (this.UpdateType == StallUpdateType.State)
                {
                    this.IsOpen = packet.ReadValue<bool>();
                    this.StallNetResult = packet.ReadEnum<StallNetworkResult>();
                }

                if (this.UpdateType == StallUpdateType.Message)
                {
                    this.Message = packet.ReadValue<string>();
                }
            }

            return true;
        }


        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_STALL_UPDATE_RESPONSE);

            packet.WriteEnum<PacketResultCode>(this.Code);

            if (this.Code == PacketResultCode.Error)
                return packet;

            if(this.UpdateType == StallUpdateType.AddItem || this.UpdateType == StallUpdateType.RemoveItem)
            {
                packet.WriteByteArray(this.AddRemoveUpdateTypeBytes);
            }

            if(this.UpdateType == StallUpdateType.State)
            {
                packet.WriteValue<bool>(this.IsOpen);
                packet.WriteEnum<StallNetworkResult>(this.StallNetResult);
            }

            if(this.UpdateType == StallUpdateType.Message)
            {
                packet.WriteValue<string>(this.Message);
            }
            

            return packet;
        }


        #endregion

        //--------------------------------------------------------------------------
    }
}
