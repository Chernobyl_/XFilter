﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_STALL_BUY_REQUEST = 0x70B4
    /// TESTME
    /// </summary>
    public class StallBuyRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public byte Slot
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public StallBuyRequestEntity(byte slot)
        {
            this.Slot = slot;
        }

        public StallBuyRequestEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Slot = packet.ReadValue<byte>();
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_STALL_BUY_REQUEST);

            packet.WriteValue<byte>(this.Slot);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
