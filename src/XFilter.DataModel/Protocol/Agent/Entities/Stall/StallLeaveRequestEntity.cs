﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_STALL_LEAVE_REQUEST = 0x70B5
    /// TESTME
    /// </summary>
    public class StallLeaveRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Constructors

        public StallLeaveRequestEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_STALL_LEAVE_REQUEST);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
