﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_STALL_CREATE_RESPONSE = 0xB0B1
    /// TESTME
    /// </summary>
    public class StallCreateResponseEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public PacketResultCode Code
        { get; set; }

        public StallErrorCode ErrorCode
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// Success by default ctor.
        /// </summary>
        public StallCreateResponseEntity()
        {
            this.Code = PacketResultCode.Success;
        }

        /// <summary>
        /// Error message ctor.
        /// </summary>
        /// <param name="errorCode"></param>
        public StallCreateResponseEntity(StallErrorCode errorCode)
        {
            this.Code = PacketResultCode.Error;
            this.ErrorCode = ErrorCode;
        }


        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Code = packet.ReadEnum<PacketResultCode>();

            if (this.Code == PacketResultCode.Error)
                this.ErrorCode = packet.ReadEnum<StallErrorCode>();

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_STALL_CREATE_RESPONSE);

            packet.WriteEnum<PacketResultCode>(this.Code);

            if (this.Code == PacketResultCode.Error)
                packet.WriteValue<StallErrorCode>(this.ErrorCode);

            return packet;
        }


        #endregion

        //--------------------------------------------------------------------------
    }
}
