﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_STALL_BUY_RESPONSE = 0xB0B4
    /// TESTME
    /// </summary>
    public class StallBuyResponseEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public PacketResultCode Code
        { get; set; }

        public StallErrorCode ErrorCode
        { get; set; }

        public byte Slot
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public StallBuyResponseEntity(byte slot)
        {
            this.Code = PacketResultCode.Success;
            this.Slot = slot;
        }

        public StallBuyResponseEntity(StallErrorCode errorCode)
        {
            this.Code = PacketResultCode.Error;
            this.ErrorCode = errorCode;
        }

        public StallBuyResponseEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Code = packet.ReadEnum<PacketResultCode>();

            if (this.Code == PacketResultCode.Success)
                this.Slot = packet.ReadValue<byte>();

            if (this.Code == PacketResultCode.Error)
                this.ErrorCode = packet.ReadEnum<StallErrorCode>();

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_STALL_BUY_RESPONSE);

            packet.WriteEnum<PacketResultCode>(this.Code);

            if (this.Code == PacketResultCode.Success)
                packet.WriteValue<byte>(this.Slot);
            else packet.WriteEnum<StallErrorCode>(this.ErrorCode);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
