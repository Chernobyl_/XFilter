﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_XTRAP_CHALLENGE = 0x2113
    /// TESTME
    /// </summary>
    public class XtrapChallengeEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public byte[] Content
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public XtrapChallengeEntity(byte[] challenge)
        {
            this.Content = challenge;
        }

        public XtrapChallengeEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Content = packet.ReadByteArray(packet.RemainRead);
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_XTRAP_CHALLENGE, true);

            packet.WriteByteArray(this.Content);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
