﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_XTRAP_CHALLENGE_RESPONSE = 0x2113
    /// TESTME
    /// </summary>
    public class XtrapChallengeResponseEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------
        #region Public properties

        public byte[] Content
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public XtrapChallengeResponseEntity(byte[] responseBytes)
        {
            this.Content = responseBytes;
        }

        public XtrapChallengeResponseEntity()
        {

        }

        #endregion


        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Content = packet.ReadByteArray(packet.RemainRead);
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_XTRAP_CHALLENGE_RESPONSE);

            packet.WriteByteArray(this.Content);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
