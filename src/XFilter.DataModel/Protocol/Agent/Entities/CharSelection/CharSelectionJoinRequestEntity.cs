﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_CHARACTER_SELECTION_JOIN_REQUEST = 0x7001
    /// Do NOT rely on this packet for charname.
    /// TESTME
    /// </summary>
    public class CharSelectionJoinRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public string Charname
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors


        /// <summary>
        /// Normal ctor.
        /// </summary>
        /// <param name="charname"></param>
        public CharSelectionJoinRequestEntity(string charname)
        {
            this.Charname = charname;
        }

        public CharSelectionJoinRequestEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Charname = packet.ReadValue<string>();
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_CHARACTER_SELECTION_JOIN_REQUEST);

            packet.WriteValue<string>(this.Charname);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
