﻿using NLog;
using System;
using System.Collections.Generic;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_CHARACTER_SELECTION_RESPONSE = B007
    /// TESTME
    /// </summary>
    public class CharSelectionActionResultEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public CharSelectionAction ActionCode
        { get; set; }

        public PacketResultCode ResultCode
        { get; set; }

        public CharSelectionErrorCode ErrorCode
        { get; set; }

        public byte CharacterCount => (byte)this.Characters.Count;

        public List<CharSelectionCharInfo> Characters
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

      

        public CharSelectionActionResultEntity
            (
            PacketResultCode resCode, CharSelectionAction action, 
            List<CharSelectionCharInfo> chars = null
            )
        {
            if (action == CharSelectionAction.List && chars == null)
                throw new ArgumentNullException("Must specify characters for listing.");

            this.ResultCode = resCode;
            this.ActionCode = action;
            this.Characters = chars;
        }

        public CharSelectionActionResultEntity(CharSelectionErrorCode errorCode)
        {
            this.ResultCode = PacketResultCode.Error;
            this.ErrorCode = errorCode;
            this.Characters = null;
        }

        public CharSelectionActionResultEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Characters = new List<CharSelectionCharInfo>();

            this.ActionCode = packet.ReadEnum<CharSelectionAction>();

            #region Create, Delete, CheckName, Restore

            if (this.ActionCode == CharSelectionAction.Create ||
               this.ActionCode == CharSelectionAction.Delete ||
               this.ActionCode == CharSelectionAction.CheckName ||
               this.ActionCode == CharSelectionAction.Restore)
            {
                this.ResultCode = packet.ReadEnum<PacketResultCode>();

                if (this.ResultCode == PacketResultCode.Error)
                    this.ErrorCode = packet.ReadEnum<CharSelectionErrorCode>();
                
            }

            #endregion

            #region Listing characters

            if (this.ActionCode == CharSelectionAction.List)
            {
                this.ResultCode = packet.ReadEnum<PacketResultCode>();
                if (this.ResultCode == PacketResultCode.Success)
                {
                    //Not need 2 save it as we have property getting char count from list
                    var charCount = packet.ReadValue<byte>();

                    for (int i = 0; i < charCount; i++)
                    {
                        var info = new CharSelectionCharInfo();
                        info.ReadFromPacket(packet);

                        this.Characters.Add(info);
                    }
                }

                if (this.ResultCode == PacketResultCode.Error)
                    this.ErrorCode = packet.ReadEnum<CharSelectionErrorCode>();
                
            }

            #endregion

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_CHARACTER_SELECTION_RESPONSE);
            //got rekt rewriting :D

            packet.WriteEnum<CharSelectionAction>(this.ActionCode);
            if (this.ActionCode == CharSelectionAction.Create ||
              this.ActionCode == CharSelectionAction.Delete ||
              this.ActionCode == CharSelectionAction.CheckName ||
              this.ActionCode == CharSelectionAction.Restore)
            {
                packet.WriteEnum<PacketResultCode>(this.ResultCode);
                if (this.ResultCode == PacketResultCode.Error)
                    packet.WriteEnum<CharSelectionErrorCode>(this.ErrorCode);
            }

            if(this.ActionCode == CharSelectionAction.List)
            {
                packet.WriteEnum<PacketResultCode>(this.ResultCode);
                if(this.ResultCode == PacketResultCode.Success)
                {
                    packet.WriteValue<byte>((byte)this.CharacterCount);
                    foreach(var character in this.Characters)
                    {
                        character.AppendToPacket(packet);
                    }
                }

                if (this.ResultCode == PacketResultCode.Error)
                    packet.WriteEnum<CharSelectionErrorCode>(this.ErrorCode);
            }
            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
