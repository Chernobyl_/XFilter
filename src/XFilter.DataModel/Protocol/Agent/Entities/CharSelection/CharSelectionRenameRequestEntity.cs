﻿using NLog;
using System;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_CHARACTER_SELECTION_RENAME_REQUEST = 0x7450
    /// TESTME
    /// </summary>
    public class CharSelectionRenameRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public CharSelectionNameAction Action
        { get; set; }

        public string Name
        { get; set; }

        public string NewName
        { get; set; }


        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public CharSelectionRenameRequestEntity(CharSelectionNameAction action, string name, string newName = null)
        {
            this.Action = action;
            this.Name = name;

            if(this.Action == CharSelectionNameAction.CharRename || action == CharSelectionNameAction.GuildRename)
            {
                if (newName == null)
                    throw new ArgumentNullException("New name cannot be null for this name action.");
                this.NewName = newName;
            }
        }

        public CharSelectionRenameRequestEntity()
        {

        }


        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            var action = packet.ReadEnum<CharSelectionNameAction>();

            this.Name = packet.ReadValue<string>();

            if (action == CharSelectionNameAction.CharRename || action == CharSelectionNameAction.GuildRename)
                this.NewName = packet.ReadValue<string>();

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_CHARACTER_SELECTION_RENAME_REQUEST);

            packet.WriteEnum<CharSelectionNameAction>(this.Action);
            packet.WriteValue<string>(this.Name);

            if (this.Action == CharSelectionNameAction.CharRename || this.Action == CharSelectionNameAction.GuildRename)
                packet.WriteValue<string>(this.NewName);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
