﻿using NLog;
using System;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_LOGOUT_CANCEL_RESPONSE = 0xB006
    /// TESTME
    /// </summary>
    public class LogoutCancelResponseEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------


        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public PacketResultCode Code
        { get; set; }

        public LogoutErrorCode ErrorCode
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// Logout cancel success ctor.
        /// </summary>
        /// <param name="resCode">Result code.</param>
        public LogoutCancelResponseEntity(PacketResultCode resCode)
        {
            if (resCode == PacketResultCode.Error)
                throw new ArgumentException("Error code required.");

            this.Code = resCode;
        }

        /// <summary>
        /// Logout cancel error ctor.
        /// </summary>
        /// <param name="errorCode"></param>
        public LogoutCancelResponseEntity(LogoutErrorCode errorCode)
        {
            this.Code = PacketResultCode.Error;
            this.ErrorCode = errorCode;
        }

        public LogoutCancelResponseEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Code = packet.ReadEnum<PacketResultCode>();

            if (this.Code == PacketResultCode.Error)
            {
                this.ErrorCode = packet.ReadEnum<LogoutErrorCode>();
            }

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_LOGOUT_CANCEL_RESPONSE);

            packet.WriteEnum<PacketResultCode>(this.Code);
            if (this.Code == PacketResultCode.Error)
                packet.WriteValue<ushort>((ushort)this.ErrorCode);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
