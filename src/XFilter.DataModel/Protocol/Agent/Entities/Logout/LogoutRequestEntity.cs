﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_LOGOUT_REQUEST = 0x7005
    /// TESTME
    /// </summary>
    public class LogoutRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public LogoutMode Mode
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public LogoutRequestEntity(LogoutMode mode)
        {
            this.Mode = mode;
        }

        public LogoutRequestEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Mode = packet.ReadEnum<LogoutMode>();

            return true;
        }


        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_LOGOUT_REQUEST);

            packet.WriteEnum<LogoutMode>(this.Mode);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
