﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Global
{
    /// <summary>
    /// GLOBAL_IDENTIFICATION = 0x2001, Encrypted
    /// TESTME
    /// </summary>
    public class ModuleIdentifyEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public string ServiceName
        { get; set; }

        public bool IsLocal
        { get; set; }

        /// <summary>
        /// C->S is encrypted, S->C is not.
        /// </summary>
        public bool IsEncrypted
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors
        
        public ModuleIdentifyEntity(string serviceName, bool isLocal, bool isClient)
        {
            this.ServiceName = serviceName;
            this.IsLocal = isLocal;
            this.IsEncrypted = isClient;
        }

        public ModuleIdentifyEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public bool ReadFromPacket(XPacket packet)
        {
            this.ServiceName = packet.ReadValue<string>();
            this.IsLocal = packet.ReadValue<bool>();

            this.IsEncrypted = packet.Encrypted;

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.GLOBAL_MODULE_IDENTIFY, this.IsEncrypted);

            packet.WriteValue<string>(this.ServiceName);
            packet.WriteValue<bool>(this.IsLocal);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
