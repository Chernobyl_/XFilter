﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol
{
    /// <summary>
    /// Inner packet entity interface. Only used as a part of packet. For ex: patch file info.
    /// </summary>
    interface IPacketInnerEntity
    {
        /// <summary>
        /// Reads entity from the stream.
        /// </summary>
        /// <param name="packet"></param>
        /// <returns></returns>
        bool ReadFromPacket(XPacket packet);

        /// <summary>
        /// Appends entity to the stream.
        /// </summary>
        /// <param name="packet"></param>
        /// <returns></returns>
        bool AppendToPacket(XPacket packet);
    }
}
