﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    public class GamePosition : IPacketInnerEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public ushort RegionID
        { get; set; }

        public float X
        { get; set; }

        /// <summary>
        /// Height.
        /// </summary>
        public float Y
        { get; set; }

        public float Z
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public GamePosition(ushort regionId, float x, float y, float z)
        {
            this.RegionID = regionId;
            this.X = x;
            this.Y = y;
            this.Z = z;
        }

        public GamePosition()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public bool ReadFromPacket(XPacket packet)
        {
            this.RegionID = packet.ReadValue<ushort>();

            float x, y, z;

            if(this.RegionID < short.MaxValue)
            {
                //Normal world
                x = packet.ReadValue<short>();
                y = packet.ReadValue<short>();
                z = packet.ReadValue<short>();
            }
            else
            {
                //Dungeon
                x = packet.ReadValue<int>();
                y = packet.ReadValue<int>();
                z = packet.ReadValue<int>();
            }

            this.X = x;
            this.Y = y;
            this.Z = z;

            return true;
        }

        public bool AppendToPacket(XPacket packet)
        {
            packet.WriteValue<ushort>(this.RegionID);

            if(this.RegionID < short.MaxValue)
            {
                //Normal world
                packet.WriteValue<short>(this.X);
                packet.WriteValue<short>(this.Y);
                packet.WriteValue<short>(this.Z);

            }
            else
            {
                //Dungeon
                packet.WriteValue<int>(this.X);
                packet.WriteValue<int>(this.Y);
                packet.WriteValue<int>(this.Z);
            }

            return true;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
