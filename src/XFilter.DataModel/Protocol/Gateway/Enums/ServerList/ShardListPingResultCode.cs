﻿namespace XFilter.DataModel.Protocol.Gateway
{
    //--------------------------------------------------------------------------
    public enum ShardListPingResultCode : byte
    {
        Success = 1,
        Error = 2,
    }

    //--------------------------------------------------------------------------
}
