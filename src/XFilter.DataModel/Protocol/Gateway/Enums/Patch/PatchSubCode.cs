﻿namespace XFilter.DataModel.Protocol.Gateway
{
    //--------------------------------------------------------------------------

    /// <summary>
    /// PatchServerInfo second byte. Not sure if its always sent, or only
    /// if certain PatchCode (first byte) is set to ClientNeedsPatch...
    /// Anyways, theres a check.s
    /// </summary>
    public enum PatchSubCode : byte
    {
        InvalidClient = 0x01,
        Update = 0x02,
        InvalidClient2 = 0x03,
        Inspection = 0x04,
        FullVersionRequired = 0x05,
    }

    //--------------------------------------------------------------------------
}
