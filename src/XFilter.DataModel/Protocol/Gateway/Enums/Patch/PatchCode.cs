﻿namespace XFilter.DataModel.Protocol.Gateway
{
    //--------------------------------------------------------------------------

    /// <summary>
    /// PatchServerInfo packet first byte.
    /// </summary>
    public enum PatchCode : byte
    {
        Success = 0x01,
        Error = 0x02,
    }

    //--------------------------------------------------------------------------
}
