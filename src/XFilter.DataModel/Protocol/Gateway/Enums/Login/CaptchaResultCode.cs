﻿namespace XFilter.DataModel.Protocol.Gateway
{
    //--------------------------------------------------------------------------

    public enum CaptchaResultCode : byte
    {
        Success = 0x01,
        Error = 0x02,
    }

    //--------------------------------------------------------------------------
}
