﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Gateway
{
    /// <summary>
    /// S->C SERVER_GATEWAY_SHARD_LIST_RESPONSE = 0xA101
    /// TESTME
    /// </summary>
    public class FarmOperationInfo
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public byte ID
        { get; set; }

        public byte OperationType
        { get; set; }

        public string OperationName
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors


        #endregion

        //--------------------------------------------------------------------------

        #region IPacketInnerEntity


        public bool ReadFromPacket(XPacket packet, byte farmId)
        {
            this.ID = farmId;
            this.OperationType = packet.ReadValue<byte>();
            this.OperationName = packet.ReadValue<string>();
            return true;
        }

        public bool AppendToPacket(XPacket packet)
        {
            packet.WriteValue<byte>(this.OperationType);
            packet.WriteValue<string>(this.OperationName);
            return true;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
