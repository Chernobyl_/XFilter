﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Gateway
{
    /// <summary>
    /// S->C SERVER_GATEWAY_SHARD_LIST_PING_RESPONSE = 0xA106
    /// TESTME
    /// </summary>
    public class ShardListPingResponseEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public PacketResultCode Code
        { get; private set; }

        public byte FarmId
        { get; private set; }
       
        public uint IpAddressInt
        { get; private set; }

        public ShardListPingErrorCode ErrorCode
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public ShardListPingResponseEntity(byte farmId, uint ipAddr)
        {
            this.Code = PacketResultCode.Success;
            this.FarmId = farmId;
            this.IpAddressInt = ipAddr;
        }

        public ShardListPingResponseEntity(ShardListPingErrorCode errorCode)
        {
            this.Code = PacketResultCode.Error;
            this.ErrorCode = errorCode;
        }

        public ShardListPingResponseEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public bool ReadFromPacket(XPacket packet)
        {
            this.Code = packet.ReadEnum<PacketResultCode>();

            if (this.Code == PacketResultCode.Success)
            {
                this.FarmId = packet.ReadValue<byte>();
                this.IpAddressInt = packet.ReadValue<uint>();
                return true;
            }

            this.ErrorCode = packet.ReadEnum<ShardListPingErrorCode>();

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_GATEWAY_SHARD_LIST_PING_RESPONSE);

            packet.WriteValue<byte>((byte)this.Code);

            switch(this.Code)
            {
                case PacketResultCode.Success:
                    {
                        packet.WriteValue<byte>(this.FarmId);
                        packet.WriteValue<uint>(this.IpAddressInt);
                    }
                    break;

                case PacketResultCode.Error:
                    {
                        packet.WriteValue<byte>(this.ErrorCode);
                    }
                    break;
            }

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
