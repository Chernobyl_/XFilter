﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Gateway
{
    /// <summary>
    /// C->S CLIENT_GATEWAY_SHARD_LIST_PING_REQUEST = 0x6106
    /// TESTME
    /// </summary>
    public class ShardListPingRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_GATEWAY_SHARD_LIST_PING_REQUEST, true);
            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
