﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Gateway
{
    /// <summary>
    /// C->S CLIENT_GATEWAY_SHARD_LIST_REQUEST = 0x6101, Encrypted
    /// TESTME
    /// </summary>
    public class ShardListRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Logic

        public bool ReadFromPacket(XPacket packet)
        {
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_GATEWAY_SERVERLIST_REQUEST, true);
            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
