﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Gateway
{
    /// <summary>
    /// S->C SERVER_GATEWAY_SHARD_LIST_RESPONSE = 0xA101
    /// TESTME
    /// </summary>
    public class ServerListShardInfo : IPacketInnerEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public ushort ID
        { get; set; }

        public string Name
        { get; set; }

        public uint CurUsers
        { get; set; }
        
        public uint MaxUsers
        { get; set; }

        public bool IsOperating
        { get; set; }

        /// <summary>
        /// The referenced operation ID.
        /// </summary>
        public byte FarmID
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public ServerListShardInfo()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketInnerEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.ID = packet.ReadValue<ushort>();
            this.Name = packet.ReadValue<string>();
            this.CurUsers = packet.ReadValue<ushort>();
            this.MaxUsers = packet.ReadValue<ushort>();
            this.IsOperating = packet.ReadValue<bool>();
            this.FarmID = packet.ReadValue<byte>();

            return true;
        }

        public bool AppendToPacket(XPacket packet)
        {
            packet.WriteValue<ushort>(this.ID);
            packet.WriteValue<string>(this.Name);
            packet.WriteValue<ushort>(this.CurUsers);
            packet.WriteValue<ushort>(this.MaxUsers);
            packet.WriteValue<bool>(this.IsOperating);
            packet.WriteValue<byte>(this.FarmID);
            return true;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
