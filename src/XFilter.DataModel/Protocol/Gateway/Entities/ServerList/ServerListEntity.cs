﻿using System.Collections.Generic;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Gateway
{
    /// <summary>
    /// S->C SERVER_GATEWAY_SHARD_LIST_RESPONSE = 0xA101
    /// TESTME
    /// </summary>
    public class ServerListEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private List<FarmOperationInfo> _globalOperations;
        private List<ServerListShardInfo> _shards;

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public ServerListEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            _globalOperations = new List<FarmOperationInfo>();
            _shards = new List<ServerListShardInfo>();

            byte farmId = 0;
            while (packet.HasDataToRead && packet.ReadValue<bool>() == true)
            {
                var opInfo = new FarmOperationInfo();
                opInfo.ReadFromPacket(packet, farmId++);
                _globalOperations.Add(opInfo);
            }

            while (packet.HasDataToRead && packet.ReadValue<bool>() == true)
            {
                var shardInfo = new ServerListShardInfo();
                shardInfo.ReadFromPacket(packet);
                _shards.Add(shardInfo);
            }
            return true;
        }

        public XPacket WriteToPacket()
        {
            //???
            var packet = new XPacket((ushort)GameOpcode.SERVER_GATEWAY_SHARD_LIST_RESPONSE);

            //No global operation entities - wtf?
            if(_globalOperations.Count == 0)
            {
                packet.WriteValue<bool>(false);
                return packet;
            }

            //No shards - wtf ?
            if(_shards.Count == 0)
            {
                packet.WriteValue<bool>(false);
                return packet;
            }

            foreach(var globalOp in _globalOperations)
            {
                //Has entities
                packet.WriteValue<bool>(true);

                globalOp.AppendToPacket(packet);
            }

            packet.WriteValue<bool>(false);

            foreach (var shard in _shards)
            {
                packet.WriteValue<bool>(true);
                shard.AppendToPacket(packet);
            }

            packet.WriteValue<bool>(false);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
