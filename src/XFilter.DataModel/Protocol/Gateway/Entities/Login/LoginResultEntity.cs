﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Gateway
{
    /// <summary>
    /// S->C SERVER_GATEWAY_LOGIN_RESPONSE = 0xA102,
    /// TESTME
    /// </summary>
    public class LoginResultEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Properties - General

        public LoginResultCode Code
        { get; set; }

        public LoginErrorCode ErrorCode
        { get; set; }

        public LoginBlockType BlockType
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Properties - Max login attempts

        public uint MaxAttempts
        { get; set; }

        public uint CurAttempts
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Properties - Login block punishment info

        public string BanReason
        { get; set; }

        public ushort BanEndYear
        { get; set; }

        public ushort BanEndMonth
        { get; set; }

        public ushort BanEndDay
        { get; set; }

        public ushort BanEndHour
        { get; set; }

        public ushort BanEndMinute
        { get; set; }

        public ushort BanEndSecond
        { get; set; }

        public ushort BanEndMicrosecond
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Properties - Agent info (success)

        public uint Token
        { get; private set; }
        
        public string Address
        { get; set; }

        public ushort Port
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public LoginResultEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Code = packet.ReadEnum<LoginResultCode>();
            
            if (this.Code == LoginResultCode.Success)
            {
                this.Token = packet.ReadValue<uint>();
                this.Address = packet.ReadValue<string>();
                this.Port = packet.ReadValue<ushort>();
                return true;
            }

            if (this.Code == LoginResultCode.Error)
            {
                this.ErrorCode = packet.ReadEnum<LoginErrorCode>();

                if(this.ErrorCode == LoginErrorCode.InvalidCredentials)
                {
                    this.MaxAttempts = packet.ReadValue<uint>();
                    this.CurAttempts = packet.ReadValue<uint>();
                }

                if(this.ErrorCode == LoginErrorCode.Blocked)
                {
                    var blockType = (LoginBlockType)packet.ReadValue<byte>();

                    this.BanReason = packet.ReadValue<string>();
                    this.BanEndYear = packet.ReadValue<ushort>();
                    this.BanEndMonth = packet.ReadValue<ushort>();
                    this.BanEndDay = packet.ReadValue<ushort>();
                    this.BanEndHour = packet.ReadValue<ushort>();
                    this.BanEndMinute = packet.ReadValue<ushort>();
                    this.BanEndSecond = packet.ReadValue<ushort>();
                    this.BanEndMicrosecond = packet.ReadValue<ushort>();
                }
            }

            if(this.Code == LoginResultCode.Custom)
            {
                //UNIMPL
            }

            return true;
        }

        public XPacket WriteToPacket()
        {
            XPacket packet = new XPacket((ushort)GameOpcode.SERVER_GATEWAY_LOGIN_RESPONSE);
            packet.WriteEnum<LoginResultCode>(this.Code);

            if(this.Code == LoginResultCode.Success)
            {
                packet.WriteValue<uint>(this.Token);
                packet.WriteValue<string>(this.Address);
                packet.WriteValue<ushort>(this.Port);
            }

            if(this.Code == LoginResultCode.Error)
            {
                packet.WriteEnum<LoginErrorCode>(this.ErrorCode);

                if (this.ErrorCode == LoginErrorCode.InvalidCredentials)
                {
                    packet.WriteValue<uint>(this.MaxAttempts);
                    packet.WriteValue<uint>(this.CurAttempts);
                }

                if(this.ErrorCode == LoginErrorCode.Blocked)
                {
                    packet.WriteEnum<LoginBlockType>(this.BlockType);
                    packet.WriteValue<string>(this.BanReason);
                    packet.WriteValue<ushort>(this.BanEndYear);
                    packet.WriteValue<ushort>(this.BanEndMonth);
                    packet.WriteValue<ushort>(this.BanEndDay);
                    packet.WriteValue<ushort>(this.BanEndHour);
                    packet.WriteValue<ushort>(this.BanEndMinute);
                    packet.WriteValue<ushort>(this.BanEndSecond);
                    packet.WriteValue<ushort>(this.BanEndMicrosecond);
                }
            }

            if(this.Code == LoginResultCode.Custom)
            {
                
            }

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
