﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Gateway
{
    /// <summary>
    /// C->S CLIENT_GATEWAY_CAPTCHA_ANSWER = 0x6323
    /// TESTME
    /// </summary>
    public class CaptchaAnswerEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public string Answer
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public CaptchaAnswerEntity()
        {
            
        }

        public CaptchaAnswerEntity(string answer)
        {
            this.Answer = answer;
        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Answer = packet.ReadValue<string>();
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_GATEWAY_CAPTCHA_ANSWER);

            packet.WriteValue<string>(this.Answer);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
