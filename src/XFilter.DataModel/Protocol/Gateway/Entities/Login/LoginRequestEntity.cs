﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Gateway
{
    /// <summary>
    /// C->S CLIENT_GATEWAY_LOGIN_REQUEST = 0x6102
    /// TESTME
    /// </summary>
    public class LoginRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public SrOperationType Locale
        { get; set; }

        public string Username
        { get; private set; }

        public string Password
        { get; private set; }

        public ushort ShardID
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public LoginRequestEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Locale = packet.ReadEnum<SrOperationType>();
            this.Username = packet.ReadValue<string>();
            this.Password = packet.ReadValue<string>();
            this.ShardID = packet.ReadValue<ushort>();

            return true;
        }

        public XPacket WriteToPacket()
        {
            XPacket packet = new XPacket((ushort)GameOpcode.CLIENT_GATEWAY_LOGIN_REQUEST, true);
            packet.WriteEnum<SrOperationType>(this.Locale);
            packet.WriteValue<string>(this.Username);
            packet.WriteValue<string>(this.Password);
            packet.WriteValue<ushort>(this.ShardID);
            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
