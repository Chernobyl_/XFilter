﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Gateway
{
    /// <summary>
    /// S->C SERVER_GATEWAY_CAPTCHA_RESULT = 0xA323
    /// TESTME
    /// </summary>
    public class CaptchaResultEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties & statics

        public PacketResultCode Code
        { get; set; }

        public uint MaxAttempts
        { get; set; }

        public uint CurAttempts
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors
        public CaptchaResultEntity()
        {

        }
        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Code = packet.ReadEnum<PacketResultCode>();

            if (this.Code == PacketResultCode.Error)
            {
                this.MaxAttempts = packet.ReadValue<uint>();
                this.CurAttempts = packet.ReadValue<uint>();
            }

            return true;
        }


        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_GATEWAY_CAPTCHA_RESULT);
            packet.WriteEnum<PacketResultCode>(this.Code);

            if(this.Code == PacketResultCode.Error)
            {
                packet.WriteValue<uint>(this.MaxAttempts);
                packet.WriteValue<uint> (this.CurAttempts);
            }

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
