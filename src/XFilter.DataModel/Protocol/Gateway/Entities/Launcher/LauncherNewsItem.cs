﻿using System;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Gateway
{
    /// <summary>
    /// S->C SERVER_GATEWAY_LAUNCHER_NEWS_RESPONSE = 0xA104, Massive
    /// TESTME
    /// </summary>
    public class LauncherNewsItem : IPacketInnerEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public string Title
        { get; set; }

        public string Content
        { get; set; }

        public ushort EditYear
        { get; set; }

        public ushort EditMonth
        { get; set; }

        public ushort EditDay
        { get; set; }

        public ushort EditHour
        { get; set; }

        public ushort EditMinute
        { get; set; }

        public ushort EditSecond
        { get; set; }

        public ushort EditMilisecond
        { get; set; }

        public ushort EditMicrosecond
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public LauncherNewsItem()
        {
           
        }

        
        public LauncherNewsItem(string title, string content, bool fromCurDate = true)
        {
            this.Title = title;
            this.Content = content;

            if (fromCurDate)
            {
                var curTime = DateTime.Now;

                this.EditYear = (ushort)curTime.Year;
                this.EditMonth = (ushort)curTime.Month;
                this.EditDay = (ushort)curTime.Day;
                this.EditHour = (ushort)curTime.Hour;
                this.EditMinute = (ushort)curTime.Minute;
                this.EditSecond = (ushort)curTime.Second;
                this.EditMilisecond = (ushort)curTime.Millisecond;
                this.EditMicrosecond = 0;
            }
        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketInnerEntity
        
        public bool ReadFromPacket(XPacket packet)
        {
            this.Title = packet.ReadValue<string>();
            this.Content = packet.ReadValue<string>();
            this.EditYear = packet.ReadValue<ushort>();
            this.EditMonth = packet.ReadValue<ushort>();
            this.EditDay = packet.ReadValue<ushort>();
            this.EditHour = packet.ReadValue<ushort>();
            this.EditMinute = packet.ReadValue<ushort>();
            this.EditSecond = packet.ReadValue<ushort>();
            this.EditMilisecond = packet.ReadValue<ushort>();
            this.EditMicrosecond = packet.ReadValue<ushort>();
            return true;
        }

        public bool AppendToPacket(XPacket packet)
        {
            packet.WriteValue<string>(this.Title);
            packet.WriteValue<string>(this.Content);
            packet.WriteValue<ushort>(this.EditYear);
            packet.WriteValue<ushort>(this.EditMonth);
            packet.WriteValue<ushort>(this.EditDay);
            packet.WriteValue<ushort>(this.EditHour);
            packet.WriteValue<ushort>(this.EditMinute);
            packet.WriteValue<ushort>(this.EditSecond);
            packet.WriteValue<ushort>(this.EditMilisecond);
            packet.WriteValue<ushort>(this.EditMicrosecond);
            return true;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
