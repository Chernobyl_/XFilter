﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Gateway
{
    /// <summary>
    /// S->C CLIENT_GATEWAY_LAUNCHER_NEWS_REQUEST = 0x6104
    /// TESTME
    /// </summary>
    public class LauncherNewsRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties
        public SrOperationType OperationType
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public LauncherNewsRequestEntity()
        {
        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.OperationType = packet.ReadEnum<SrOperationType>();
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_GATEWAY_LAUNCHER_NEWS_REQUEST);
            
            packet.WriteEnum<SrOperationType>(this.OperationType);
            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
