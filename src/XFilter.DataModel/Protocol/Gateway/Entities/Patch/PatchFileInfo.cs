﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Gateway
{
    /// <summary>
    /// S->C SERVER_GATEWAY_DOWNLOAD_RESPONSE = 0xA100, Massive
    /// TESTME
    /// </summary>
    public class PatchFileInfo : IPacketInnerEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public uint ID
        { get; private set; }

        public string Name
        { get; set; }

        public string Path
        { get; set; }

        public uint Size
        { get; set; }

        public bool Pack
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// The ctor.
        /// </summary>
        public PatchFileInfo()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketInnerEntity
        
        public bool ReadFromPacket(XPacket packet)
        {
            this.ID = packet.ReadValue<uint>();
            this.Name = packet.ReadValue<string>();
            this.Path = packet.ReadValue<string>();
            this.Size = packet.ReadValue<uint>();
            this.Pack = packet.ReadValue<bool>();
            return true;
        }

        /// <summary>
        /// Appends stored data to given packet.
        /// </summary>
        /// <param name="packet">The packet.</param>
        public bool AppendToPacket(XPacket packet)
        {
            packet.WriteValue<uint>(this.ID);
            packet.WriteValue<string>(this.Name);
            packet.WriteValue<string>(this.Path);
            packet.WriteValue<uint>(this.Size);
            packet.WriteValue<bool>(this.Pack);
            return true;
        }


        #endregion

        //--------------------------------------------------------------------------
    }
}
