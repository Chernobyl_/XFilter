﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Gateway
{
    /// <summary>
    /// C->S CLIENT_GATEWAY_PATCH_REQUEST = 0x6100, Encrypted
    /// TESTME
    /// </summary>
    public class PatchRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public SrOperationType OperationType
        { get; private set; }

        public string ModuleName
        { get; private set; }

        public uint Version
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.OperationType = packet.ReadEnum<SrOperationType>();
            this.ModuleName = packet.ReadValue<string>();
            this.Version = packet.ReadValue<uint>();

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_GATEWAY_PATCH_REQUEST, true);

            packet.WriteEnum<SrOperationType>(this.OperationType);
            packet.WriteValue<string>(this.ModuleName);
            packet.WriteValue<uint>(this.Version);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
