﻿using NLog;
using System.Collections.Generic;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Gateway
{
    /// <summary>
    /// S->C SERVER_GATEWAY_DOWNLOAD_RESPONSE = 0xA100, Massive
    /// TESTME
    /// </summary>
    public class PatchInfoEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public PacketResultCode Code
        { get; set; }

        public PatchSubCode SubCode
        { get; set; }

        public string DownloadServerIp
        { get; set; }

        public ushort DownloadServerPort
        { get; set; }

        public uint Version
        { get; set; }

        public List<PatchFileInfo> Files
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public PatchInfoEntity()
        {
           
        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Files = new List<PatchFileInfo>();
            this.Code = packet.ReadEnum<PacketResultCode>();

            if (this.Code == PacketResultCode.Success)
                return true;

            if (this.Code == PacketResultCode.Error)
            {
                this.SubCode = packet.ReadEnum<PatchSubCode>();

                if (this.SubCode == PatchSubCode.Update)
                    ParsePatchMetadata(packet);
            }
            return true;
        }

        /// <summary>
        /// Prases the patch info packet data.
        /// </summary>
        /// <param name="packet">The packet.</param>
        private void ParsePatchMetadata(XPacket packet)
        {
            this.DownloadServerIp = packet.ReadValue<string>();
            this.DownloadServerPort = packet.ReadValue<ushort>();
            this.Version = packet.ReadValue<uint>();
            bool hasFile = packet.ReadValue<bool>();

            while (hasFile)
            {
                var file = new PatchFileInfo();
                file.ReadFromPacket(packet);
                this.Files.Add(file);

                hasFile = packet.ReadValue<bool>();
            }
        }

        /// <summary>
        /// Serializes entity into patch packet that contains patch file infos.
        /// </summary>
        /// <returns>Patch info packet.</returns>
        private XPacket SerializeAsPatchInfo()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_GATEWAY_DOWNLOAD_RESPONSE, false, true);

            packet.WriteEnum<PacketResultCode>(this.Code);
            packet.WriteEnum<PatchSubCode>(this.SubCode);

            packet.WriteValue<string>(this.DownloadServerIp);
            packet.WriteValue<ushort>(this.DownloadServerPort);

            packet.WriteValue<uint>(this.Version);

            bool hasFiles = (this.Files.Count > 0) ? true : false;
            packet.WriteValue<bool>(hasFiles);

            foreach(var file in this.Files)
            {
                file.AppendToPacket(packet);
                packet.WriteValue<bool>(true);
            }

            return packet;
        }

        /// <summary>
        /// Serializes stored data into packet.
        /// </summary>
        /// <returns>null if entity was not propertly parsed (or invalid state).</returns>
        public XPacket WriteToPacket()
        {
            //Patch info.
            if(this.Code == PacketResultCode.Success && this.SubCode == PatchSubCode.Update)
                return SerializeAsPatchInfo();

            //Client is up to date, or error.
            var packet = new XPacket((ushort)GameOpcode.SERVER_GATEWAY_DOWNLOAD_RESPONSE, false, true);

            packet.WriteEnum<PacketResultCode>(this.Code);

            if (this.Code != PacketResultCode.Success)
                packet.WriteEnum<PatchSubCode>(this.SubCode);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}