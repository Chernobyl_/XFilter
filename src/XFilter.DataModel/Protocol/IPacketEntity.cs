﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol
{
    /// <summary>
    /// Packet entity interface.
    /// </summary>
    interface IPacketEntity
    {
        /// <summary>
        /// Deserializes packet entity from the packet stream.
        /// </summary>
        /// <param name="packet">The packet stream.</param>
        /// <returns></returns>
        bool ReadFromPacket(XPacket packet);

        /// <summary>
        /// Writes packet entity to the packet.
        /// </summary>
        /// <returns>The packet stream.</returns>
        XPacket WriteToPacket();
    }
}
