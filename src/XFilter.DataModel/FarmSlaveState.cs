﻿namespace XFilter.DataModel
{
    /// <summary>
    /// Inter-process client state.
    /// FINISHME
    /// </summary>
    public sealed class FarmSlaveState
    {
        /// <summary>
        /// The unique session id.
        /// </summary>
        public int Id
        { get; set; }

        public string ApiKey
        { get; set; }

        public bool IsHandshaked
        { get; set; }

        public string IpStr
        { get; set; }

    

        public void Reset()
        {
            this.Id = 0;
            this.ApiKey = "123123";
            this.IsHandshaked = false;
            this.IpStr = null;
        }
    }
}
