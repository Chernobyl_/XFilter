﻿namespace XFilter.DataModel.Network.Ipc
{
    /// <summary>
    /// Inter-process slave auth response code.
    /// </summary>
    public enum IpcAuthResponseCode
    {
        Success,
        InvalidApiKey,
        InvalidHost,
    }
}
