﻿namespace XFilter.DataModel.Network.Ipc
{
    /// <summary>
    /// Inter-process operation codes
    /// </summary>
    public enum IpcOpcode
    {
        /// <summary>
        /// Slave authentication.
        /// </summary>
        Authentication = 0x1000,

        /// <summary>
        /// Farm module identify.
        /// </summary>
        ModuleIdentify = 0x2001,

        /// <summary>
        /// Slave info request / response
        /// </summary>
        SlaveServerUpdate = 0x1337, 

    }
}
