﻿namespace XFilter.DataModel
{
    /// <summary>
    /// Strings that are commonly used by the DTOs.
    /// </summary>
    internal static class JsonStrings
    {
        public const string ServerMetadata = "Server metadata";
        public const string NetEngineSettings = "Net engine settings";
        public const string AllowedIPs = "IP addresses allowed to connect";
        public const string RedirectionRules = "Redirection rules";
        public const string ModuleAddress = "Module address";
        public const string ModulePort = "Module port";
        public const string ConnectionTimeout = "Connection timeout";
    }
}
