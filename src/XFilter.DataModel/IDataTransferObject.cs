﻿using System.Data.Entity;

namespace XFilter.DataModel
{
    /// <summary>
    /// Service model data transfer object interface.
    /// </summary>
    public interface IDataTransferObject
    {
        /// <summary>
        /// Saves given context state to the database.
        /// </summary>
        /// <param name="context">The EF db context.</param>
        /// <param name="serverId">The referenced server ID.</param>
        void Save(DbContext context, int serverId);
    }
}
