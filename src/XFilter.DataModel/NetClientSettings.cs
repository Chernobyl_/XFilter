﻿using Newtonsoft.Json;
using System;
using XFilter.Shared.Interfaces;


namespace XFilter.DataModel
{
    /// <summary>
    /// Base configuration for every net client.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class NetClientSettings : IJsonSerializable
    {
        //--------------------------------------------------------------------------

        #region JSON serializable properties

        /// <summary>
        /// The remote IPv4 Address (xxx.xxx.xxx.xxx), or hostname.
        /// </summary>
        [JsonProperty(PropertyName = JsonStrings.ModuleAddress)]
        public string Address
        { get; private set; }

        /// <summary>
        /// The remote port.
        /// </summary>
        [JsonProperty(PropertyName = JsonStrings.ModulePort)]
        public int Port
        { get; private set; }

        /// <summary>
        /// The connection timeout (milliseconds).
        /// </summary>
        [JsonProperty(PropertyName = JsonStrings.ConnectionTimeout)]
        public int ConnectionTimeout
        { get; private set; }

        /// <summary>
        /// The reconnection interval (milliseconds).
        /// </summary>
        [JsonProperty(PropertyName = "Reconnection intervla")]
        public int ReconnectionInterval
        { get; private set; }


        /// <summary>
        /// The ping interval (milliseconds).
        /// </summary>
        [JsonProperty(PropertyName = "Ping interval")]
        public int PingInterval
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic / IJsonSerializable impl

        /// <summary>
        /// Setups the default configuration instance.
        /// </summary>
        public bool GenerateDefaults()
        {
            this.Address = "127.0.0.1";
            this.Port = 5000;
            this.ConnectionTimeout = 5000;
            this.ReconnectionInterval = 3000;
            this.PingInterval = 2500;

            return true;
        }


        public string Dump() =>
            throw new NotImplementedException();

        #endregion

        //--------------------------------------------------------------------------
    }
}
