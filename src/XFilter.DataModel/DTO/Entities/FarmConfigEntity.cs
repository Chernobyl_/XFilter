﻿using Newtonsoft.Json;
using System.Linq;
using XFilter.DataModel.DTO.TableObjects;
using XFilter.DataModel.Models;

namespace XFilter.DataModel.DTO.Entities
{
    /// <summary>
    /// Farm manager aka filter master server configuration entity class.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class FarmConfigEntity
    {
        //--------------------------------------------------------------------------

        #region Properties

        /// <summary>
        /// Server metadata.
        /// </summary>
        [JsonProperty(PropertyName = JsonStrings.ServerMetadata)]
        public ServerConfigDTO ServerConfig
        { get; private set; }
        
        /// <summary>
        /// Basic network engine configuration (bind interface address,
        /// port, buffer allocation, etc).
        /// </summary>
        [JsonProperty(PropertyName = JsonStrings.NetEngineSettings)]
        public NetEngineConfigDTO NetEngineConfig
        { get; private set; }

        /// <summary>
        /// Addresses (or hostnames?) that can connect to the farm server.
        /// </summary>
        [JsonProperty(PropertyName = JsonStrings.AllowedIPs)]
        public AllowedIPsDTO AllowedIPs
        { get; private set; }

        /// <summary>
        /// Farm manager specific config.
        /// </summary>
        [JsonProperty(PropertyName = "Farm server settings")]
        public FarmConfigDTO FarmConfig
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// Requred for proper JSON deserialization.
        /// </summary>
        public FarmConfigEntity()
        {
            this.ServerConfig = new ServerConfigDTO();
            this.NetEngineConfig = new NetEngineConfigDTO();
            this.AllowedIPs = new AllowedIPsDTO();
            this.FarmConfig = new FarmConfigDTO();
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Gets data transfer object entity from the model.
        /// </summary>
        /// <param name="entities">The entity.</param>
        /// <param name="serverId">Server id from _Servers table.</param>
        /// <returns>Data transfer object ENTITY.</returns>
        public static FarmConfigEntity FromModel(XServiceDBEntities entities, int serverId)
        {
            var serverRec = entities.C_Servers.FirstOrDefault(server => server.ID == serverId);

            //No referenced server found.

            if (serverRec == null)
                return null;

            var netRec = entities.C_NetEngineConfig.First(rec => rec.RefServerID == serverId);
            var allowedIpsRec = entities.C_AllowedIPs.Where(rec => rec.RefServerID == serverId);
            var farmRec = entities.C_FarmServers.First(rec => rec.RefServerID == serverId);

            var inst = new FarmConfigEntity()
            {
                ServerConfig = new ServerConfigDTO(serverRec),
                NetEngineConfig = new NetEngineConfigDTO(netRec),
                AllowedIPs = new AllowedIPsDTO(allowedIpsRec),
                FarmConfig = new FarmConfigDTO(farmRec),
            };

            return inst;
        }

        /// <summary>
        /// Saves given data transfer object into database (also flushes).
        /// </summary>
        /// <param name="entities">Database entities (referenced DB).</param>
        /// <param name="entity">The farm config entity class instance.</param>
        /// <param name="serverId">The referenced server ID.</param>
        public static void Save(XServiceDBEntities entities, FarmConfigEntity entity, int serverId)
        {
            entity.ServerConfig.Save(entities, serverId);
            entity.NetEngineConfig.Save(entities, serverId);
            entity.AllowedIPs.Save(entities, serverId);
            entity.FarmConfig.Save(entities, serverId);

            entities.SaveChanges();
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
