﻿using Newtonsoft.Json;
using System.Linq;
using XFilter.DataModel.DTO.TableObjects;
using XFilter.DataModel.Models;

namespace XFilter.DataModel.DTO.Entities
{
    /// <summary>
    /// Gateway relay server configuration entity class.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class GatewayConfigEntity
    {
        //--------------------------------------------------------------------------

        #region Properties

        /// <summary>
        /// Server metadata.
        /// </summary>
        [JsonProperty(PropertyName = JsonStrings.ServerMetadata)]
        public ServerConfigDTO ServerConfig
        { get; private set; }

        /// <summary>
        /// The gateway server network engine configuration.
        /// </summary>
        [JsonProperty(PropertyName = JsonStrings.NetEngineSettings)]
        public NetEngineConfigDTO NetEngineConfig
        { get; private set; }

        /// <summary>
        /// Redirection rules. Hostnames must be resolved on redirection rule creation.
        /// </summary>
        [JsonProperty(PropertyName = JsonStrings.RedirectionRules)]
        public RedirectionRuleArrayDTO RedirectionRules
        { get; private set; }

        /// <summary>
        /// IP addresses allowed to connect.
        /// </summary>
        [JsonProperty(PropertyName = JsonStrings.AllowedIPs)]
        public AllowedIPsDTO AllowedIPs
        { get; private set; }

        /// <summary>
        /// Gateway-only configuration info.
        /// </summary>
        [JsonProperty(PropertyName = "Gateway server settings")]
        public GatewayConfigDTO GatewayConfig
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// Requred for proper JSON deserialization.
        /// </summary>
        private GatewayConfigEntity()
        {
            this.ServerConfig = new ServerConfigDTO();
            this.NetEngineConfig = new NetEngineConfigDTO();
            this.RedirectionRules = new RedirectionRuleArrayDTO();
            this.AllowedIPs = new AllowedIPsDTO();
            this.GatewayConfig = new GatewayConfigDTO();
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Gets data transfer object entity from the model.
        /// </summary>
        /// <param name="entities">The entity.</param>
        /// <param name="serverId">Server id from _Servers table.</param>
        /// <returns>Data transfer object ENTITY.</returns>
        public static GatewayConfigEntity FromModel(XServiceDBEntities entities, int serverId)
        {
            var serverRec = entities.C_Servers.FirstOrDefault(server => server.ID == serverId);

            //No referenced server found.
            if (serverRec == null)
                return null;

            var netRec = entities.C_NetEngineConfig.FirstOrDefault(rec => rec.RefServerID == serverId);
            var rulesRec = entities.C_RedirectionRules.Where(rec => rec.RefServerID == serverId);
            var allowedIpsRec = entities.C_AllowedIPs.Where(rec => rec.RefServerID == serverId);
            var gatewayRec = entities.C_GatewayServers.FirstOrDefault(rec => rec.RefServerID == serverId);

            var inst = new GatewayConfigEntity()
            {
                ServerConfig = new ServerConfigDTO(serverRec),
                NetEngineConfig = new NetEngineConfigDTO(netRec),
                RedirectionRules = new RedirectionRuleArrayDTO(rulesRec),
                AllowedIPs = new AllowedIPsDTO(allowedIpsRec),
                GatewayConfig = new GatewayConfigDTO(gatewayRec),
            };

            return inst;
        }

        /// <summary>
        /// Saves govem data transfer object entity into given database / reference server id.
        /// Flushes to database.
        /// </summary>
        /// <param name="entities">The database model entities.</param>
        /// <param name="entity">The data transfer object.</param>
        /// <param name="serverId">The referenced server id.</param>
        public static void Save(XServiceDBEntities entities, GatewayConfigEntity entity, int serverId)
        {
            entity.ServerConfig.Save(entities, serverId);
            entity.NetEngineConfig.Save(entities, serverId);
            entity.AllowedIPs.Save(entities, serverId);
            entity.RedirectionRules.Save(entities, serverId);
            entity.GatewayConfig.Save(entities, serverId);

            entities.SaveChanges();
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
