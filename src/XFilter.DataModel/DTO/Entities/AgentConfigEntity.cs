﻿using Newtonsoft.Json;
using System.Linq;
using XFilter.DataModel.DTO.TableObjects;
using XFilter.DataModel.Models;

namespace XFilter.DataModel.DTO.Entities
{
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class AgentConfigEntity
    {
        //--------------------------------------------------------------------------

        #region Properties

        /// <summary>
        /// Basic server information.
        /// </summary>
        [JsonProperty(PropertyName = JsonStrings.ServerMetadata)]
        public ServerConfigDTO ServerConfig
        { get; private set; }

        /// <summary>
        /// The network engine config. Does not include target module address.
        /// </summary>
        [JsonProperty(PropertyName = JsonStrings.NetEngineSettings)]
        public NetEngineConfigDTO NetEngineConfig
        { get; private set; }

        /// <summary>
        /// IP addresses that are allowed to connect.
        /// </summary>
        [JsonProperty(PropertyName = JsonStrings.AllowedIPs)]
        public AllowedIPsDTO AllowedIPs
        { get; private set;}

        /// <summary>
        /// Agent-only config, systems are enabled/disabled from here.
        /// </summary>
        [JsonProperty(PropertyName = "Agent server specific settings")]
        public AgentConfigDTO AgentConfig
        { get; private set; }

        /// <summary>
        /// Game delays, for ex: stall delay, logout delay.
        /// </summary>
        [JsonProperty(PropertyName = "Game delay settings")]
        public AgentDelaysDTO Delays
        { get; private set; }

        /// <summary>
        /// Version specific fixes (maybe we want to support non-vsro188 server files).
        /// </summary>
        [JsonProperty(PropertyName = "Server file version specific fixes")]
        public VersionSpecificFixesDTO VersionSpecificFixes
        { get; private set; }

        /// <summary>
        /// Auto notice message configuration.
        /// </summary>
        [JsonProperty(PropertyName = "Auto-notice settings")]
        public AutoNoticeArrayDTO AutoNotices
        { get; private set; }

        /// <summary>
        /// Alchemy related configuration.
        /// </summary>
        [JsonProperty(PropertyName = "Alchemy settings")]
        public AgentAlchemyConfigDTO AlchemyConfig
        { get; private set; }


        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// Requred for proper JSON deserialization.
        /// </summary>
        public AgentConfigEntity()
        {
            this.ServerConfig = new ServerConfigDTO();
            this.NetEngineConfig = new NetEngineConfigDTO();
            this.AllowedIPs = new AllowedIPsDTO();
            this.AgentConfig = new AgentConfigDTO();
            this.Delays = new AgentDelaysDTO();
            this.VersionSpecificFixes = new VersionSpecificFixesDTO();
            this.AutoNotices = new AutoNoticeArrayDTO();
            this.AlchemyConfig = new AgentAlchemyConfigDTO();
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Gets data transfer object entity from the model.
        /// </summary>
        /// <param name="entities">The entity.</param>
        /// <param name="serverId">Server id from _Servers table.</param>
        /// <returns>Data transfer object ENTITY.</returns>
        public static AgentConfigEntity FromModel(XServiceDBEntities entities, int serverId)
        {
            var serverRec = entities.C_Servers.FirstOrDefault(server => server.ID == serverId);
          
            //No referenced server found.
            if (serverRec == null)
                return null;

            var netRec = entities.C_NetEngineConfig.First(net => net.RefServerID == serverId);
            var allowedIpsRec = entities.C_AllowedIPs.Where(ip => ip.RefServerID == serverId);
            var agentRec = entities.C_AgentServers.First(rec => rec.RefServerID == serverId);
            var delaysRec = entities.C_GameDelays.First(rec => rec.RefServerID == serverId);
            var verSpecificRec = entities.C_VersionSpecificFixes.First(rec => rec.RefServerID == serverId);
            var autoNoticeRec = entities.C_AutoNotice.Where(rec => rec.RefServerID == serverId);
            var alchemyCfgRec = entities.C_AlchemyConfig.First(rec => rec.RefServerID == serverId);

            var inst = new AgentConfigEntity()
            {
                ServerConfig = new ServerConfigDTO(serverRec),
                NetEngineConfig = new NetEngineConfigDTO(netRec),
                AllowedIPs = new AllowedIPsDTO(allowedIpsRec),
                AgentConfig = new AgentConfigDTO(agentRec),
                Delays = new AgentDelaysDTO(delaysRec),
                VersionSpecificFixes = new VersionSpecificFixesDTO(verSpecificRec),
                AutoNotices = new AutoNoticeArrayDTO(autoNoticeRec),
                AlchemyConfig = new AgentAlchemyConfigDTO(alchemyCfgRec)
            };

            return inst;
        }

        /// <summary>
        /// Saves & flushes given agent config entity into database.
        /// </summary>
        /// <param name="entities">The EF database entities.</param>
        /// <param name="entity">The agent configuration entity.</param>
        /// <param name="serverId">The referenced server id.</param>
        public static void Save(XServiceDBEntities entities, AgentConfigEntity entity, int serverId)
        {
            entity.ServerConfig.Save(entities, serverId);
            entity.NetEngineConfig.Save(entities, serverId);
            entity.AllowedIPs.Save(entities, serverId);
            entity.AgentConfig.Save(entities, serverId);
            entity.Delays.Save(entities, serverId);
            entity.VersionSpecificFixes.Save(entities, serverId);
            entity.AutoNotices.Save(entities, serverId);
            entity.AlchemyConfig.Save(entities, serverId);

            entities.SaveChanges();
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
