﻿using Newtonsoft.Json;
using System.Linq;
using XFilter.DataModel.DTO.TableObjects;
using XFilter.DataModel.Models;

namespace XFilter.DataModel.DTO.Entities
{
    /// <summary>
    /// Gateway relay server configuration entity class.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class DownloadConfigEntity
    {
        //--------------------------------------------------------------------------

        #region Properties

        /// <summary>
        /// Server metadata.
        /// </summary>
        [JsonProperty(PropertyName = JsonStrings.ServerMetadata)]
        public ServerConfigDTO ServerConfig
        { get; private set; }

        /// <summary>
        /// The gateway server network engine configuration.
        /// </summary>
        [JsonProperty(PropertyName = JsonStrings.NetEngineSettings)]
        public NetEngineConfigDTO NetEngineConfig
        { get; private set; }

        /// <summary>
        /// IP addresses allowed to connect.
        /// </summary>
        [JsonProperty(PropertyName = JsonStrings.AllowedIPs)]
        public AllowedIPsDTO AllowedIPs
        { get; private set; }

        /// <summary>
        /// Download-only configuration info.
        /// </summary>
        [JsonProperty(PropertyName = "Download server settings")]
        public DownloadConfigDTO DownloadConfig
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// Requred for proper JSON deserialization.
        /// </summary>
        private DownloadConfigEntity()
        {
            this.ServerConfig = new ServerConfigDTO();
            this.NetEngineConfig = new NetEngineConfigDTO();
            this.AllowedIPs = new AllowedIPsDTO();
            this.DownloadConfig = new DownloadConfigDTO();
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Gets data transfer object entity from the model.
        /// </summary>
        /// <param name="entities">The entity.</param>
        /// <param name="serverId">Server id from _Servers table.</param>
        /// <returns>Data transfer object ENTITY.</returns>
        public static DownloadConfigEntity FromModel(XServiceDBEntities entities, int serverId)
        {
            var serverRec = entities.C_Servers.FirstOrDefault(server => server.ID == serverId);

            //No referenced server found.
            if (serverRec == null)
                return null;

            var netRec = entities.C_NetEngineConfig.First(rec => rec.RefServerID == serverId);
            var rulesRec = entities.C_RedirectionRules.Where(rec => rec.RefServerID == serverId);
            var allowedIpsRec = entities.C_AllowedIPs.Where(rec => rec.RefServerID == serverId);
            var downloadRec = entities.C_DownloadServers.FirstOrDefault(rec => rec.RefServerID == serverId);

            var inst = new DownloadConfigEntity()
            {
                ServerConfig = new ServerConfigDTO(serverRec),
                NetEngineConfig = new NetEngineConfigDTO(netRec),
                AllowedIPs = new AllowedIPsDTO(allowedIpsRec),
                DownloadConfig = new DownloadConfigDTO(downloadRec),
            };

            return inst;
        }

        /// <summary>
        /// Saves govem data transfer object entity into given database / reference server id.
        /// Flushes to database.
        /// </summary>
        /// <param name="entities">The database model entities.</param>
        /// <param name="entity">The data transfer object.</param>
        /// <param name="serverId">The referenced server id.</param>
        public static void Save(XServiceDBEntities entities, DownloadConfigEntity entity, int serverId)
        {
            entity.ServerConfig.Save(entities, serverId);
            entity.NetEngineConfig.Save(entities, serverId);
            entity.AllowedIPs.Save(entities, serverId);
            entity.DownloadConfig.Save(entities, serverId);
            entities.SaveChanges();
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
