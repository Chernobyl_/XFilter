﻿using Newtonsoft.Json;
using System.Data.Entity;
using System.Linq;
using XFilter.DataModel.Models;


namespace XFilter.DataModel.DTO.TableObjects
{
    /// <summary>
    /// The gateway-only data transfer object class.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class GatewayConfigDTO : IDataTransferObject
    {
        //--------------------------------------------------------------------------

        #region Properties

        /// <summary>
        /// The target module ip address.
        /// </summary>
        [JsonProperty(PropertyName = JsonStrings.ModuleAddress)]
        public string ModuleAddress
        { get; set; }

        /// <summary>
        /// The target module port.
        /// </summary>
        [JsonProperty(PropertyName = JsonStrings.ModulePort)]
        public int? ModulePort
        { get; set; }

        /// <summary>
        /// Defines if auto captcha is enabled.
        /// </summary>
        [JsonProperty(PropertyName = "Enable or disable auto captcha")]
        public bool? EnableAutoCaptcha
        { get;  set; }

        /// <summary>
        /// Defines the auto captcha value.
        /// </summary>
        [JsonProperty(PropertyName = "The auto captcha value")]
        public string AutoCaptchaValue
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// The GatewayConfigDTO constructor (from EF db record).
        /// </summary>
        /// <param name="config">The EF db record.</param>
        public GatewayConfigDTO(C_GatewayServers config)
        {
            this.ModuleAddress = config.ModuleAddress;
            this.ModulePort = config.ModulePort;
            this.EnableAutoCaptcha = config.AutoCaptcha;
            this.AutoCaptchaValue = config.AutoCaptchaString;
        }


        /// <summary>
        /// Requred for proper JSON deserialization.
        /// </summary>
        public GatewayConfigDTO()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic
        /// <summary>
        /// Saves settings into database.
        /// Does <b>not</b> flush. Flush must be done from AgentConfigEntity.
        /// </summary>
        /// <param name="context">The database context.</param>
        /// <param name="serverId">The referenced server id.</param>
        public void Save(DbContext context, int serverId)
        {
            var entities = context as XServiceDBEntities;
            var gateRec = entities.C_GatewayServers.First(rule => rule.RefServerID == serverId);
            gateRec.AutoCaptcha = this.EnableAutoCaptcha;
            gateRec.AutoCaptchaString = this.AutoCaptchaValue;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
