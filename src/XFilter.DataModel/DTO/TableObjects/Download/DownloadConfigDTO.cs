﻿using Newtonsoft.Json;
using System;
using System.Data.Entity;
using XFilter.DataModel.Models;


namespace XFilter.DataModel.DTO.TableObjects
{
    /// <summary>
    /// The download-only data transfer object class.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class DownloadConfigDTO : IDataTransferObject
    {
        //--------------------------------------------------------------------------

        #region Properties

        /// <summary>
        /// The target module ip address.
        /// </summary>
        [JsonProperty(PropertyName = JsonStrings.ModuleAddress)]
        public string ModuleAddress
        { get; set; }

        /// <summary>
        /// The target module port.
        /// </summary>
        [JsonProperty(PropertyName = JsonStrings.ModulePort)]
        public int? ModulePort
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// The DownloadConfigDTO constructor (from EF db record).
        /// </summary>
        /// <param name="config">The EF db record.</param>
        public DownloadConfigDTO(C_DownloadServers config)
        {
            this.ModuleAddress = config.ModuleAddress;
            this.ModulePort = config.ModulePort;
        }


        /// <summary>
        /// Requred for proper JSON deserialization.
        /// </summary>
        public DownloadConfigDTO()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Saves settings into database.
        /// Does <b>not</b> flush. Flush must be done from AgentConfigEntity.
        /// </summary>
        /// <param name="context">The database context.</param>
        /// <param name="serverId">The referenced server id.</param>
        public void Save(DbContext context, int serverId)
        {
            var entities = context as XServiceDBEntities;
            throw new NotImplementedException();
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
