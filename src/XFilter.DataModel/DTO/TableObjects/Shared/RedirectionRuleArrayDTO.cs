﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using XFilter.DataModel.Models;

namespace XFilter.DataModel.DTO.TableObjects
{
    /// <summary>
    /// Redirection rule data transfer object class. Used for redirecting clients
    /// to custom ip/port (typically our own "fake" external port). Currently will be only
    /// used for gateway and download server.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class RedirectionRuleArrayDTO : IDataTransferObject
    {
        //--------------------------------------------------------------------------

        #region Properties

        /// <summary>
        /// The list of redirection rules.
        /// </summary>
        [JsonProperty(PropertyName = JsonStrings.RedirectionRules)]
        public List<RedirectionRuleDTO> Rules
        { get;  private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// The RedirectionRuleArrayDTO constructor (from array of EF db records).
        /// </summary>
        /// <param name="rules">The EF db records.</param>
        public RedirectionRuleArrayDTO(IQueryable<C_RedirectionRules> rules)
        {
            this.Rules = new List<RedirectionRuleDTO>();
            foreach (var rule in rules)
            {
                this.Rules.Add(
                    new RedirectionRuleDTO(rule.SourceAddr, rule.SourcePort, rule.TargetAddr, rule.TargetPort)
                    );
            }
        }

        /// <summary>
        /// Requred for proper JSON deserialization.
        /// </summary>
        public RedirectionRuleArrayDTO()
        {
            this.Rules = new List<RedirectionRuleDTO>();
        }


        #endregion

        //--------------------------------------------------------------------------

        #region Logic
        /// <summary>
        /// Saves settings into database.
        /// Does <b>not</b> flush. Flush must be done from AgentConfigEntity.
        /// </summary>
        /// <param name="context">The database context.</param>
        /// <param name="serverId">The referenced server id.</param>
        public void Save(DbContext context, int serverId)
        {
            var entities = context as XServiceDBEntities;
            var rulesRec = entities.C_RedirectionRules.Where(ip => ip.RefServerID == serverId);
            entities.C_RedirectionRules.RemoveRange(rulesRec);

            foreach (var rule in this.Rules)
            {
                entities.C_RedirectionRules.Add(new C_RedirectionRules()
                {
                    RefServerID = serverId,
                    SourceAddr = rule.SrcAddr,
                    SourcePort = rule.SrcPort,
                    TargetAddr = rule.DestAddr,
                    TargetPort = rule.DestPort
                });
            }
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
