﻿using Newtonsoft.Json;
using System.Data.Entity;
using System.Linq;
using XFilter.DataModel.Models;

namespace XFilter.DataModel.DTO.TableObjects
{
    /// <summary>
    /// Network engine config data transfer object. Does not include the target module address
    /// and port since some modules might be not relays (for instance - our master aka FarmManager 
    /// only accepts connections, but does not relay them to the real server).
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class NetEngineConfigDTO : IDataTransferObject
    {
        //--------------------------------------------------------------------------

        #region Properties

        /// <summary>
        /// Bind address to listen on (if multiple network interfaces).
        /// </summary>
        [JsonProperty(PropertyName = "Bind address")]
        public string BindAddress
        { get; private set; }

        /// <summary>
        /// Bind address to listen on.
        /// </summary>
        [JsonProperty(PropertyName = "Bind port")]
        public int? BindPort
        { get; set; }

        /// <summary>
        /// Maximum concurent client connections (over limit will be dropped).
        /// </summary>
        [JsonProperty(PropertyName = "Maximum server-wide connections")]
        public int? MaxConnections
        { get; set; }

        /// <summary>
        /// Maximum concurent client connections per single ip address.
        /// </summary>
        [JsonProperty(PropertyName = "Maximum connections per ip address")]
        public int? MaxConnsPerIp
        { get; set; }

        /// <summary>
        /// Time (milliseconds) after which client connection is determined as 
        /// hanging. Connection will be dropped.
        /// </summary>
        [JsonProperty(PropertyName = "Context pulse timeout")]
        public int? ContextPulseTimeout
        { get; set; }

        /// <summary>
        /// Packet log size (for debug and exploit logging purposes).
        /// </summary>
        [JsonProperty(PropertyName = "Packet log size")]
        public int? PacketLogSize
        { get; set; }

        /// <summary>
        /// How many threads are processing work (packets?). Usually set to
        /// cpu count * 2. IMPORTANT.
        /// </summary>
        [JsonProperty(PropertyName = "Work processing task count")]
        public int WorkQueueProcessTaskCount
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// NetEngineConfigDTO constructor (from EF db record).
        /// </summary>
        /// <param name="cfg">The EF db record.</param>
        public NetEngineConfigDTO(C_NetEngineConfig cfg)
        {
            this.BindAddress = cfg.BindAddress;
            this.BindPort = cfg.BindPort;
            this.MaxConnections = cfg.MaximumConnections;
            this.MaxConnsPerIp = cfg.MaximumConnectionsPerIp;
            this.ContextPulseTimeout = cfg.ContextPulseTimeout;
            this.PacketLogSize = cfg.PacketLogSize;
        }

        /// <summary>
        /// Requred for proper JSON deserialization.
        /// </summary>
        public NetEngineConfigDTO()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Saves settings into database.
        /// Does <b>not</b> flush. Flush must be done from AgentConfigEntity.
        /// </summary>
        /// <param name="context">The database context.</param>
        /// <param name="serverId">The referenced server id.</param>
        public void Save(DbContext context, int serverId)
        {
            var entities = context as XServiceDBEntities;
            var netRec = entities.C_NetEngineConfig.First(net => net.RefServerID == serverId);

            netRec.BindAddress = this.BindAddress;
            netRec.BindPort = this.BindPort;
            netRec.MaximumConnections = this.MaxConnections;
            netRec.MaximumConnectionsPerIp = this.MaxConnsPerIp;
            netRec.ContextPulseTimeout = this.ContextPulseTimeout;
            netRec.PacketLogSize = this.PacketLogSize;
            netRec.WorkProcessTaskCount = this.WorkQueueProcessTaskCount;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
