﻿using Newtonsoft.Json;
using System;
using System.Data.Entity;
using XFilter.DataModel.Models;

namespace XFilter.DataModel.DTO.TableObjects
{
    /// <summary>
    /// Redirection rule array item data transfer object class.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class RedirectionRuleDTO : IDataTransferObject
    {
        //--------------------------------------------------------------------------

        #region Properties

        /// <summary>
        /// The source address.
        /// </summary>
        [JsonProperty(PropertyName = "Source address")]
        public string SrcAddr
        { get; set; }

        /// <summary>
        /// The source port.
        /// </summary>
        [JsonProperty(PropertyName = "Source port")]
        public int SrcPort
        { get; set; }

        /// <summary>
        /// The new, spoofed address.
        /// </summary>
        [JsonProperty(PropertyName = "Destination address")]
        public string DestAddr
        { get; set; }

        /// <summary>
        /// The new, spoofed port.
        /// </summary>
        [JsonProperty(PropertyName = "Destination port")]
        public int DestPort
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// The RedirectionRuleDTO constructor (from user args).
        /// </summary>
        /// <param name="srcAddr">The source ip address.</param>
        /// <param name="srcPort">The source port.</param>
        /// <param name="destAddr">The new ip address.</param>
        /// <param name="destPort">The new port.</param>
        public RedirectionRuleDTO(string srcAddr, int srcPort, string destAddr, int destPort)
        {
            this.SrcAddr = srcAddr;
            this.SrcPort = srcPort;
            this.DestAddr = destAddr;
            this.DestPort = destPort;
        }


        /// <summary>
        /// Requred for proper JSON deserialization.
        /// </summary>
        public RedirectionRuleDTO()
        {

        }


        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Saves settings into database.
        /// Does <b>not</b> flush. Flush must be done from AgentConfigEntity.
        /// </summary>
        /// <param name="context">The database context.</param>
        /// <param name="serverId">The referenced server id.</param>
        public void Save(DbContext context, int serverId)
        {
            var entities = context as XServiceDBEntities;
            throw new NotImplementedException();
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
