﻿using Newtonsoft.Json;
using System.Data.Entity;
using System.Linq;
using XFilter.DataModel.Models;

namespace XFilter.DataModel.DTO.TableObjects
{
    /// <summary>
    /// Server metadata data transfer object class.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class ServerConfigDTO : IDataTransferObject
    {
        //--------------------------------------------------------------------------

        #region Properties

        /// <summary>
        /// The unique server id (RefServerID).
        /// </summary>
        [JsonProperty(PropertyName = "ID")]
        public int ID
        { get; private set; }

        /// <summary>
        /// The server name (Bloody, Send Me Nodes Online).
        /// </summary>
        [JsonProperty(PropertyName = "Server name")]
        public string ServerName
        { get; set; }

        /// <summary>
        /// The server type.
        /// 0 = Farm
        /// 1 = Gateway
        /// 2 = Download
        /// 3 = Agent
        /// </summary>
        [JsonProperty(PropertyName = "Server type")]
        public ServerType ServerType
        { get; set; }

        /// <summary>
        /// Defines if only those ips in the list of allowed ips will be able to connect.
        /// Set to false on production.
        /// </summary>
        [JsonProperty(PropertyName = "Enable accepting only only whitelisted IPs")]
        public bool OnlyAllowedIPs
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// The ServerConfigDTO constructor (from EF db record).
        /// </summary>
        /// <param name="server">The EF db record.</param>
        public ServerConfigDTO(C_Servers server)
        {
            this.ID = server.ID;
            this.ServerName = server.Name;
            this.ServerType = (ServerType)server.ServerType;
            this.OnlyAllowedIPs = server.OnlyAllowedIPs;
        }


        /// <summary>
        /// Requred for proper JSON deserialization.
        /// </summary>
        public ServerConfigDTO()
        {
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Saves settings into database.
        /// Does <b>not</b> flush. Flush must be done from AgentConfigEntity.
        /// </summary>
        /// <param name="context">The database context.</param>
        /// <param name="serverId">The referenced server id.</param>
        public void Save(DbContext context, int serverId)
        {
            var entities = context as XServiceDBEntities;
            var serverRec = entities.C_Servers.First(server => server.ID == serverId);

            serverRec.ID = this.ID;
            serverRec.Name = this.ServerName;
            serverRec.ServerType = this.ServerType;
            serverRec.OnlyAllowedIPs = this.OnlyAllowedIPs;
        }

        #endregion Logic

        //--------------------------------------------------------------------------
    }
}
