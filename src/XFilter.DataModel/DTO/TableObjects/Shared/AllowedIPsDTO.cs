﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using XFilter.DataModel.Models;

namespace XFilter.DataModel.DTO.TableObjects
{
    /// <summary>
    /// Data tramsfer object for IP addresses allowed to connect to our module.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class AllowedIPsDTO : IDataTransferObject
    {
        //--------------------------------------------------------------------------

        #region Properties

        /// <summary>
        /// The list of allowed ip addresses.
        /// </summary>
        [JsonProperty(PropertyName = JsonStrings.AllowedIPs)]
        public ICollection<string> Addresses
        { get; private set; }


        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// The AllowedIPsDTO constructor (from array of C_AllowedIPs records).
        /// </summary>
        /// <param name="ips">Array of C_AllowedIPs.</param>
        public AllowedIPsDTO(IQueryable<C_AllowedIPs> ips)
        {
            this.Addresses = new List<string>();

            foreach(var item in ips)
                this.Addresses.Add(item.Address);
        }

        /// <summary>
        /// Requred for proper JSON deserialization.
        /// </summary>
        public AllowedIPsDTO()
        {
            this.Addresses = new List<string>();
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Saves settings into database.
        /// Does <b>not</b> flush. Flush must be done from AgentConfigEntity.
        /// </summary>
        /// <param name="context">The database context.</param>
        /// <param name="serverId">The referenced server id.</param>
        public void Save(DbContext context, int serverId)
        {
            var entities = context as XServiceDBEntities;
            var ipRec = entities.C_AllowedIPs.Where(ip => ip.RefServerID == serverId);
            entities.C_AllowedIPs.RemoveRange(ipRec);

            foreach(var ipAddr in this.Addresses)
            {
                entities.C_AllowedIPs.Add(new C_AllowedIPs()
                {
                    RefServerID = serverId,
                    Address = ipAddr
                });
            }
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
