﻿using Newtonsoft.Json;
using System.Data.Entity;
using System.Linq;
using XFilter.DataModel.Models;


namespace XFilter.DataModel.DTO.TableObjects
{
    /// <summary>
    /// Service manager (SMC) client config data transfer object class.
    /// Will be used for automatic module startup on crash etc possibly.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class ServiceManagerConfigDTO : IDataTransferObject
    {
        //--------------------------------------------------------------------------

        #region Properties

        /// <summary>
        /// The SMC username.
        /// </summary>
        [JsonProperty(PropertyName = "Username")]
        public string Username
        { get; private set; }

        /// <summary>
        /// The SMC password.
        /// </summary>
        [JsonProperty(PropertyName = "Password")]
        public string Password
        { get; private set; }


        /// <summary>
        /// The global manager ip address.
        /// </summary>
        [JsonProperty(PropertyName = "Global manager address")]
        public string Address
        { get; private set; }

        /// <summary>
        /// The global manager port.
        /// </summary>
        [JsonProperty(PropertyName = "Global manager port")]
        public int? Port
        { get; private set; }


        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// The ServiceManagerConfigDTO constructor (from EF db record).
        /// </summary>
        /// <param name="records"></param>
        public ServiceManagerConfigDTO(C_ServiceManagerClients record)
        {
            this.Username = record.Username;
            this.Password = record.Password;
            this.Address = record.Address;
            this.Port = record.Port;
        }

        /// <summary>
        /// Requred for proper JSON deserialization.
        /// </summary>
        public ServiceManagerConfigDTO()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Saves settings into database.
        /// Does <b>not</b> flush. Flush must be done from AgentConfigEntity.
        /// </summary>
        /// <param name="context">The database context.</param>
        /// <param name="serverId">The referenced server id.</param>
        public void Save(DbContext context, int serverId)
        {
            var entities = context as XServiceDBEntities;
            var smcConfigRec = entities.C_ServiceManagerClients.FirstOrDefault(farm => farm.RefServerID == serverId);
            if (smcConfigRec == null)
                return;
            
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
