﻿using Newtonsoft.Json;
using System.Data.Entity;
using System.Linq;
using XFilter.DataModel.Models;

namespace XFilter.DataModel.DTO.TableObjects
{
    /// <summary>
    /// The auto notice data transfer object class.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class AutoNoticeDTO : IDataTransferObject
    {
        //--------------------------------------------------------------------------

        #region Public properties

        [JsonProperty(PropertyName = "Interval (milliseconds)")]
        public int? Interval
        { get; set; }

        [JsonProperty(PropertyName = "Text")]
        public string Text
        { get; set; }



        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// The AutoNoticeDTO constructor.
        /// </summary>
        /// <param name="record">The EF database record.</param>
        public AutoNoticeDTO(C_AutoNotice record) => 
            new AutoNoticeDTO(record.Interval, record.Text);

        /// <summary>
        /// The AutoNoticeDTO constructor (from supplied user arguments).
        /// </summary>
        /// <param name="interval">Notice interval, in milliseconds.</param>
        /// <param name="text">The notice text.</param>
        public AutoNoticeDTO(int? interval, string text)
        {
            this.Interval = interval;
            this.Text = text;
        }

        /// <summary>
        /// Requred for proper JSON deserialization.
        /// </summary>
        public AutoNoticeDTO()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Saves settings into database.
        /// Does <b>not</b> flush. Flush must be done from AgentConfigEntity.
        /// </summary>
        /// <param name="context">The database context.</param>
        /// <param name="serverId">The referenced server id.</param>
        public void Save(DbContext context, int serverId)
        {
            var entities = context as XServiceDBEntities;
            var record = entities.C_AutoNotice.First(item => item.RefServerID == serverId);

            record.Interval = this.Interval;
            record.Text = this.Text;
        }

        #endregion

        //--------------------------------------------------------------------------

    }
}
