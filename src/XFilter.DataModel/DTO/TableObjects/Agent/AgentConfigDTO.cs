﻿using Newtonsoft.Json;
using System.Data.Entity;
using System.Linq;
using XFilter.DataModel.Models;

namespace XFilter.DataModel.DTO.TableObjects
{
    /// <summary>
    /// Agent server config data transfer object class.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class AgentConfigDTO : IDataTransferObject
    {
        //--------------------------------------------------------------------------

        #region Properties

        /// <summary>
        /// The target agent module address.
        /// </summary>
        [JsonProperty(PropertyName = JsonStrings.ModuleAddress)]
        public string ModuleAddress
        { get; set; }

        /// <summary>
        /// The target agent module port.
        /// </summary>
        [JsonProperty(PropertyName = JsonStrings.ModulePort)]
        public int ModulePort
        { get; set; }

        /// <summary>
        /// Defines if auto notices are enabled on this agent server.
        /// </summary>
        [JsonProperty(PropertyName = "Enable or disable auto-notices")]
        public bool? EnableAutoNotices
        { get; set; }

        /// <summary>
        /// Defines if chat logging is enabled on this agent server.
        /// </summary>
        [JsonProperty(PropertyName = "Enable or disable chat logging")]
        public bool? EnableChatLog
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors
        
        /// <summary>
        /// The agent config data transfer object constructor (from DB model record).
        /// </summary>
        /// <param name="cfg">The EF data model record.</param>
        public AgentConfigDTO(C_AgentServers cfg)
        {
            this.ModuleAddress = cfg.ModuleAddress;
            this.ModulePort = cfg.ModulePort;
            this.EnableAutoNotices = cfg.EnableAutoNotices;
            this.EnableChatLog = cfg.EnableChatLog;
        }

        /// <summary>
        /// Requred for proper JSON deserialization.
        /// </summary>
        public AgentConfigDTO()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Saves settings into database.
        /// Does <b>not</b> flush. Flush must be done from AgentConfigEntity.
        /// </summary>
        /// <param name="context">The database context.</param>
        /// <param name="serverId">The referenced server id.</param>
        public void Save(DbContext context, int serverId)
        {
            var entities = context as XServiceDBEntities;
            var agentRec = entities.C_AgentServers.First(agent => agent.ID == serverId);

            agentRec.ModuleAddress = this.ModuleAddress;
            agentRec.ModulePort = this.ModulePort;
            agentRec.EnableAutoNotices = this.EnableAutoNotices;
            agentRec.EnableChatLog = this.EnableChatLog;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
