﻿using Newtonsoft.Json;
using System.Data.Entity;
using System.Linq;
using XFilter.DataModel.Models;

namespace XFilter.DataModel.DTO.TableObjects
{
    /// <summary>
    /// Agent server delays (exchange, logout, stall ETC) data transfer object class.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class AgentDelaysDTO : IDataTransferObject
    {
        //--------------------------------------------------------------------------

        #region Public properties

        /// <summary>
        /// Defines if logout delay system is enabled.
        /// </summary>
        [JsonProperty(PropertyName = "Enable or disable logout delay")]
        public bool? EnableLogoutDelay
        { get; set; }

        /// <summary>
        /// The logout delay in milliseconds.
        /// </summary>
        [JsonProperty(PropertyName = "Logout delay (in milliseconds)")]
        public int? LogoutDelay
        { get; set; }

        /// <summary>
        /// The logout delay message.
        /// 'You are not allowed to exchange for {delay} milliseconds.'
        /// </summary>
        [JsonProperty(PropertyName = "Logout delay message")]
        public string LogoutDelayMsg
        { get; set; }

        /// <summary>
        /// Defines if the stall delay system is enabled.
        /// </summary>
        [JsonProperty(PropertyName = "Enable or disable stall delay")]
        public bool? EnableStallDelay
        { get; set; }

        /// <summary>
        /// The stall creation delay in milliseconds.
        /// </summary>
        [JsonProperty(PropertyName = "Stall creation delay (in milliseconds)")]
        public int? StallDelay
        { get; set; }

        /// <summary>
        /// The stall creation delay message.
        /// 'You are not allowed to create stall for {delay} milliseconds.'
        /// </summary>
        [JsonProperty(PropertyName = "Stall delay message")]
        public string StallDelayMsg
        { get; set; }


        #endregion

        //--------------------------------------------------------------------------

        #region Constructors
        
        /// <summary>
        /// The agent delay data transfer object constructor (from EF db table record).
        /// </summary>
        /// <param name="cfg">The EF db record.</param>
        public AgentDelaysDTO(C_GameDelays cfg)
        {
            this.EnableLogoutDelay = cfg.EnableLogoutDelay;
            this.LogoutDelay = cfg.LogoutDelay;
            this.LogoutDelayMsg = cfg.LogoutDelayMsg;

            this.EnableStallDelay = cfg.EnableStallDelay;
            this.StallDelay = cfg.StallDelay;
            this.StallDelayMsg = cfg.StallDelayMsg;
        }


        
        /// <summary>
        /// Requred for proper JSON deserialization.
        /// </summary>
        public AgentDelaysDTO()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Saves settings into database.
        /// Does <b>not</b> flush. Flush must be done from AgentConfigEntity.
        /// </summary>
        /// <param name="context">The database context.</param>
        /// <param name="serverId">The referenced server id.</param>
        public void Save(DbContext context, int serverId)
        {
            var entities = context as XServiceDBEntities;
            var agentRec = entities.C_GameDelays.First(agent => agent.ID == serverId);

            agentRec.EnableLogoutDelay = this.EnableLogoutDelay;
            agentRec.LogoutDelay = this.LogoutDelay;
            agentRec.LogoutDelayMsg = this.LogoutDelayMsg;

            agentRec.EnableStallDelay = this.EnableStallDelay;
            agentRec.StallDelay = this.StallDelay;
            agentRec.StallDelayMsg = this.StallDelayMsg;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
