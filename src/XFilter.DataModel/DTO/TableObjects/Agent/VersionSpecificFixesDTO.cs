﻿using Newtonsoft.Json;
using System.Data.Entity;
using System.Linq;
using XFilter.DataModel.Models;

namespace XFilter.DataModel.DTO.TableObjects
{
    /// <summary>
    /// Version-specific fixes - for example, csro-r item mall fix.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class VersionSpecificFixesDTO : IDataTransferObject
    {
        //--------------------------------------------------------------------------

        #region Public properties
        
        /// <summary>
        /// Defines if CSRO item mall fix is enabled.
        /// </summary>
        [JsonProperty(PropertyName = "Enable or disable CSRO Item Mall fix")]
        public bool? CSRO_ItemMallFix
        { get; set; }

        /// <summary>
        /// Defines if CSRO item mall silk display fix is enabled.
        /// </summary>
        [JsonProperty(PropertyName = "Enable or disable CSRO silk display fix")]
        public bool? CSRO_SilkDisplayFix
        { get; set; }

        /// <summary>
        /// Defines if CSRO item mall token fix is enabled.
        /// </summary>
        [JsonProperty(PropertyName = "Enable or disable CSRO web mall token fix")]
        public bool? CSRO_WebMallTokenFix
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// VersionSpecificFixesDTO constructor (from EF db record).
        /// </summary>
        /// <param name="record">The EF db record.</param>
        public VersionSpecificFixesDTO(C_VersionSpecificFixes record)
        {
            this.CSRO_ItemMallFix = record.CSRO_MallFix;
            this.CSRO_SilkDisplayFix = record.CSRO_SilkDisplayFix;
            this.CSRO_WebMallTokenFix = record.CSRO_WebMallTokenFix;
        }

        /// <summary>
        /// Requred for proper JSON deserialization.
        /// </summary>
        public VersionSpecificFixesDTO()
        {

        }


        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Saves settings into database.
        /// Does <b>not</b> flush. Flush must be done from AgentConfigEntity.
        /// </summary>
        /// <param name="context">The database context.</param>
        /// <param name="serverId">The referenced server id.</param>
        public void Save(DbContext context, int serverId)
        {
            var entities = context as XServiceDBEntities;
            var cfg = entities.C_VersionSpecificFixes.First(item => item.RefServerID == serverId);

            cfg.CSRO_MallFix = this.CSRO_ItemMallFix;
            cfg.CSRO_SilkDisplayFix = this.CSRO_SilkDisplayFix;
            cfg.CSRO_WebMallTokenFix = this.CSRO_WebMallTokenFix;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
