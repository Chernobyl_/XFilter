﻿using System;
using System.Threading.Tasks;

namespace XFilter.Shared.Interfaces
{
    public delegate void DataReceivedEventHandler(byte[] buffer, int offset, int count);

    /// <summary>
    /// Network connection interface.
    /// </summary>
    public interface INetworkConnection
    {
        /// <summary>
        /// Gets the value indicating if the connection is established.
        /// </summary>
        bool IsConnected { get; }

        /// <summary>
        /// Fired once some data is received.
        /// </summary>
        event DataReceivedEventHandler DataReceived;

        /// <summary>
        /// Fired once the socket got disconnected.
        /// </summary>
        event EventHandler Disconnected;

        /// <summary>
        /// Closes the connection.
        /// </summary>
        void Disconnect();

        /// <summary>
        /// Starts receiving data from the socket.
        /// </summary>
        /// <returns></returns>
        Task StartReceivingData();

        /// <summary>
        /// Starts sending data to the socket.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="offset">The start offset.</param>
        /// <param name="count">The count of bytes.</param>
        /// <returns></returns>
        Task StartSendingData(byte[] data, int offset, int count);
    }
}
