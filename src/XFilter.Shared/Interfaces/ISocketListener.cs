﻿using System.Net.Sockets;

namespace XFilter.Shared.Interfaces
{
    /// <summary>
    /// Socket accepted event handler.
    /// </summary>
    /// <param name="client"></param>
    public delegate void SocketAcceptedEventHandler(Socket client);

    /// <summary>
    /// Socket listener interface.
    /// </summary>
    public interface ISocketListener
    {
        /// <summary>
        /// Gets the value indicating if the socket listener is running.
        /// </summary>
        bool Running { get; }

        /// <summary>
        /// Gets or sets the value indicating if the new clients will be accepted / session started, or dropped instantly.
        /// </summary>
        bool CreateNewSessions { get; set; }

        /// <summary>
        /// Fired once a client socket is accepted.
        /// </summary>
        event SocketAcceptedEventHandler SocketAccepted;

        /// <summary>
        /// Starts the listener and  accepting connections.
        /// </summary>
        /// <returns></returns>
        bool Start();

        /// <summary>
        /// Stops the listener.
        /// </summary>
        void Stop();

        /// <summary>
        /// Blocks given IPv4 address from connecting (before session creation).
        /// </summary>
        /// <param name="addr">The client IPv4 address string.</param>
        void BlockIpAddress(string addr);

        /// <summary>
        /// Unblocks given IPv4 address from connecting (before session creation).
        /// </summary>
        /// <param name="addr">The client IPv4 address string.</param>
        void UnblockIpAddress(string addr);
    }
}
