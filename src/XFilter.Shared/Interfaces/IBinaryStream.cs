﻿using System.Collections.Generic;

namespace XFilter.Shared.Interfaces
{
    /// <summary>
    /// The binary stream mode. Stream cannot be both writable and readable at same time.
    /// </summary>
    public enum BinaryStreamMode
    {
        Read,
        Write
    }

    /// <summary>
    /// Interface for binary stream.
    /// </summary>
    public interface IBinaryStream
    {
        /// <summary>
        /// Gets the binary stream mode.
        /// </summary>
        BinaryStreamMode Mode { get; }

        /// <summary>
        /// Gets the stream length. Don't mess this with capacity.
        /// </summary>
        int Length { get; }

        /// <summary>
        /// Gets the current stream offset.
        /// </summary>
        int Offset { get; }

        /// <summary>
        /// Gets count of remaining bytes to read, only appliable to read mode.
        /// </summary>
        int RemainRead { get; }

        /// <summary>
        /// Reads a value of given type from the stream.
        /// </summary>
        /// <typeparam name="T">The value type.</typeparam>
        /// <returns></returns>
        T Read<T>();

        /// <summary>
        /// Reads a collection of values of given type from the stream.
        /// </summary>
        /// <typeparam name="T">The type of values.</typeparam>
        /// <param name="count">The count of items to read.</param>
        /// <returns></returns>
        ICollection<T> Read<T>(int count);


        /// <summary>
        /// Writes value(s) of given type to the stream.
        /// </summary>
        /// <typeparam name="T">The value type.</typeparam>
        /// <param name="items">The values.</param>
        void Write<T>(params T[] items);

        /// <summary>
        /// Writes array to the stream (with start offset & count).
        /// </summary>
        /// <typeparam name="T">The value type.</typeparam>
        /// <param name="items">The array.</param>
        /// <param name="offset">The starting offset.</param>
        /// <param name="count">The count of items to take starting with offset.</param>
        void WriteArray<T>(T[] items, int offset, int count);
    }
}
