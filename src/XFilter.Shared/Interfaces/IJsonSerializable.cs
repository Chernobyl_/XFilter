﻿namespace XFilter.Shared.Interfaces
{
    /// <summary>
    /// Used to ensure if type is json serializable/deserializable.
    /// </summary>
    public interface IJsonSerializable
    {
        /// <summary>
        /// Creates default configuration.
        /// </summary>
        bool GenerateDefaults();

        /// <summary>
        /// Dump the object as string representation.
        /// </summary>
        /// <returns>The string.</returns>
        string Dump();
    }
}
