﻿namespace XFilter.Shared.Interfaces
{
    /// <summary>
    /// Every network service must implement this.
    /// </summary>
    public interface INetworkService
    {
        bool Initialize();
    }
}
