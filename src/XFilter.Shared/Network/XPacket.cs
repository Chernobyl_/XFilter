﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace XFilter.Shared.Network
{


    /// <summary>
    /// Network packet class
    /// </summary>
    public sealed class XPacket
    {
        //--------------------------------------------------------------------------

        #region Private properties & constants

        private MemoryStream _stream;
        private BinaryReader _reader;
        private BinaryWriter _writer;

        //true = packet is in read only mode
        private bool _locked;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public XPacket(ushort opcode)
        {
            _stream = new MemoryStream();
            _reader = new BinaryReader(_stream);
            _writer = new BinaryWriter(_stream);


            this.Opcode = opcode;
            this.Encrypted = false;
            this.Massive = false;
        }

        public XPacket(ushort opcode, bool encrypted)
        {
            _stream = new MemoryStream();
            _reader = new BinaryReader(_stream);
            _writer = new BinaryWriter(_stream);

            this.Opcode = opcode;
            this.Encrypted = encrypted;
            this.Massive = false;
        }

        public XPacket(ushort opcode, bool encrypted, bool massive)
        {
            if (encrypted && massive)
                throw new ArgumentException("Packet cannot be both encrypted and massive");

            _stream = new MemoryStream();
            _reader = new BinaryReader(_stream);
            _writer = new BinaryWriter(_stream);

            this.Opcode = opcode;
            this.Encrypted = encrypted;
            this.Massive = massive;
        }

        public XPacket(ushort opcode, bool encrypted, bool massive, byte[] bytes, int offset, int length)
        {
            if (encrypted && massive)
                throw new ArgumentException("Packet cannot be both encrypted and massive");

            _stream = new MemoryStream();
            _reader = new BinaryReader(_stream);
            _writer = new BinaryWriter(_stream);

            _writer.Write(bytes, offset, length);

            this.Opcode = opcode;
            this.Encrypted = encrypted;
            this.Massive = massive;
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public ushort Opcode
        { get; private set; }

        public bool Encrypted
        { get; private set; }

        public bool Massive
        { get; private set; }

        public bool HasDataToRead => _stream.Position < _stream.Length;

        public long Length => _stream.Length;

        #endregion

        //--------------------------------------------------------------------------

        #region Logic - reading

        public T ReadValue<T>() => ReadValue<T>(out bool tmp);

        public T ReadValue<T>(out bool success)
        {
            if (!_locked)
                throw new InvalidOperationException("Packet is in write mode");

            T result = default(T);
            Type tType = typeof(T);
            success = false;

            try
            {
                if (tType == typeof(bool))
                    result = (T)Convert.ChangeType(_reader.ReadBoolean(), tType);

                if (tType == typeof(short))
                    result = (T)Convert.ChangeType(_reader.ReadInt16(), tType);

                if (tType == typeof(ushort))
                    result = (T)Convert.ChangeType(_reader.ReadUInt16(), tType);

                if (tType == typeof(int))
                    result = (T)Convert.ChangeType(_reader.ReadInt32(), tType);

                if (tType == typeof(uint))
                    result = (T)Convert.ChangeType(_reader.ReadUInt32(), tType);

                if (tType == typeof(long))
                    result = (T)Convert.ChangeType(_reader.ReadInt64(), tType);

                if (tType == typeof(ulong))
                    result = (T)Convert.ChangeType(_reader.ReadUInt64(), tType);

                if (tType == typeof(sbyte))
                    result = (T)Convert.ChangeType(_reader.ReadSByte(), tType);

                if (tType == typeof(byte))
                    result = (T)Convert.ChangeType(_reader.ReadByte(), tType);

                if (tType == typeof(float))
                    result = (T)Convert.ChangeType(_reader.ReadSingle(), tType);
                if (tType == typeof(string))
                {
                    ushort length = _reader.ReadUInt16();
                    byte[] strBytes = _reader.ReadBytes(length);
                    result = (T)Convert.ChangeType(
                        Encoding.ASCII.GetString(strBytes, 0, length),
                        tType);
                }

                success = true;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }

            return result;
        }

        public ushort ReadUShort()
        {
            if (!_locked)
            {
                throw new InvalidOperationException("Cannot Read from an unlocked Packet.");
            }
            return _reader.ReadUInt16();
        }

        public string ReadString(ushort length, int codepage = 1251)
        {
            if (!_locked)
                throw new InvalidOperationException("Cannot Read from an unlocked Packet.");

            byte[] bytes = _reader.ReadBytes(length);
            return Encoding.GetEncoding(codepage).GetString(bytes);
        }

        public uint ReadUInt()
        {
            if (!_locked)
                throw new InvalidOperationException("Cannot Read from an unlocked Packet.");

            return _reader.ReadUInt32();
        }

        public List<T> ReadList<T>(int count)
        {
            if (!_locked)
                throw new InvalidOperationException("Packet is in write mode");

            List<T> result = new List<T>(count);
            for (int i = 0; i < count; i++)
                result.Add(ReadValue<T>());
            return result;
        }

        public byte[] ReadByteArray(int count)
        {
            if (!_locked)
                throw new InvalidOperationException("Packet is in write mode");

            return _reader.ReadBytes(count);
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic - writing

        public void WriteValue<T>(dynamic value)
        {
            if (_locked)
                throw new InvalidOperationException("Packet is in read mode");

            //zomg t3h hax
            value = (T)value;

            //Using this "unobvious" way because of serializer putting 1 byte
            //string length... we dont need that !
            if (value is string)
            {
                string str = value as string;
                byte[] bytes = ASCIIEncoding.ASCII.GetBytes(str);

                _writer.Write((ushort)bytes.Length);
                _writer.Write((byte[])bytes);
                return;
            }

            _writer.Write(value);
        }

        public void WriteList<T>(params dynamic[] values)
        {
            if (_locked)
                throw new InvalidOperationException("Packet is in read mode");

            for (int i = 0; i < values.Length; i++)
                WriteValue<T>(values[i]);
        }

        public void WriteByteArray(byte[] data)
        {
            if (_locked)
                throw new InvalidOperationException("Packet is in read mode");

            _writer.Write(data);
        }

        public void WriteByteArray(byte[] data, int offset, int count)
        {
            if (_locked)
                throw new InvalidOperationException("Packet is in read mode");

            _writer.Write(data, offset, count);
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic - other

        public void Lock()
        {
            _reader.BaseStream.Seek(0, SeekOrigin.Begin);
            _locked = true;
        }

        public byte[] GetDataBytes()
        {
            if (!_locked)
                Lock();

            long origPos = _reader.BaseStream.Position;
            _reader.BaseStream.Seek(0, SeekOrigin.Begin);

            byte[] bytes = _reader.ReadBytes((int)_stream.Length);

            //Seek back where we were
            _reader.BaseStream.Seek(origPos, SeekOrigin.Begin);

            return bytes;

        }


        public byte[] GetBytes() => GetDataBytes();
        public int RemainRead => (int)(_stream.Length - _reader.BaseStream.Position);
        public void SeekToBeginning() => _stream.Seek(0, SeekOrigin.Begin);


        #endregion

        //--------------------------------------------------------------------------
    }
}