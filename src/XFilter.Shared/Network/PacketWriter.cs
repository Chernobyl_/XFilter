﻿namespace XFilter.Shared.Network
{
    using System.IO;

    public sealed class PacketWriter : BinaryWriter
    {
        private MemoryStream _stream;

        public PacketWriter()
        {
            _stream = new MemoryStream();
            this.OutStream = _stream;
        }

        public byte[] GetBytes()
        {
            return _stream.ToArray();
        }
    }
}
