﻿using NLog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;
using XFilter.Shared.Extensions;
using XFilter.Shared.Interfaces;

namespace XFilter.Shared.Network
{
    /// <summary>
    /// TCP socket listener class.
    /// </summary>
    public class SocketListener : ISocketListener
    {
        #region SocketListener private & static

        private TcpListener _listener;
        private readonly ConcurrentDictionary<string, DateTime> _lastConnectionTimes;
        private readonly ICollection<string> _blockedIpAddrs;
        private readonly IPEndPoint _localEndPoint;
        private readonly int _reconnectionCooldown;
        private readonly bool _enableReconnectionCooldown;
        private readonly object _blockedIpAddrsLock = new object();
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Constructors

        /// <summary>
        /// 
        /// </summary>
        /// <param name="endPoint">The local endpoint for listener.</param>
        /// <param name="reconnectionCooldown">Per-ip reconnection cooldown (milliseconds).</param>
        /// <param name="enableReconnectionCooldown"></param>
        public SocketListener(IPEndPoint endPoint, int reconnectionCooldown, bool enableReconnectionCooldown)
        {
            _localEndPoint = endPoint;
            _lastConnectionTimes = new ConcurrentDictionary<string, DateTime>();
            _blockedIpAddrs = new List<string>();
            _reconnectionCooldown = reconnectionCooldown;
            _enableReconnectionCooldown = enableReconnectionCooldown;
        }

        private SocketListener()
        {

        }

        #endregion

        #region SocketListener logic

        /// <summary>
        /// Checks if its possible to bind & start listening on given local IP endpoint.
        /// </summary>
        /// <param name="ep">The local endpoint.</param>
        /// <returns></returns>
        private bool CanStartListener(IPEndPoint ep)
        {
            bool result;
            var ipGlobalProps = IPGlobalProperties.GetIPGlobalProperties();

            //Could use linq Any here, but Count is more expressive.
            result = ipGlobalProps.GetActiveTcpListeners().Count(
                listener => listener.Address == ep.Address && listener.Port == ep.Port) == 0;

            return result;
        }

        /// <summary>
        /// Starts accepting new socket connection.
        /// </summary>
        private void StartAcceptingNextSocket()
        {
            if (!this.Running)
                return;

            try
            {
                _listener.BeginAcceptSocket(SocketAcceptedCallback, null);
            }
            catch (ObjectDisposedException) { return; }
            catch (Exception ex)
            {
                Logger.Error(ex, "BeginAcceptSocket failed.");
                StartAcceptingNextSocket();
            }
        }

        /// <summary>
        /// Checks if the given IPv4 address is blocked.
        /// </summary>
        /// <param name="address">The client IPv4 address (xxx.xxx.xxx.xxx).</param>
        /// <returns></returns>
        private bool IsClientAddressBlocked(string address)
        {
            bool blocked = false;
            try
            {
                if (Monitor.TryEnter(_blockedIpAddrsLock))
                {
                    if (_blockedIpAddrs.Contains(address))
                    {
                        blocked = true;
                        Monitor.Exit(_blockedIpAddrsLock);
                    }
                }
            }
            catch { }

            return blocked;
        }

        /// <summary>
        /// Checks if the client is on connection cooldown (reconnecting too fast).
        /// </summary>
        /// <param name="address">The client IPv4 address (xxx.xxx.xxx.xxx).</param>
        /// <returns></returns>
        private bool IsClientOnConnectionCooldown(string address)
        {
            if (!_enableReconnectionCooldown)
                return false;

            if (_lastConnectionTimes.ContainsKey(address))
            {
                TimeSpan elapsedSinceLastConn = (DateTime.Now - _lastConnectionTimes[address]);
                if (elapsedSinceLastConn.TotalMilliseconds > _reconnectionCooldown)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Fired once there is a client socket to accept.
        /// </summary>
        /// <param name="iar">The async result.</param>
        private void SocketAcceptedCallback(IAsyncResult iar)
        {
            if (!this.Running)
                return;

            Socket client = null;

            try
            {
                client = _listener.EndAcceptSocket(iar);
            }
            catch (ObjectDisposedException) { return; }
            catch (Exception ex)
            {
                Logger.Error(ex, "EndAcceptSocket failed.");
            }

            if (!this.CreateNewSessions)
            {
                client.CloseNoThrow();
                StartAcceptingNextSocket();
                return;
            }

            string clientAddr = client.GetAddressString();

            //Check if we didnt fail getting the socket IPv4 address string.
            if (clientAddr == null)
            {
                StartAcceptingNextSocket();
                return;
            }

            //First time this IP address connects. Store connection time.
            if (!_lastConnectionTimes.ContainsKey(clientAddr))
                _lastConnectionTimes.TryAdd(clientAddr, DateTime.Now);

            //Check if the client ip address is blocked.
            if (IsClientAddressBlocked(clientAddr))
            {
                client.CloseNoThrow();
                StartAcceptingNextSocket();
                return;
            }

            //Check if client is connecting too often.
            if (IsClientOnConnectionCooldown(clientAddr))
            {
                client.CloseNoThrow();
                Logger.Warn($"The client is on connection cooldown state. IP: {clientAddr}.");
                StartAcceptingNextSocket();
                return;
            }

            //The server is accepting new clients, client ip is not blocked and no cooldown is in progress.
            StartAcceptingNextSocket();
            this.SocketAccepted?.Invoke(client);
        }

        #endregion

        #region ISocketListener

        /// <summary>
        /// Gets the value indicating if the socket listener is running.
        /// </summary>
        public bool Running { get; private set; }

        /// <summary>
        /// Gets or sets the value indicating if the new clients will be accepted / session started, or dropped instantly.
        /// </summary>
        public bool CreateNewSessions { get; set; }

        /// <summary>
        /// Fired once a client socket is accepted.
        /// </summary>
        public event SocketAcceptedEventHandler SocketAccepted = (Socket client) => { };

        /// <summary>
        /// Starts the listener and  accepting connections.
        /// </summary>
        /// <returns></returns>
        public bool Start()
        {
            if (this.Running)
                throw new InvalidOperationException("Trying to start listener which is already running.");

            if (!CanStartListener(_localEndPoint))
                return false;

            _listener = new TcpListener(_localEndPoint);
            _listener.Start();

            //Must be set before calling StartAcceptingNextSocket.
            this.Running = true;
            StartAcceptingNextSocket();

            return true;
        }

        /// <summary>
        /// Stops the listener.
        /// </summary>
        public void Stop()
        {
            if (!this.Running)
                throw new InvalidOperationException("Trying to stop listener which is not running.");

            _listener.Stop();
            _lastConnectionTimes.Clear();

            this.CreateNewSessions = false;
            this.Running = false;
        }


        /// <summary>
        /// Blocks given IPv4 address from connecting (before session creation).
        /// </summary>
        /// <param name="addr">The client IPv4 address string.</param>
        public void BlockIpAddress(string addr)
        {
            if (Monitor.TryEnter(_blockedIpAddrsLock))
            {
                _blockedIpAddrs.Add(addr);
                Monitor.Exit(_blockedIpAddrsLock);
            }
        }

        /// <summary>
        /// Unblocks given IPv4 address from connecting (before session creation).
        /// </summary>
        /// <param name="addr">The client IPv4 address string.</param>
        public void UnblockIpAddress(string addr)
        {
            if (Monitor.TryEnter(_blockedIpAddrsLock))
            {
                _blockedIpAddrs.Remove(addr);
                Monitor.Exit(_blockedIpAddrsLock);
            }
        }

        #endregion
    }
}
