﻿using System;
using System.Linq;
using System.Text;
using XFilter.Shared.Collections;
using XFilter.Shared.Interfaces;

namespace XFilter.Shared.Network
{
    /// <summary>
    /// Packet stream class. Currently only supports SSA.
    /// For protocol details see
    /// https://github.com/DummkopfOfHachtenduden/SilkroadDoc/wiki/Silkroad-Security
    /// </summary>
    public class PacketStream : BinaryStream, IPacketStream
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of <see cref="PacketStream"/> for writing.
        /// </summary>
        /// <param name="opcode">The opcode.</param>
        public PacketStream(ushort opcode) :
            base()
        {
            this.Opcode = opcode;
            this.IsEncrypted = false;
            this.IsMassive = false;
        }

        /// <summary>
        /// Initializes a new instance of <see cref="PacketStream"/> for writing.
        /// </summary>
        /// <param name="opcode">The opcode.</param>
        /// <param name="isEncrypted">The encryption flag.</param>
        public PacketStream(ushort opcode, bool isEncrypted) :
            base()
        {
            this.Opcode = opcode;
            this.IsEncrypted = isEncrypted;
            this.IsMassive = false;
        }

        /// <summary>
        /// Initializes a new instance of <see cref="PacketStream"/> for writing.
        /// Packet can be only normal (no enc/not massive) or one of those two.
        /// </summary>
        /// <param name="opcode">The opcode.</param>
        /// <param name="isEncrypted">The encryption flag.</param>
        /// <param name="isMassive">The massive flag.</param>
        public PacketStream(ushort opcode, bool isEncrypted, bool isMassive) :
            base()
        {
            if (isEncrypted && isMassive)
                throw new ArgumentException("Packet cannot be both encrypted and massive.");

            this.Opcode = opcode;
            this.IsEncrypted = isEncrypted;
            this.IsMassive = isMassive;
        }

        /// <summary>
        /// Initializes a new instance of <see cref="PacketStream"/> for reading.
        /// </summary>
        /// <param name="opcode">The opcode.</param>
        /// <param name="isEncrypted">The encryption flag.</param>
        /// <param name="isMassive">The massive flag.</param>
        /// <param name="buffer">The data buffer.</param>
        /// <param name="offset">The data buffer start offset.</param>
        /// <param name="count">The count of bytes.</param>
        public PacketStream(ushort opcode, bool isEncrypted, bool isMassive, byte[] buffer, int offset, int count) :
            base(buffer, offset, count)
        {
            if (isEncrypted && isMassive)
                throw new ArgumentException("Packet cannot be both encrypted and massive.");

            this.Opcode = opcode;
            this.IsEncrypted = isEncrypted;
            this.IsMassive = IsMassive;
        }


        /// <summary>
        /// Prevents creation of <see cref="PacketStream"/> instance without supplying arguments.
        /// </summary>
        private PacketStream()
        {

        }

        #endregion

        #region IPacketStream

        /// <summary>
        /// Gets the packet opcode.
        /// </summary>
        public ushort Opcode { get; private set; }

        /// <summary>
        /// Gets the value indicating if the packet is encrypted.
        /// </summary>
        public bool IsEncrypted { get; private set; }

        /// <summary>
        /// Gets the value indicating if the packet is massive.
        /// </summary>
        public bool IsMassive { get; private set; }

        /// <summary>
        /// Reads a ASCII string with given codepage from the stream.
        /// </summary>
        /// <param name="codepage">The codepage.</param>
        /// <returns>The string.</returns>
        public string ReadStringASCII(int codepage = 1251)
        {
            var binaryStream = (this as IBinaryStream);
            if (binaryStream.Mode != BinaryStreamMode.Read)
                throw new InvalidOperationException("The underlying binary stream is not in read mode.");

            UInt16 length = binaryStream.Read<UInt16>();
            if (length > binaryStream.RemainRead)
                throw new InvalidOperationException("Cannot read string longer than RemainRead.");

            var encoding = ASCIIEncoding.GetEncoding(codepage);
            return encoding.GetString(binaryStream.Read<byte>(length).ToArray());
        }

        /// <summary>
        /// Writes a ASCII string with given codepage to the stream.
        /// </summary>
        /// <param name="str">The string to write.</param>
        /// <param name="codepage">The string codepage.</param>
        public void WriteStringASCII(string str, int codepage = 1251)
        {
            if (str.Length > UInt16.MaxValue)
                throw new ArgumentException("String too long.");

            var binaryStream = (this as IBinaryStream);
            if (binaryStream.Mode != BinaryStreamMode.Write)
                throw new InvalidOperationException("The underlying binary stream is not in write mode.");

            UInt16 length = (UInt16)str.Length;
            var encoding = ASCIIEncoding.GetEncoding(codepage);
            binaryStream.Write<byte>(encoding.GetBytes(str));
        }

        /// <summary>
        /// Reads a unicode string from the stream.
        /// </summary> 
        /// <returns>The string.</returns>
        public string ReadStringUnicode()
        {
            var binaryStream = (this as IBinaryStream);

            if (binaryStream.Mode != BinaryStreamMode.Read)
                throw new InvalidOperationException("The underlying binary stream is not in read mode.");

            UInt16 length = binaryStream.Read<UInt16>();
            if ((length * 2) > binaryStream.RemainRead)
                throw new InvalidOperationException("Cannot read string longer than RemainRead.");

            return UnicodeEncoding.Unicode.GetString(binaryStream.Read<byte>(length * 2).ToArray());
        }

        /// <summary>
        /// Writes a unicode string to the stream.
        /// </summary>
        /// <param name="str">The string.</param>
        public void WriteStringUnicode(string str)
        {
            if (str.Length > Int16.MaxValue)
                throw new ArgumentException("String too long.");

            var binaryStream = (this as IBinaryStream);
            if (binaryStream.Mode != BinaryStreamMode.Write)
                throw new InvalidOperationException("The underlying binary stream is not in write mode.");

            UInt16 length = (UInt16)str.Length;
            binaryStream.Write<byte>(UnicodeEncoding.Unicode.GetBytes(str));
        }

        #endregion
    }
}
