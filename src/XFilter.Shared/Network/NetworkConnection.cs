﻿using System;
using System.Buffers;
using System.Net.Sockets;
using System.Threading.Tasks;
using XFilter.Shared.Extensions;
using XFilter.Shared.Interfaces;

namespace XFilter.Shared.Network
{
    /// <summary>
    /// Network connection interface.
    /// </summary>
    public class NetworkConnection : INetworkConnection
    {
        #region Private properties & statics

        private Socket _socket;
        private byte[] _recvBuffer;

        #endregion

        #region Constructors

        public NetworkConnection(Socket sock)
        {
            _socket = sock;
            _recvBuffer = ArrayPool<byte>.Shared.Rent(4096);
            this.IsConnected = true;
        }

        #endregion

        #region INetworkConnection

        /// <summary>
        /// Gets the value indicating if the connection is established.
        /// </summary>
        public bool IsConnected { get; private set; }

        /// <summary>
        /// Fired once some data is received.
        /// </summary>
        public event DataReceivedEventHandler DataReceived = (byte[] buffer, int offset, int count) => { };

        /// <summary>
        /// Fired once the socket got disconnected.
        /// </summary>
        public event EventHandler Disconnected = (s, e) => { };

        /// <summary>
        /// Closes the connection.
        /// </summary>
        public void Disconnect()
        {
            //Do not fire the disconnected event handler if the socket is already disconnected. 
            //That could cause a deadlock because of recursion, be careful.
            if (!this.IsConnected)
                return;

            this.IsConnected = false;

            _socket.CloseNoThrow();
            ArrayPool<byte>.Shared.Return(_recvBuffer, false);
            this?.Disconnected(this, null);
        }

        /// <summary>
        /// Starts receiving data from the socket.
        /// </summary>
        /// <returns></returns>
        public Task StartReceivingData()
        {
            Task recvTask = new Task(() =>
            {
                try
                {
                    _socket.BeginReceive(_recvBuffer, 0, _recvBuffer.Length, SocketFlags.None,
                        (IAsyncResult iar) =>
                        {
                            try
                            {
                                int recvCount = _socket.EndReceive(iar);
                                if (recvCount == 0)
                                {
                                    this.Disconnect();
                                    return;
                                }

                                this?.DataReceived(_recvBuffer, 0, recvCount);
                            }
                            catch { this.Disconnect(); }
                        }, null);
                }
                catch { this.Disconnect(); }
            });
            return recvTask;
        }

        /// <summary>
        /// Starts sending data to the socket.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="offset">The start offset.</param>
        /// <param name="count">The count of bytes.</param>
        /// <returns></returns>
        public Task StartSendingData(byte[] data, int offset, int count)
        {
            Task sendTask = new Task(() =>
            {
                try
                {
                    _socket.BeginSend(data, offset, count, SocketFlags.None,
                        (IAsyncResult iar) =>
                        {
                            try
                            {
                                _socket.EndSend(iar);
                            }
                            catch { this.Disconnect(); }
                        }, null);
                }
                catch { this.Disconnect(); }
            });

            return sendTask;
        }

        #endregion
    }
}
