﻿using NLog;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using XFilter.Shared.Collections;
using XFilter.Shared.Helpers;

namespace XFilter.Shared.Network
{

    //--------------------------------------------------------------------------

    #region Enums

    /// <summary>
    /// Socket base operation result.
    /// </summary>
    public enum SocketOp
    {
        ConnectSuccess,
        StoppedByUser,
        ConnectFailedOrTimeout,
        Ping
    }

    #endregion

    //--------------------------------------------------------------------------

    /// <summary>
    /// Base class for every net client.
    /// Don't touch, magic involved in reconnection sys.
    /// </summary>
    public abstract class NetClientBase
    {
        //--------------------------------------------------------------------------

        #region Private properties & constants

        private Socket _socket;
        private readonly IPEndPoint _ep;
        private readonly int _reconnInterval;
        private readonly int _connTimeout;

        private ByteBuffer _buffer;
        private readonly ByteBufferManager _bufferManager;
        private readonly TimerItem _reconnector;
        private readonly TimerItem _pinger;

        //NOTE: We need a new SrSecurity object on each reconnection.
        private SrSecurity _security;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        
        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// The ctor.
        /// </summary>
        /// <param name="bufferManager">The buffer manager.</param>
        /// <param name="addr">The target IPv4 address (xxx.xxx.xxx.xxx) or hostname.</param>
        /// <param name="port">The por.t</param>
        /// <param name="reconnInterval">The reconnection interval.</param>
        /// <param name="connTimeout">The connection timeout.</param>
        public NetClientBase(ByteBufferManager bufferManager, string addr, int port, int reconnInterval, int connTimeout, int pingInterval)
        {
            var ip = NetHelper.ParseOrResolve(addr);
            if (ip == null)
                throw new ArgumentException("[NetClientBase] Invalid address or hostname.");

            _bufferManager = bufferManager;
            _ep = new IPEndPoint(ip, port);
            _reconnInterval = reconnInterval;
            _connTimeout = connTimeout;
            _reconnector = new TimerItem(ReconnectTickEventHandler, reconnInterval);
            _pinger = new TimerItem(PingTickEventHandler, pingInterval);

            //_buffer = _bufferManager.GetBuffer();
            //_buffer.OnReleasedByObjectPool = this.BufferReleasedFromObjectPoolEventHandler;


            _security = new SrSecurity();
            _security.GenerateSecurity(true, true, true);
            this.Connected = false;
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties and events

        /// <summary>
        /// Defines if the client is connected.
        /// </summary>
        public bool Connected { get; private set; }

        /// <summary>
        /// Net client callback handlers.
        /// </summary>
        public event Action<SocketOp> StateChanged;

        /// <summary>
        /// Gets fired once some data is received from server.
        /// </summary>
        public event Action<List<XPacket>> DataReceived;

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Initliazes client connection.
        /// </summary>
        public void Start()
        {
            _buffer = _bufferManager.GetBuffer();
            _buffer.OnReleasedByObjectPool = this.BufferReleasedFromObjectPoolEventHandler;


            //reconnector timer gets started from there
            ReconnectTickEventHandler();
        }

        /// <summary>
        /// Stops the net client.
        /// </summary>
        /// <param name="stopReconnecting">Pass true if you REALLY want client to stop.</param>
        private void Stop(bool reconnect = true)
        {
            this.Connected = false;

            NetHelper.CloseSocket(_socket);
            _pinger.Stop();

            if (_buffer != null)
            {
                //NOTE: Maybe it was released from ObjectPool.
                if (_buffer.IsUsed)
                    _bufferManager.ReleaseBuffer(_buffer);
            }

            if (!reconnect)
            {
                _reconnector.Stop();

                StateChanged(SocketOp.StoppedByUser);
            }
        }

        public void BufferReleasedFromObjectPoolEventHandler()
        {
            Logger.Warn("Buffer was released by object pool (timeout). Restarting context...");
            this.Stop(true);
        }

        /// <summary>
        /// Completely terminates the client (it wont reconnect)
        /// </summary>
        public void Terminate() => Stop(false);

        private void PingTickEventHandler() =>  StateChanged(SocketOp.Ping);

        /// <summary>
        /// Fired on _reconnector timer tick.
        /// </summary>
        private async void ReconnectTickEventHandler()
        {
            if (this.Connected)
                return;

   
            //We pause futher ticks of reconnector until work completed
            _reconnector.Stop();
            await Task.Factory.StartNew(() =>
            {
                _socket = NetHelper.ConnectWithTimeout(_ep, _connTimeout);

                if (_socket == null)
                {
                    this.Connected = false;
                    this.StateChanged?.Invoke(SocketOp.ConnectFailedOrTimeout);
                }
                else
                {
                    //_buffer = _bufferManager.GetBuffer();
                   // _buffer.OnReleasedByObjectPool = this.BufferReleasedFromObjectPoolEventHandler;


                    this.Connected = true;
                    this.StateChanged?.Invoke(SocketOp.ConnectSuccess);

                    //We need a new security object...
                    _security = new SrSecurity();
                    _security.GenerateSecurity(true, true, true);

                    _pinger.Start();

                    //Initiate the handshake sequence.
                    FlushOutgoing();
                    
                    ReceiveData();

                }
            });

            _reconnector.Start();
            
        }

        /// <summary>
        /// Initializes data reception from server.
        /// </summary>
        private void ReceiveData()
        {
            try
            {
                _socket.BeginReceive(_buffer.Buffer, 0, _buffer.Buffer.Length, SocketFlags.None,
                    ReceiveDataCallback, null);

                _buffer.Pulse();
            }
            catch { Stop(); }
        }

        /// <summary>
        /// Fired once there is some data to process or if 0
        /// </summary>
        /// <param name="iar">IAsyncResult</param>
        private void ReceiveDataCallback(IAsyncResult iar)
        {
            try
            {
                int recvCount = _socket.EndReceive(iar);

                if(recvCount == 0)
                {
                    Stop();
                    return;
                }

                _buffer.Pulse();

                _security.Recv(_buffer.Buffer, 0, recvCount);
                var packets = _security.TransferIncoming();
                if (packets != null)
                {
                    this.DataReceived(packets);
                }

                FlushOutgoing();

                ReceiveData();
            }
            catch { Stop(); }
        }

        /// <summary>
        /// Flushes all outgoing buffers to network.
        /// </summary>
        public void FlushOutgoing()
        {
            try
            {
                var data = _security.TransferOutgoing();

                //Nothing to flush.
                if (data == null)
                    return;

                //TODO: Statistics measurement.
                foreach(var kvp in data)
                { 
                    SendData(kvp.Key.Buffer, kvp.Key.Buffer.Length);
                }
            }
            catch { Stop(); }
        }


        /// <summary>
        /// Enqueue packet for sending.
        /// </summary>
        /// <param name="packet">The packet.</param>
        public void Send(XPacket packet, bool flush = true)
        {
            _security.Send(packet);
            if (flush)
                FlushOutgoing();
        }

        /// <summary>
        /// Starts sending data to the server.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        /// <param name="count">The count of bytes to send.</param>
        public void SendData(byte[] buffer, int count)
        {
            try
            {
                _socket.BeginSend(buffer, 0, count, SocketFlags.None,
                    SendDataCallback, null);

                _buffer.Pulse();
            }
            catch { Stop(); }
        }

        /// <summary>
        /// Data sending callback.
        /// </summary>
        /// <param name="iar">IAsyncResult</param>
        private void SendDataCallback(IAsyncResult iar)
        {
            try
            {
                _socket.EndSend(iar);

                _buffer.Pulse();
            }
            catch { Stop(); }
        }
        #endregion

        //--------------------------------------------------------------------------
    }
}
