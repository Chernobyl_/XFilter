﻿namespace XFilter.Shared.Helpers
{
    using System;
    using System.Timers;


    /// <summary>
    /// Timer wrapper class.
    /// </summary>
    public sealed class TimerItem
    {
        //--------------------------------------------------------------------------

        #region Private properties & constants

        private Timer _timer;

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors
        /// <summary>
        /// The ctor.
        /// </summary>
        /// <param name="onTick">Event fired on tick.</param>
        /// <param name="interval">Milliseconds.</param>
        public TimerItem(Action<object> onTick, int interval)
        {
            _timer = new Timer()
            {
                AutoReset = true,
                Interval = interval
            };
            _timer.Elapsed += (s, e) => onTick(onTick);
        }

        public TimerItem(Action onTick, int interval)
        {
            _timer = new Timer()
            {
                AutoReset = true,
                Interval = interval,
            };
            _timer.Elapsed += (s, e) => onTick();
        }

        private TimerItem()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Starts the timer.
        /// </summary>
        public void Start() => _timer.Start();

        /// <summary>
        /// Stops the timer.
        /// </summary>
        public void Stop() => _timer.Stop();

        #endregion

        //--------------------------------------------------------------------------
    }
}
