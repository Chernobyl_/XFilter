﻿namespace XFilter.Shared.Helpers
{
    using System.Text;
    using System;

    public static class StringExtensions
    {
        public static string TryFormat(string str, params object[] args)
        {
            string result = null;
            try
            {
                result = string.Format(str, args);
            }
            catch { }
            return result;
        }

        public static string DumpFormat(string str, params object[] args)
        {
            var builder = new StringBuilder();
            builder.Append($"Dumping string {str}");
            int index = 0;
            foreach(var arg in args)
            {
                var typeName = arg.GetType().FullName;
                builder.Append($"Index = {index++} Type = {typeName}, Value = {arg}{Environment.NewLine}");
            }

            return builder.ToString();
        }
    }
}
