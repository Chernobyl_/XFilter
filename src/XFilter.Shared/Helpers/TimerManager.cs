﻿namespace XFilter.Shared.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;


    /// <summary>
    /// Timer manager class.
    /// </summary>
    public sealed class TimerManager
    {
        //--------------------------------------------------------------------------

        #region Private properties & constants

        private readonly List<TimerItem> _timers;

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// The ctor.
        /// </summary>
        public TimerManager()
        {
            _timers = new List<TimerItem>();
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Creates a timer and starts it.
        /// </summary>
        /// <param name="onTick">Event fired on timer tick.</param>
        /// <param name="interval">Milliseconds.</param>
        public void Create(Action<object> onTick, int interval)
        {
            var timer = new TimerItem(onTick, interval);
            timer.Start();
            _timers.Add(timer);
        }


        public void Create(Action onTick, int interval)
        {
            var timer = new TimerItem(onTick, interval);
            timer.Start();
            _timers.Add(timer);
        }

        /// <summary>
        /// Destroys the timer.
        /// </summary>
        /// <param name="timer">The timer.</param>
        public void Destroy(TimerItem timer)
        {
            timer.Stop();
            _timers.Remove(timer);
        }

        /// <summary>
        /// Pauses the timer.
        /// </summary>
        /// <param name="timer">The timer.</param>
        public void Pause(TimerItem timer) => 
            _timers.First(item => item == timer).Stop();

        /// <summary>
        /// Resumes the timer.
        /// </summary>
        /// <param name="timer">Milliseconds.</param>
        public void Resume(TimerItem timer) => 
            _timers.First(item => item == timer).Start();

        /// <summary>
        /// Destroys all timers.
        /// </summary>
        public void DestroyAll()
        {
            _timers.ForEach(timer => timer.Stop());
            _timers.Clear();
        }

        #endregion

        //--------------------------------------------------------------------------

    }
}
