﻿
namespace XFilter.Shared.Helpers
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Net.Sockets;

    /// <summary>
    /// Class containing common network related methods.
    /// </summary>
    public static class NetHelper
    {
        //--------------------------------------------------------------------------

        #region Private properties & constants

        //NOTE: Check if we need to call GetIPGlobalProperties() every time we are willing to
        //fetch network information
        private static readonly IPGlobalProperties IPGlobalProperties = IPGlobalProperties.GetIPGlobalProperties();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Closes the given socket without throwing any exceptions.
        /// </summary>
        /// <param name="sock">The socket object.</param>
        public static void CloseSocket(Socket sock)
        {
            try
            {
                sock.Disconnect(true);
            }
            catch { }
            try
            {
                sock.Shutdown(SocketShutdown.Both);
                sock.Close();

                
            }
            catch { }
            try
            {
                sock.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            }
            catch { }

            try
            {
                sock.Dispose();
            }
            catch { }

           
        }

        /// <summary>
        /// Parses given ipv4 address or hostname into IPAddress structure.
        /// </summary>
        /// <param name="addr">IPv4 addresss (xxx.xxx.xxx.xxx) or hostname</param>
        /// <returns>IPAddress, or null on failure.</returns>
        public static IPAddress ParseOrResolve(string addr)
        {
            IPAddress ip = null;
            //DNS resolution isnt needed
            if (IPAddress.TryParse(addr, out ip))
                return ip;

            try
            {
                var hostEntry = Dns.GetHostEntry(addr);
                ip = hostEntry.AddressList.First();
            }
            catch { }
            return ip;
        }

        /// <summary>
        /// Connects to given ip endpoint with given timeout.
        /// </summary>
        /// <param name="ep">The network endpoint.</param>
        /// <param name="timeout"></param>
        /// <returns>The socket object.</returns>
        public static Socket ConnectWithTimeout(IPEndPoint ep, int timeout)
        {
            var sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                var iar = sock.BeginConnect(ep, null, null);
                if(iar.AsyncWaitHandle.WaitOne(timeout))
                {
                    sock.EndConnect(iar);
                    return sock;
                }
            }
            catch { }
            CloseSocket(sock);

            return null;
        }

        /// <summary>
        /// Connects to given address / port with given timeout.
        /// </summary>
        /// <param name="address">The IPv4 address or hostname.</param>
        /// <param name="port">The port.</param>
        /// <param name="timeout">The timeput (milliseconds).</param>
        /// <returns></returns>
        public static Socket ConnectWithTimeout(string address, int port, int timeout)
        {
            IPAddress ip = ParseOrResolve(address);
            if (ip == null)
                return null;

            return ConnectWithTimeout(new IPEndPoint(ip, port), timeout);
        }

        /// <summary>
        /// Binds socket and starts listening on given address port with specified backlog
        /// </summary>
        /// <param name="bindAddr">The address to bind port on.</param>
        /// <param name="port">The port.</param>
        /// <param name="backlog">Queue size for accepting connections.</param>
        /// <returns></returns>
        public static Socket BindAndListen(string bindAddr, int port, int backlog)
        {
            var sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            var localIp = ParseOrResolve(bindAddr);

            if (localIp == null)
                return null;

            var ep = new IPEndPoint(localIp, port);
            try
            {
                sock.Bind(ep);
                sock.Listen(backlog);
                return sock;
            }
            catch { }

            return null;
        }

        /// <summary>
        /// Checks if given TCP port is used for listening.
        /// </summary>
        /// <param name="port">The port.</param>
        /// <returns>true / false</returns>
        public static bool IsLocalPortBusy(int port) =>
            IPGlobalProperties.GetActiveTcpListeners().Count(listener => listener.Port == port) > 0;

        /// <summary>
        /// Gets total count of connections to given local port.
        /// </summary>
        /// <param name="port">The port.</param>
        /// <returns>The total count of connections to local port.</returns>
        public static int CountOfConnsToLocalPort(int port) =>
            IPGlobalProperties.GetActiveTcpConnections().Count(conn => conn.LocalEndPoint.Port == port);

        /// <summary>
        /// Gets total count of connections to given local port from given remote address (IPv4 or hostname).
        /// </summary>
        /// <param name="addr">IPv4 address (xxx.xxx.xxx.xxx) or hostname.</param>
        /// <param name="port">The port.</param>
        /// <returns>The total count of connections to given local port from given remote address (IPv4 or hostname).</returns>
        public static int CountOfConnsToLocalPortFromAddr(string addr, int port)
        {
            var ip = ParseOrResolve(addr);
            if (ip == null)
                return 0;

            return IPGlobalProperties.GetActiveTcpConnections().Count(
                conn => conn.RemoteEndPoint.Address == ip && conn.LocalEndPoint.Port == port);
        }

        /// <summary>
        /// Gets socket IPv4 address string (xxx.xxx.xxx.xxx)
        /// </summary>
        /// <param name="sock"></param>
        /// <returns></returns>
        public static string GetSockIpAddrStr(Socket sock)
        {
            var ep = (sock.LocalEndPoint ?? sock.RemoteEndPoint) as IPEndPoint;
            return ep.Address.ToString();
        }

        public static int GetSockAddrHash(Socket sock)
        {
            var ep = sock.RemoteEndPoint as IPEndPoint;
            return (int)(ep.Address.GetHashCode() + ep.Port.GetHashCode());
        }


        public static string GetMacString(byte[] bytes)
        {
            if (bytes == null)
                throw new ArgumentNullException("Mac address bytes cannot be null.");

            if (bytes.Length != 6)
                throw new ArgumentException("Mac address must contain 6 bytes.");

            return string.Format(
            "{0:02X}-{0:02X}-{0:02X}-{0:02X}-{0:02X}-{0:02X}",
            bytes[0], bytes[1],
            bytes[2], bytes[3],
            bytes[4], bytes[5]);

        }

        public static uint IpAddrStrToInt(string addr)
        {
            var ip = ParseOrResolve(addr);
            return BitConverter.ToUInt32(ip.GetAddressBytes(), 0);
        }

        public static string IpAddrStrFromInt(uint ip) =>
            new IPAddress((uint)IPAddress.HostToNetworkOrder((int)ip)).ToString();

        #endregion

        //--------------------------------------------------------------------------
    }
}
