﻿namespace XFilter.Shared.Database
{
    using NLog;
    using System.Data;
    using System.Data.SqlClient;
    using System.Threading.Tasks;
    using XFilter.Shared.Helpers;
    using System.Collections.Generic;

    public class XSqlConnection
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private string _connStr;
        private SqlConnection _connection;

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public XSqlConnection(string connStr)
        {
            _connStr = connStr;
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Checks if connection is currently open.
        /// </summary>
        /// <returns></returns>
        public bool IsOpen()
        {
            if (_connection == null)
                return false;
            
            if (_connection.State == ConnectionState.Connecting ||
                _connection.State == ConnectionState.Executing ||
                _connection.State == ConnectionState.Fetching ||
                _connection.State == ConnectionState.Open)
                return true;

            return false;
        }

        /// <summary>
        /// Attempts to establish connection to the database.
        /// </summary>
        /// <returns>Boolean</returns>
        public async Task<bool> Connect()
        {
            if(this.IsOpen())
            {
                Logger.Warn("SQL Connection is already open...");
                return true;
            }

            _connection = new SqlConnection(_connStr);
            try
            {
                await _connection.OpenAsync();
            }
            catch { return false; }
            return true;
        }

        /// <summary>
        /// Closes the SQL connection if its open.
        /// </summary>
        public void Disconnect()
        {
            if (!this.IsOpen())
                return;

            try
            {
                _connection.Close();
            }
            catch { Logger.Error($"Failed to close sql connection."); }
        }


        /// <summary>
        /// Executes query, returns amount of updated rows.
        /// </summary>
        /// <param name="str">Query string (can be formatted).</param>
        /// <param name="args">Arguments as in any other formatted string.</param>
        /// <returns>Updated row count. Can be -1 if no rows were updated (fetch operation).</returns>
        public async Task<int?> ExecuteNonQuery(string str, params object[] args)
        {
            var cmd = CreateSqlCommand(str, args);
            if(cmd == null)
            {
                Logger.Error($"Failed to execute. Format dump: {StringExtensions.DumpFormat(str, args)}");
                return null;
            }

            return await ExecuteNonQuery(cmd);
        }

        /// <summary>
        /// Executes query for given SqlCommand, returns amount of updated rows.
        /// </summary>
        /// <param name="cmd">The sql command.</param>
        /// <returns>null on command failure, -1 if no rows updated, otherwise updated row count.</returns>
        public async Task<int?> ExecuteNonQuery(SqlCommand cmd)
        {
            Task<int?> result = null;
            try
            {
                using (cmd)
                    result = Task.FromResult<int?>(await cmd.ExecuteNonQueryAsync());
            }
            catch
            {
                // i will get this catch if connection dropped, right? yeah, because sql command will fail :) actually
                // i think that u will get exception at CreateSqlCommand
                Logger.Error($"Could not execute SQL command (format: {cmd.CommandText})");
                return null;
            }

            return result.Result;
        }

        /// <summary>
        /// Creates sql command from classic TSQL query / args.
        /// </summary>
        /// <param name="str">The query string (format).</param>
        /// <param name="args"></param>
        /// <returns></returns>
        private SqlCommand CreateSqlCommand(string str, params object[] args)
        {
            SqlCommand result = null;
            str = StringExtensions.TryFormat(str, args);
            if(str == null)
            {
                Logger.Error($"Failed to format str {str}.");
                return result; //null
            }

            result = new SqlCommand(str, _connection);
            return result;
        }

        /// <summary>
        /// Gets data from database (from classic TSQL query).
        /// </summary>
        /// <param name="str">The query string (format).</param>
        /// <param name="args">The string format arguments.</param>
        /// <returns>null on failure, Task SqlDataReader on success.</returns>
        public async Task<SqlDataReader> GetReaderResult(string str, params object[] args)
        {
            var cmd = CreateSqlCommand(str, args);
            if (cmd == null)
            {
                Logger.Error($"Failed to execute. Format dump: {StringExtensions.DumpFormat(str, args)}");
                return null;
            }
            return await GetReaderResult(cmd);
        }

        /// <summary>
        /// Gets data from database (using user-supplied SqlCommand).
        /// </summary>
        /// <param name="cmd">The command.</param>
        /// <returns>null on failure, Task SqlDataReader on success.</returns>
        public async Task<SqlDataReader> GetReaderResult(SqlCommand cmd)
        {
            Task<SqlDataReader> result = null;
            try
            {
                using (cmd)
                    result = Task.FromResult(await cmd.ExecuteReaderAsync());
            }
            catch { return null; }
            return result.Result;
        }

        public async Task<object> ExecuteProcedure(string str, params object[] args)
        {
            var cmd = CreateSqlCommand(str);
            if (cmd == null)
            {
                Logger.Error($"Failed to execute. Format dump: {StringExtensions.DumpFormat(str, args)}");
                return null;
            }
            //TESTME men
            if (args.Length > 0 && args.Length % 2 == 0)
            {
                for (int argindex = 0; argindex < args.Length; argindex += 2)
                {
                    //coool, no errors here 100%. 
                    cmd.Parameters.AddWithValue(args[argindex].ToString(), args[argindex + 1]);
                }
            }
            else
            {
                Logger.Error($"Could not execute procedure. Incorrect args.");
                return -1;
            }
            return await ExecuteProcedure(cmd);
        }

        public async Task<object> ExecuteProcedure(SqlCommand cmd)
        {
            Task<object> result = null;
            try
            {
                using (cmd)
                    result = Task.FromResult<object>(await cmd.ExecuteNonQueryAsync());
            }
            catch { return null; }
            return result;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
