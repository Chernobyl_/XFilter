﻿using System.Diagnostics;

namespace XFilter.Shared.Collections
{
    /// <summary>
    /// Work item class.
    /// </summary>
    class WorkItem : IWorkItem
    {
        #region IWorkItem

        private Stopwatch _executionTime;

        /// <summary>
        /// Gets the current work execution state.
        /// </summary>
        public WorkState State { get; private set; }

        /// <summary>
        /// Occours when the work state is changed.
        /// </summary>
        public event WorkStateChangedEventHandler WorkStateChanged;

        /// <summary>
        /// Initializes a new instance of <see cref="WorkItem"/> class.
        /// </summary>
        public WorkItem()
        {
            _executionTime = new Stopwatch();
            this.WorkStateChanged.Invoke(this, new WorkItemEventArgs(WorkState.Idle));
        }

        #endregion
    }
}
