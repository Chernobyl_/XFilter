﻿using System.Collections.Generic;

namespace XFilter.Shared.Collections
{
    /// <summary>
    /// Circular buffer interface.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ICircularBuffer<T> : IEnumerable<T>
    {
        /// <summary>
        /// Gets the flag indicating if the buffer is fully populated.
        /// </summary>
        bool IsPopulated { get; }

        /// <summary>
        /// Adds value to the buffer, oldest element gets removed if capacity reached.
        /// </summary>
        /// <param name="item"></param>
        void Add(T item);

        /// <summary>
        /// Resets the buffer.
        /// </summary>
        void Reset();
    }
}
