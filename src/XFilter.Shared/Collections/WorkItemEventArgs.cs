﻿using System;

namespace XFilter.Shared.Collections
{
    /// <summary>
    /// Work execution progress state.
    /// </summary>
    public enum WorkState
    {
        Idle,
        Started,
        Executing,
        Finished
    }

    /// <summary>
    /// Event args for <see cref="IWorkItem"/> which contains the work state.
    /// </summary>
    public sealed class WorkItemEventArgs : EventArgs
    {
        /// <summary>
        /// Gets the current work state.
        /// </summary>
        public WorkState State { get; private set; }

        /// <summary>
        /// Initialize a new instance of <see cref="WorkItemEventArgs"/>
        /// </summary>
        /// <param name="state"></param>
        public WorkItemEventArgs(WorkState newState)
        {
            this.State = newState;
        }
    }
}
