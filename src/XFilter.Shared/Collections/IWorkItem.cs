﻿namespace XFilter.Shared.Collections
{
    /// <summary>
    /// A delegate which is executed when work execution state got changed.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The new work execution state.</param>
    public delegate void WorkStateChangedEventHandler(object sender, WorkItemEventArgs e);

    /// <summary>
    /// Work item interface.
    /// </summary>
    public interface IWorkItem
    {
        /// <summary>
        /// Gets the current work execution state.
        /// </summary>
        WorkState State { get; }

        /// <summary>
        /// Occours when the work state is changed.
        /// </summary>
        event WorkStateChangedEventHandler WorkStateChanged;
    }
}
