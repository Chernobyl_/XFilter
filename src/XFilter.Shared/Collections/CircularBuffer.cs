﻿using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace XFilter.Shared.Collections
{
    /// <summary>
    /// Circular buffer class. If the capacity reached the last (oldest) 
    /// element gets removed.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CircularBuffer<T> : ICircularBuffer<T>, IEnumerable<T>
    {
        #region Constructors

        public CircularBuffer(int capacity)
        {
            _capacity = capacity;
            _queue = new ConcurrentQueue<T>();
        }

        #endregion

        #region ICircularBuffer

        private ConcurrentQueue<T> _queue;
        private int _capacity;

        /// <summary>
        /// Gets the flag indicating if the buffer is fully populated.
        /// </summary>
        public bool IsPopulated { get; private set; }

        /// <summary>
        /// Adds value to the buffer, oldest element gets removed if capacity reached.
        /// </summary>
        /// <param name="item"></param>
        public void Add(T item)
        {
            if (this.IsPopulated)
                _queue.TryDequeue(out T tmp);
            _queue.Enqueue(item);
        }

        /// <summary>
        /// Resets the buffer.
        /// </summary>
        public void Reset()
        {
            while (_queue.TryDequeue(out T tmp)) ;
        }


        #endregion

        #region IEnumerable

        public IEnumerator<T> GetEnumerator() => _queue.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => _queue.GetEnumerator();

        #endregion
    }
}
