﻿using System;
using System.Collections.Concurrent;

namespace XFilter.Shared.Collections
{

    /// <summary>
    /// The generic work queue. Will be improved later.
    /// </summary>
    /// <typeparam name="T">Type of work item (must be derived from WorkItemBase).</typeparam>
    public class WorkQueue<T> where T : WorkItem 
    {
        //--------------------------------------------------------------------------

        #region Private properties, statics, constants

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly BlockingCollection<T> _workQueue;

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public WorkQueue()
        {
            _workQueue = new BlockingCollection<T>();
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        /// <summary>
        /// Count of pending work items.
        /// </summary>
        public int Size => _workQueue.Count;

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Adds work to the queue.
        /// </summary>
        /// <param name="workItem">The work item.</param>
        public void AddWork(T workItem)
        {
            bool added = _workQueue.TryAdd(workItem);
            if (!added)
                Logger.Warn("Adding packet workitem to work queue failed.");
        }

        /// <summary>
        /// Gets work from queue if any.
        /// </summary>
        /// <returns>null if no work.</returns>
        public T GetWork()
        {
            T workItem;

            bool taken = _workQueue.TryTake(out workItem);
            if (!taken)
                return null;

            return workItem;
        }

        /// <summary>
        /// Gets the queue dump string.
        /// </summary>
        /// <returns>Queue dump string.</returns>
        public string DumpState()
        {
            return $"Work queue size {this.Size}" + Environment.NewLine;
        }
        #endregion

        //--------------------------------------------------------------------------
    }
}
