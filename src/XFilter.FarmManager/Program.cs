﻿namespace XFilter.FarmManager
{
    using NLog;
    using System;
    using XFilter.FarmManager.Services;

    class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        
        static void Main(string[] args)
        {
            gs.Service = new ServiceManager();
            bool serviceStarted = gs.Service.Initialize();

            if(!serviceStarted)
            {
                Logger.Fatal("Could not start the service.");
            }
            else
            {
                Logger.Info("Farm manager service initialization success.");
            }

            var cmdParser = new CommandParser();
            cmdParser.Initialize();

            Console.ReadLine();
        }
    }
}
