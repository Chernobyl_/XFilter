﻿namespace XFilter.FarmManager
{
    using System;
    using XFilter.FarmManager.Services;


    /// <summary>
    /// Global static / constant data (Modify on production builds if neccessary).
    /// </summary>
    public static class gs
    {
        public const string NetEngineVersionString = "1.0.0";

        public static readonly string CommentLine = "--------------------" + Environment.NewLine;

        public static ServiceManager Service;
    }
}
