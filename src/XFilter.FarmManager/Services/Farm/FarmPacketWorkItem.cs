﻿namespace XFilter.FarmManager.Services.Farm
{
    using XFilter.Shared.Collections;
    using XFilter.Shared.Network;


    public sealed class FarmPacketWorkItem : WorkItem
    {
        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// The ctor.
        /// </summary>
        /// <param name="context">The client context.</param>
        /// <param name="packet">The packet to process</param>
        public FarmPacketWorkItem(FarmUserContext context, XPacket packet) : base()
        {
            this.ClientContext = context;
            this.Packet = packet;
        }


        #endregion

        //--------------------------------------------------------------------------

        #region Public properties & bugslayer related

        /// <summary>
        /// The client context.
        /// </summary>
        public readonly FarmUserContext ClientContext;

        /// <summary>
        /// The packet that needs to be processed.
        /// </summary>
        public readonly XPacket Packet;

        #endregion

        //--------------------------------------------------------------------------
    }
}
