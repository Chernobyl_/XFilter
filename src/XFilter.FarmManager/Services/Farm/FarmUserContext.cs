﻿namespace XFilter.FarmManager.Services.Farm
{
    using System;
    using System.Net.Sockets;
    using XFilter.DataModel;
    using XFilter.Shared.Collections;
    using XFilter.Shared.Helpers;
    using XFilter.Shared.Network;


    /// <summary>
    /// The connected client context.
    /// </summary>
    public sealed class FarmUserContext
    { 
        //--------------------------------------------------------------------------

        #region Private properties & constants

        private Socket _client;
        private ByteBuffer _buffer;
        private SrSecurity _security;

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public FarmUserContext()
        {
            _security = new SrSecurity();
            _security.ChangeIdentity("X_FarmManager", 0);
            _buffer = gs.Service.Farm.BufferManager.GetBuffer();
            this.State = new FarmSlaveState();
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        /// <summary>
        /// The farm client context service state.
        /// </summary>
        public FarmSlaveState State { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic
        
        /// <summary>
        /// Starts receiving & dispatching data from given client socket.
        /// </summary>
        /// <param name="client"></param>
        public void Start(Socket client)
        {
            _client = client;
            this.State.IpStr = NetHelper.GetSockIpAddrStr(client);
            _buffer.Pulse();

            ReceiveData();
        }

        /// <summary>
        /// Stops the client context.
        /// </summary>
        public void Stop()
        {
            NetHelper.CloseSocket(_client);
            if (_buffer.IsUsed)
                gs.Service.Farm.BufferManager.ReleaseBuffer(_buffer);
        }

        /// <summary>
        /// Initializes receiving data op.
        /// </summary>
        private void ReceiveData()
        {
            try
            {
                _client.BeginReceive(_buffer.Buffer, 0, _buffer.Buffer.Length, SocketFlags.None,
                    ReceiveDataCallback, null);

                _buffer.Pulse();
            }
            catch { Stop(); }
        }


        /// <summary>
        /// Fired once there is some data to process (or 0).
        /// </summary>
        /// <param name="iar">IAsyncResult</param>
        private void ReceiveDataCallback(IAsyncResult iar)
        {
            try
            {
                int recvCount = _client.EndReceive(iar);
                if(recvCount == 0)
                {
                    Stop();
                    return;
                }

                _buffer.Pulse();

                _security.Recv(_buffer.Buffer, 0, recvCount);
                var packets = _security.TransferIncoming();

                if (packets != null)
                {
                    foreach (var packet in packets)
                    {
                        gs.Service.Farm.PacketDispatcher.AddWork(new FarmPacketWorkItem(this, packet));
                    }
                }

                FlushOutgoing();
                ReceiveData();
            }
            catch { Stop(); }
        }


        /// <summary>
        /// Flushes all outgoing buffers to network.
        /// </summary>
        public void FlushOutgoing()
        {
            try
            {
                var data = _security.TransferOutgoing();

                //Nothing to flush.
                if (data == null)
                    return;

                //TODO: Statistics measurement.
                foreach (var kvp in data)
                {
                    SendData(kvp.Key.Buffer, kvp.Key.Buffer.Length);
                }
            }
            catch { Stop(); }
        }


        /// <summary>
        /// Enqueue packet for sending.
        /// </summary>
        /// <param name="packet">The packet.</param>
        public void Send(XPacket packet, bool flush = true)
        {
            _security.Send(packet);
            if (flush)
                FlushOutgoing();
        }

        /// <summary>
        /// Starts sending data to the client.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        /// <param name="count">The count of bytes to send.</param>
        public void SendData(byte[] buffer, int count)
        {
            try
            {
                _client.BeginSend(buffer, 0, count, SocketFlags.None,
                    SendDataCallback, null);

                _buffer.Pulse();
            }
            catch { Stop(); }
        }

        /// <summary>
        /// Data sending callback.
        /// </summary>
        /// <param name="iar">IAsyncResult</param>
        private void SendDataCallback(IAsyncResult iar)
        {
            try
            {
                _client.EndSend(iar);

                _buffer.Pulse();
            }
            catch { Stop(); }
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
