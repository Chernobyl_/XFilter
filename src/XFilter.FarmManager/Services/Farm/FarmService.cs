﻿using NLog;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using XFilter.DataModel.DTO.Entities;
using XFilter.DataModel.Models;
using XFilter.FarmManager.Protocol.Farm;
using XFilter.Shared.Collections;
using XFilter.Shared.Interfaces;
using XFilter.Shared.Network;
using System.Net;


namespace XFilter.FarmManager.Services.Farm
{

    public sealed class FarmService : INetworkService
    {
        //--------------------------------------------------------------------------

        #region Private properties, static and constants

        private Task _dbPollTask;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Managers & public API

        /// <summary>
        /// The farm manager settings storage, EF.
        /// </summary>
        public FarmConfigEntity Settings
        { get; private set; }

        /// <summary>
        /// The service byte buffer manager (sessions etc).
        /// </summary>
        public ByteBufferManager BufferManager
        { get; private set; }

        /// <summary>
        /// The main packet dispatcher for service.
        /// </summary>
        public FarmPacketDispatcher PacketDispatcher
        { get; private set; }
        
        /// <summary>
        /// The socket acceptor.
        /// </summary>
        public ISocketListener SocketListener
        { get; private set; }

        /// <summary>
        /// The user context manager.
        /// </summary>
        public FarmContextManager ContextManager
        { get; private set; }

        /// <summary>
        /// The Entity Framework database manager.
        /// </summary>
        public XServiceDBEntities DatabaseManager
        { get; private set; }

        /// <summary>
        /// The server ID
        /// </summary>
        public int ServerID
        { get; private set; }

        #endregion


        //--------------------------------------------------------------------------

        #region Initialization

        /// <summary>
        /// Reads settings from json file, or generates default if neccessary.
        /// </summary>
        private bool SetupConfiguration()
        {
            this.DatabaseManager = new XServiceDBEntities();
            try
            {
                DatabaseManager.Database.Connection.Open();

            }
            catch
            {
                Logger.Fatal("Database connection failed.");
                return false;
            }

            this.ServerID = int.Parse(File.ReadAllText("farm_id.txt"));
            this.Settings = FarmConfigEntity.FromModel(this.DatabaseManager, this.ServerID);

            if(this.Settings == null)
            {
                Logger.Fatal("Failed to read farm manager settings. Invalid referenced server ID ?");
                return false;
            }
            else
            {
                Logger.Info("Configuration loaded.");
                return true;
            }
        }

        /// <summary>
        /// Initializes everything required for service to run.
        /// </summary>
        /// <returns>true / false</returns>
        public bool Initialize()
        {

            //Generate default configuration if neccessary
            if (!SetupConfiguration())
                return false;

            var netCfg = this.Settings.NetEngineConfig;

            this.PacketDispatcher = new FarmPacketDispatcher();
            this.PacketDispatcher.Initialize();

            this.ContextManager = new FarmContextManager();
            this.ContextManager.Initialize();

            this.SocketListener = new SocketListener(
                new IPEndPoint(IPAddress.Parse(netCfg.BindAddress).Address, (int)netCfg.BindPort), 100, false);

                //new IPEndPoint(IPAddress.Parse(netCfg.BindAddress), netCfg.BindPort), 100, true);

            this.SocketListener.SocketAccepted += this.ContextManager.EnqueueStartup;
            this.SocketListener.Start();
            //this.SocketListener.Initialize(netCfg.BindAddress, netCfg.BindPort, this.ContextManager.EnqueueStartup);


            Logger.Info("Switching farm service to active mode.");


            //change settings

            this.Settings.ServerConfig.ServerName = "FarmManager";
            FarmConfigEntity.Save(this.DatabaseManager, this.Settings, this.ServerID);

            this.SocketListener.CreateNewSessions = true;

            _dbPollTask = Task.Run(() => DatabasePollTaskWorker());
            return true;
        }

        public void Deinitialize()
        {
           // Logger.Warn("Farm service is stopping now.");

        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        //--------------------------------------------------------------------------

        private void ProcessPendingRestarts()
        {
            
            //Logger.Info("Searching for servers to update...");

            var toRestart = this.DatabaseManager.C_SlaveServerMappings;

     

            foreach (var entity in DatabaseManager.ChangeTracker.Entries())
                entity.Reload();
            
            foreach (var rec in toRestart)
            {
                if(rec.RestartRequired)
                {
                    Logger.Warn($"Restarting srv Slave {rec.SlaveServerID}, RefSrv {rec.RefServerID}.");
                    ServerUpdateRequest.StartOrRestart((int)rec.SlaveServerID, rec.RefServerID);
                    rec.RestartRequired = false;
                }
            }
        
            DatabaseManager.SaveChanges();
        }


        private void DatabasePollTaskWorker()
        {
            while(true)
            {
      
                if (DatabaseManager.Database.Connection.State != System.Data.ConnectionState.Open)
                {
                    System.Threading.Thread.Sleep(100);
                    continue;
                }

                try
                {
                    ProcessPendingRestarts();
                }
                catch(Exception ex)
                {
                    Logger.Fatal($"ProcessPendingRestarts() failed with ex {ex}.");
                }
                System.Threading.Thread.Sleep(3000);
            }
        }

      

        public string DumpServiceState()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(gs.CommentLine);
            builder.Append("Begin service state dump" + Environment.NewLine);
            builder.Append($"Net engine version {gs.NetEngineVersionString}" + Environment.NewLine);
            builder.Append(gs.CommentLine);
            builder.Append("Begin context manager dump" + Environment.NewLine);
            builder.Append(this.ContextManager.DumpState());
            builder.Append("End context manager dump" + Environment.NewLine);
            builder.Append(gs.CommentLine);
            builder.Append("Begin packet dispatcher dump" + Environment.NewLine);
            builder.Append(this.PacketDispatcher.DumpState());
            builder.Append("End packet dispatcher dump");
            return builder.ToString();
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
