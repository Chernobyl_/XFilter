﻿namespace XFilter.FarmManager.Services.Farm
{
    using NLog;
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using XFilter.DataModel.Network.Ipc;
    using XFilter.FarmManager.Network;
    using XFilter.FarmManager.Protocol.Farm;
    using XFilter.Shared.Collections;


    /// <summary>
    /// Farm manager packet dispatcher
    /// </summary>
    public sealed class FarmPacketDispatcher
    {
        //--------------------------------------------------------------------------

        #region Private properties & constants

        private readonly Dictionary<IpcOpcode, IFarmPacketHandler> _handlers;

        private WorkQueue<FarmPacketWorkItem> _workQueue;
        private Task[] _workTasks;
        private CancellationTokenSource _workCTS;
        private CancellationToken _workCT;


        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public FarmPacketDispatcher()
        {
            _handlers = new Dictionary<IpcOpcode, IFarmPacketHandler>();

            _workQueue = new WorkQueue<FarmPacketWorkItem>();
            _workCTS = new CancellationTokenSource();
            _workCT = _workCTS.Token;

            this.DefaultHandler = new DefaultPacketHandler();
            this.ProcessedCount = 0;
        }

        #endregion

        //--------------------------------------------------------------------------


        #region Public properties etc

        /// <summary>
        /// The default packet handler.
        /// </summary>
        public IFarmPacketHandler DefaultHandler;

        /// <summary>
        /// The amount of packets pending in queue (work queue).
        /// </summary>
        public long WorkQueueSize => _workQueue.Size;

        /// <summary>
        /// The total count of processed packets.
        /// </summary>
        public long ProcessedCount;

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Initializes the packet dispatcher.
        /// </summary>
        public void Initialize()
        {
            SetupHandlers();
            SetupWorkTasks();
        }

        /// <summary>
        /// Stops packet dispatchment.
        /// </summary>
        public void Deinitialize()
        {
            StopWorkTasks();
        }

        /// <summary>
        /// Set your handlers here.
        /// </summary>
        private void SetupHandlers()
        {
            _handlers[IpcOpcode.Authentication] = new AuthenticationHandler();
            _handlers[IpcOpcode.SlaveServerUpdate] = new ServerInfoRequestHandler();
        }

        /// <summary>
        /// Prepares the processing tasks.
        /// </summary>
        private void SetupWorkTasks()
        {
            var netCfg = gs.Service.Farm.Settings.NetEngineConfig;

            _workTasks = new Task[netCfg.WorkQueueProcessTaskCount];
            for(int i = 0; i < netCfg.WorkQueueProcessTaskCount; i++)
            {
                _workTasks[i] = Task.Run(() => PacketProcessWorker(), _workCT);
                //TODO: Wait to start (is it really neccessary?)
            }

            Logger.Info("Server main process task started.");
        }

        /// <summary>
        /// Terminates all currently running processing tasks.
        /// </summary>
        private void StopWorkTasks()
        {
            //NOTE: Is CancellationTokenSource reusable? ... ???
            _workCTS.Cancel();
        }

        /// <summary>
        /// The actual packet/context processing callee.
        /// TODO: Serious lag/overlap detection & task restart.
        /// </summary>
        /// <param name="workItem">The work item.</param>
        private void ProcessWorkItem(FarmPacketWorkItem workItem)
        {
            workItem.OnProcessingStart();

            var handler = GetHandler(workItem.Packet.Opcode);
            if (handler != null)
            {
                try
                {
                    handler.Execute(workItem.ClientContext, workItem.Packet);
                }
                catch(Exception ex)
                {
                    Logger.Error("Failed to execute handler. Opcode: {0:X}. Ex: {1}.", workItem.Packet.Opcode, ex);
                }
            }
            workItem.OnProcessingEnd();

            workItem.IsFinished = true;

            Interlocked.Increment(ref this.ProcessedCount);

            //TODO: Do more checks here?
            //Logger.Info($"Work item process time: {workItem.ExecutionTime}");
        }

        /// <summary>
        /// The main packet processing worker (multi-task).
        /// </summary>
        private void PacketProcessWorker()
        {
            while(true)
            {
                if (_workCT.IsCancellationRequested)
                {
                   // Logger.Debug("Process task is stopping.");
                    break;
                }

                try
                {
                    var item = _workQueue.GetWork();
                    if (item != null)
                        ProcessWorkItem(item);
                }
                catch
                {
                    //TODO: Dump here if neccessary
                    Logger.Error("Error processing packet in queue."); 
                }
                Task.Delay(1).Wait();
            }
        }

        /// <summary>
        /// Gets the appropriate handler for given opcode.
        /// </summary>
        /// <param name="opcode">The opcode.</param>
        /// <returns>Default handler if none found.</returns>
        private IFarmPacketHandler GetHandler(ushort opcode)
        {
           
            if (!Enum.IsDefined(typeof(IpcOpcode), (IpcOpcode)opcode))
                return this.DefaultHandler;
            return _handlers[(IpcOpcode)opcode];
        }

        /// <summary>
        /// Adds work to the processing queue.
        /// </summary>
        /// <param name="workItem"></param>
        public void AddWork(FarmPacketWorkItem workItem) => _workQueue.AddWork(workItem);

        /// <summary>
        /// Gets work from the work queue (null if none).
        /// </summary>
        /// <returns></returns>
        private FarmPacketWorkItem GetWork() => _workQueue.GetWork();

        public string DumpState()
        {
            return string.Format("Work queue size: {0}{1}Total packets processed: {2}{3}",
                this.WorkQueueSize, Environment.NewLine,
                this.ProcessedCount, Environment.NewLine);
        }
       
        #endregion

        //--------------------------------------------------------------------------
    }
}
