﻿using NLog;
using System.Linq;
using XFilter.DataModel.Network.Ipc;
using XFilter.FarmManager.Network;
using XFilter.FarmManager.Services.Farm;
using XFilter.Shared.Network;

namespace XFilter.FarmManager.Protocol.Farm
{

    public sealed class AuthenticationHandler : IFarmPacketHandler
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public void Execute(FarmUserContext context, XPacket packet)
        {
            int serverId = packet.ReadValue<int>();
            string key = packet.ReadValue<string>();

            XPacket resp = new XPacket((ushort)IpcOpcode.Authentication);


            var keyMatches = gs.Service.Farm.DatabaseManager.C_SlaveServerMappings.Any(
                rec => rec.ApiKey == key);

            if(!keyMatches)
            {
                resp.WriteValue<int>((int)IpcAuthResponseCode.InvalidApiKey);
                context.Send(resp);

                Logger.Warn("Invalid API key.");

                gs.Service.Farm.ContextManager.Stop(context);

                return;
            }

            if(gs.Service.Farm.Settings.ServerConfig.OnlyAllowedIPs)
            {
                if(!gs.Service.Farm.Settings.AllowedIPs.Addresses.Contains(context.State.IpStr))
                {
                    resp.WriteValue<int>((int)IpcAuthResponseCode.InvalidHost);
                    context.Send(resp);

                    Logger.Warn($"Host is not allowed to access this farm. IP: {context.State.IpStr}");
                    gs.Service.Farm.ContextManager.Stop(context);
                    return;
                }
            }

            context.State.Id = serverId;
            context.State.ApiKey = key;
            context.State.IsHandshaked = true;

            resp.WriteValue<int>((int)IpcAuthResponseCode.Success);
            context.Send(resp);
            Logger.Info($"Handshake with {context.State.IpStr} successful.");
        }
    }
}
