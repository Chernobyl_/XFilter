﻿namespace XFilter.FarmManager.Protocol.Farm
{
    using NLog;
    using XFilter.FarmManager.Network;
    using XFilter.FarmManager.Services.Farm;
    using XFilter.Shared.Network;


    public sealed class ServerInfoRequestHandler : IFarmPacketHandler
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public void Execute(FarmUserContext context, XPacket packet)
        {
            ServerUpdateRequest.StartOrRestart(context.State.Id);
        }
    }
}
