﻿namespace XFilter.FarmManager.Network
{
    using XFilter.FarmManager.Services.Farm;
    using XFilter.Shared.Network;


    public interface IFarmPacketHandler
    {
        void Execute(FarmUserContext context, XPacket packet);
    }
}
