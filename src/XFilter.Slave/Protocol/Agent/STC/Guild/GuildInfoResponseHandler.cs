﻿using NLog;
using System.Threading.Tasks;
using XFilter.DataModel.Protocol.Agent;
using XFilter.Shared.Network;
using XFilter.Slave.Interfaces;
using XFilter.Slave.Services;
using XFilter.Slave.Services.Agent;

namespace XFilter.Slave.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_GUILD_INFO_DATA = 0x3101
    /// TESTME
    /// </summary>
    class GuildInfoResponseHandler : IRelayPacketHandler
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public Task<bool> Execute(RelayClientContextBase context, XPacket packet) =>
            this.ExecuteEx(context as AgentContext, packet);

        private Task<bool> ExecuteEx(AgentContext context, XPacket packet)
        {
            //Pass original packet through. Handle end if it has data.
       

            var entity = new GuildInfoEntity();
            bool isOrigEnd = entity.ReadFromPacket(packet);

            if (!isOrigEnd)
            {
                context.Send(PacketSource.Server, packet);
                return Task.FromResult<bool>(true);
            }

            var packets = entity.ToPackets();

            foreach (var pck in packets)
                context.Send(PacketSource.Server, pck);

            Logger.Debug("Entity handled.");
         
            return Task.FromResult<bool>(true);
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
