﻿using NLog;
using System.Threading.Tasks;
using XFilter.DataModel.Protocol.Agent;
using XFilter.Shared.Network;
using XFilter.Slave.Interfaces;
using XFilter.Slave.Services;
using XFilter.Slave.Services.Agent;

namespace XFilter.Slave.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_GUILD_UPDATE_RESPONSE = 0x38F5
    /// TESTME
    /// </summary>
    class GuildUpdateResponseHandler : IRelayPacketHandler
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public Task<bool> Execute(RelayClientContextBase context, XPacket packet) =>
            this.ExecuteEx(context as AgentContext, packet);

        private Task<bool> ExecuteEx(AgentContext context, XPacket packet)
        {
            //Pass original packet through. Handle end if it has data.
            
            var entity = new GuildUpdateResponseEntity();
            bool parsed = entity.ReadFromPacket(packet);

            Logger.Debug("Entity handled.");

            if (!parsed)
            {
                context.Send(PacketSource.Server, packet);
                return Task.FromResult<bool>(true);
            }


            context.Send(PacketSource.Server, entity.WriteToPacket());

            return Task.FromResult<bool>(true);
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
