﻿using NLog;
using System.Threading.Tasks;
using XFilter.DataModel.Protocol.Agent;
using XFilter.Shared.Network;
using XFilter.Slave.Interfaces;
using XFilter.Slave.Services;
using XFilter.Slave.Services.Agent;

namespace XFilter.Slave.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_LOGOUT_RESPONSE = 0xB005
    /// TESTME
    /// </summary>
    class LogoutResponseHandler : IRelayPacketHandler
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public Task<bool> Execute(RelayClientContextBase context, XPacket packet) =>
        this.ExecuteEx(context as AgentContext, packet);

        private Task<bool> ExecuteEx(AgentContext context, XPacket packet)
        {
            var entity = new LogoutResponseEntity();
            entity.ReadFromPacket(packet);


            Logger.Debug($"Ms since last logout: {context.State.MsSinceLastLogoutAttempt()}.");
            //Measure the logout time...
            context.State.MeterLogoutTime();

            Logger.Debug("Entity handled.");

            context.Send(PacketSource.Server, entity.WriteToPacket());
            return Task.FromResult<bool>(true);
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
