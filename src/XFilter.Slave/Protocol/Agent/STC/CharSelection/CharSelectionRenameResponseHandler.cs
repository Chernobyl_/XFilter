﻿using NLog;
using System;
using System.Threading.Tasks;
using XFilter.DataModel.Protocol.Agent;
using XFilter.Shared.Network;
using XFilter.Slave.Interfaces;
using XFilter.Slave.Services;
using XFilter.Slave.Services.Agent;

namespace XFilter.Slave.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_CHARACTER_SELECTION_RENAME_RESPONSE = 0xB450
    /// TESTME
    /// </summary>
    class CharSelectionRenameResponseHandler : IRelayPacketHandler
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public Task<bool> Execute(RelayClientContextBase context, XPacket packet) =>
          this.ExecuteEx(context as AgentContext, packet);

        private Task<bool> ExecuteEx(AgentContext context, XPacket packet)
        {
            var action = (CharSelectionNameAction)packet.ReadValue<byte>();
            byte resByte = packet.ReadValue<byte>();

            if (!Enum.IsDefined(typeof(CharSelectionNameErrorCode), resByte))
            {
                Logger.Error($"Unknown char selection rename response code {resByte}.");
                context.Send(PacketSource.Server, packet);
                return Task.FromResult<bool>(true);
            }
            
            Logger.Debug("Entity handled.");

            context.Send(PacketSource.Server, packet);
            return Task.FromResult<bool>(true);
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
