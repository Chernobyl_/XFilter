﻿using NLog;
using System.Threading.Tasks;
using XFilter.DataModel.Protocol.Agent;
using XFilter.Shared.Network;
using XFilter.Slave.Interfaces;
using XFilter.Slave.Services;
using XFilter.Slave.Services.Agent;

namespace XFilter.Slave.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_STALL_CREATE_RESPONSE = 0xB0B1
    /// TESTME
    /// </summary>
    class StallCreateResponseHandler : IRelayPacketHandler
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public Task<bool> Execute(RelayClientContextBase context, XPacket packet) =>
            this.ExecuteEx(context as AgentContext, packet);

        private Task<bool> ExecuteEx(AgentContext context, XPacket packet)
        {
            var entity = new StallCreateResponseEntity();
            entity.ReadFromPacket(packet);

            Logger.Debug($"Ms since last stall creation: {context.State.MsSinceLastStallCreateAttempt()}.");
            //Measure the stall creation time...
            context.State.MeterStallCreateTime();

            Logger.Debug("Entity handled.");

            context.Send(PacketSource.Server, entity.WriteToPacket());
            return Task.FromResult<bool>(true);
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
