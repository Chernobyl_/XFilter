﻿using NLog;
using System.Threading.Tasks;
using XFilter.DataModel.Protocol.Agent;
using XFilter.Shared.Network;
using XFilter.Slave.Interfaces;
using XFilter.Slave.Services;
using XFilter.Slave.Services.Agent;

namespace XFilter.Slave.Protocol.Agent
{
    /// <summary>
    /// S->C CLIENT_AGENT_ENFORCEMENT_REINFORCE_REQUEST = 0xB150
    /// TESTME
    /// </summary>
    class AlchemyReinforceResponseHandler : IRelayPacketHandler
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public Task<bool> Execute(RelayClientContextBase context, XPacket packet) =>
            this.ExecuteEx(context as AgentContext, packet);

        private Task<bool> ExecuteEx(AgentContext context, XPacket packet)
        {
            var entity = new AlchemyReinforceResponseEntity();
            bool fullyParsed = entity.ReadFromPacket(packet);

            //HACK: Prevent client crashing if not full packet structure known.
            if(!fullyParsed)
            {
                Logger.Warn("Alchemy packet not fully parsed. Sending original to client.");
                context.Send(PacketSource.Server, packet);
                return Task.FromResult<bool>(true);
            }

            Logger.Debug("Entity handled.");

            //Execute managers
            gs.Service.GetAgentByRefID(context.RefServiceID).AlchemyManager.HandleAlchemyReinforceResponse(context, entity);

            context.Send(PacketSource.Server, entity.WriteToPacket());

            return Task.FromResult<bool>(true);
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
