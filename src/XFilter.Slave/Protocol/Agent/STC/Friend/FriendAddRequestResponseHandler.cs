﻿using NLog;
using System.Threading.Tasks;
using XFilter.DataModel.Protocol.Agent;
using XFilter.Shared.Network;
using XFilter.Slave.Services;
using XFilter.Slave.Services.Agent;
using XFilter.Slave.Interfaces;

namespace XFilter.Slave.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_FRIEND_ADD_REQUEST_RESPONSE = 0x7302
    /// TESTME
    /// </summary>
    class FriendAddRequestResponseHandler : IRelayPacketHandler
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public Task<bool> Execute(RelayClientContextBase context, XPacket packet) =>
            this.ExecuteEx(context as AgentContext, packet);

        private Task<bool> ExecuteEx(AgentContext context, XPacket packet)
        {
            var entity = new FriendAddRequestResponseEntity();
            entity.ReadFromPacket(packet);

            Logger.Debug("Entity handled.");

            context.Send(PacketSource.Server, entity.WriteToPacket());
            return Task.FromResult<bool>(true);
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
