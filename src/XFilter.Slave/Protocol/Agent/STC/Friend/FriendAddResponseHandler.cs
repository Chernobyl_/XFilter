﻿using NLog;
using System.Threading.Tasks;
using XFilter.DataModel.Protocol.Agent;
using XFilter.Shared.Network;
using XFilter.Slave.Interfaces;
using XFilter.Slave.Services;
using XFilter.Slave.Services.Agent;

namespace XFilter.Slave.Protocol.Agent
{

    /// <summary>
    /// S->C SERVER_AGENT_FRIEND_ADD_RESULT_RESPONSE = 0xB302
    /// TESTME
    /// </summary>
    class FriendAddResponseHandler : IRelayPacketHandler
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public Task<bool> Execute(RelayClientContextBase context, XPacket packet) =>
            this.ExecuteEx(context as AgentContext, packet);

        private Task<bool> ExecuteEx(AgentContext context, XPacket packet)
        {
            var entity = new FriendAddResultResponseEntity();
            bool fullyParsed = entity.ReadFromPacket(packet);


            if (!fullyParsed)
            {
                Logger.Error("Entity not fully parsed.");

                context.Send(PacketSource.Server, packet);
                return Task.FromResult<bool>(true);
            }

            Logger.Info($"Entity {entity.Code} {entity.ResultCode} {entity.Accepted}.");

            Logger.Debug("Entity handled.");

            context.Send(PacketSource.Server, entity.WriteToPacket());
            return Task.FromResult<bool>(true);
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
