﻿using NLog;
using System.Threading.Tasks;
using XFilter.DataModel.Protocol.Agent;
using XFilter.Shared.Network;
using XFilter.Slave.Interfaces;
using XFilter.Slave.Services;
using XFilter.Slave.Services.Agent;

namespace XFilter.Slave.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_MOVEMENT_TELEPORT_REQUEST = 0x705A
    /// TESTME, INCOMPLETE (only normal tps supported atm).
    /// </summary>
    class TeleportRequestHandler : IRelayPacketHandler
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public Task<bool> Execute(RelayClientContextBase context, XPacket packet) =>
            this.ExecuteEx(context as AgentContext, packet);

        private Task<bool> ExecuteEx(AgentContext context, XPacket packet)
        {
            Logger.Warn(XFilter.Shared.Helpers.DebugHelper.HexDump(packet.GetBytes()));

            var entity = new TeleportRequestEntity();
            bool fullyParsed = entity.ReadFromPacket(packet);

            if(!fullyParsed)
            {
                Logger.Error("Entity not fully parsed.");
                
                context.Send(PacketSource.Client, packet);
                return Task.FromResult<bool>(true);
            }

            Logger.Debug("Entity handled.");

            context.Send(PacketSource.Client, entity.WriteToPacket());
            return Task.FromResult<bool>(true);
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
