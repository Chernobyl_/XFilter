﻿using NLog;
using System.Threading.Tasks;
using XFilter.DataModel.Protocol.Agent;
using XFilter.Shared.Network;
using XFilter.Slave.Interfaces;
using XFilter.Slave.Services;
using XFilter.Slave.Services.Agent;


namespace XFilter.Slave.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_MOVEMENT_REQUEST = 0x7021
    /// TESTME
    /// </summary>
    class MovementRequestHandler : IRelayPacketHandler
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public Task<bool> Execute(RelayClientContextBase context, XPacket packet) =>
            this.ExecuteEx(context as AgentContext, packet);

        private Task<bool> ExecuteEx(AgentContext context, XPacket packet)
        {
            var entity = new MovementRequestEntity();
            entity.ReadFromPacket(packet);

            var svc = gs.Service.GetAgentByRefID(context.RefServiceID);
            svc.MovementManager.HandleClientMovement(context, entity);


            Logger.Debug("Entity handled.");

            context.Send(PacketSource.Client, entity.WriteToPacket());
            return Task.FromResult<bool>(true);
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
