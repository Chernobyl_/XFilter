﻿using System.Threading.Tasks;
using XFilter.DataModel.Protocol.Agent;
using XFilter.Shared.Network;
using XFilter.Slave.Interfaces;
using XFilter.Slave.Services;
using XFilter.Slave.Services.Agent;

namespace XFilter.Slave.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_AUTH_REQUEST = 0x6103
    /// TESTME
    /// </summary>
    class AuthRequestHandler : IRelayPacketHandler
    {
        //--------------------------------------------------------------------------

        #region Logic

        public Task<bool> Execute(RelayClientContextBase context, XPacket packet) =>
           this.ExecuteEx(context as AgentContext, packet);

        private Task<bool> ExecuteEx(AgentContext context, XPacket packet)
        {
            var entity = new AuthRequestEntity();
            entity.ReadFromPacket(packet);

            context.Send(PacketSource.Client, entity.WriteToPacket());

            return Task.FromResult<bool>(true);
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
