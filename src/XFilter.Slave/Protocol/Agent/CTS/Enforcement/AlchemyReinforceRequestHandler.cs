﻿using NLog;
using System.Threading.Tasks;
using XFilter.DataModel.Protocol.Agent;
using XFilter.Shared.Network;
using XFilter.Slave.Interfaces;
using XFilter.Slave.Services;
using XFilter.Slave.Services.Agent;


namespace XFilter.Slave.Protocol.Agent
{
    /// <summary>
    /// C->S  CLIENT_AGENT_ENFORCEMENT_REINFORCE_REQUEST = 0x7150
    /// TESTME
    /// </summary>
    class AlchemyReinforceRequestHandler : IRelayPacketHandler
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public Task<bool> Execute(RelayClientContextBase context, XPacket packet) =>
            this.ExecuteEx(context as AgentContext, packet);

        private Task<bool> ExecuteEx(AgentContext context, XPacket packet)
        {
            var entity = new AlchemyReinforceRequestEntity();
            var fullyParsed = entity.ReadFromPacket(packet);

            if(!fullyParsed)
            {
                Logger.Warn("Entity not fully handled.");
                context.Send(PacketSource.Client, packet);
                return Task.FromResult<bool>(true);
            }

            Logger.Debug("Entity handled.");

            //Execute managers
            bool ignorePacket = gs.Service.GetAgentByRefID(
                context.RefServiceID).AlchemyManager.HandleAlchemyReinforceRequest(context, entity);

            if(!ignorePacket)
                context.Send(PacketSource.Client, entity.WriteToPacket());

            return Task.FromResult<bool>(true);
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
