﻿namespace XFilter.Slave.Protocol.InterProc
{
    using NLog;
    using XFilter.DataModel.Network.Ipc;
    using XFilter.Shared.Network;
    using XFilter.Slave.Services.InterProc;


    internal sealed class AuthResponseHandler
    {
        //--------------------------------------------------------------------------

        #region Private statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public static void Execute(IpcClientContext client, XPacket packet)
        {
            IpcAuthResponseCode resCode = (IpcAuthResponseCode)packet.ReadValue<int>();

            if(resCode == IpcAuthResponseCode.InvalidApiKey)
            {
                Logger.Error("Invalid API key.");
                client.Terminate();
                return;
            }

            if(resCode == IpcAuthResponseCode.InvalidHost)
            {
                Logger.Info("This host is not allowed to connect.");
                client.Terminate();
                return;
            }

            client.State.IsHandshaked = true;

            Logger.Info("Server accepted handshake. We are ready to go.");

            //Request the server info

            XPacket srvInfoReq = new XPacket((ushort)IpcOpcode.SlaveServerUpdate);
            client.Send(srvInfoReq);

            Logger.Info("Slave server configuration requested");
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
