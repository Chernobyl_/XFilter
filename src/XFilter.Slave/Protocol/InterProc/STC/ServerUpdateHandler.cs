﻿namespace XFilter.Slave.Protocol.InterProc
{
    using NLog;
    using XFilter.DataModel.DTO.Entities;
    using XFilter.DataModel.Models;
    using XFilter.DataModel.Network.Ipc;
    using XFilter.Shared.Helpers;
    using XFilter.Shared.Network;
    using XFilter.Slave.Services.InterProc;


    internal sealed class ServerUpdateHandler
    {
        //--------------------------------------------------------------------------

        #region Private statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        private static void OnRestartRequest(IpcClientContext client, XPacket packet)
        {
            int count = packet.ReadValue<int>();
            for (int i = 0; i < count; i++)
            {
                ServerType serverType = (ServerType)packet.ReadValue<int>();

                if (serverType == ServerType.Gateway)
                {
                    var gatewayConfig = PersistenceHelper.FromJson<GatewayConfigEntity>(
                       packet.ReadValue<string>()
                        );

                    Logger.Warn("Received gateway server configuration data transfer object.");

                    Logger.Info("[Gateway svc] Name = {0} Bind IP = {1} Bind Port = {2}",
                    gatewayConfig.ServerConfig.ServerName,
                    gatewayConfig.NetEngineConfig.BindAddress,
                    gatewayConfig.NetEngineConfig.BindPort);

                    gs.Service.RegisterAndStartGateway(gatewayConfig);
                }

                if (serverType == ServerType.Download)
                {
                    var downloadConfig = PersistenceHelper.FromJson<DownloadConfigEntity>(
                        packet.ReadValue<string>());

                    Logger.Warn("Received download server configuration data transfer object.");

                    Logger.Info("[Download svc] Name = {0} Bind IP = {1} Bind Port = {2}",
                    downloadConfig.ServerConfig.ServerName,
                    downloadConfig.NetEngineConfig.BindAddress,
                    downloadConfig.NetEngineConfig.BindPort);

                    gs.Service.RegisterAndStartDownload(downloadConfig);
                }

                if (serverType == ServerType.Agent)
                {
                    var agentConfig = PersistenceHelper.FromJson<AgentConfigEntity>(
                        packet.ReadValue<string>()
                        );


                    Logger.Warn("Received agent server configuration data transfer object.");

                    Logger.Info("[Agent svc] Name = {0} Bind IP = {1} Bind Port = {2}",
                        agentConfig.ServerConfig.ServerName,
                        agentConfig.NetEngineConfig.BindAddress,
                        agentConfig.NetEngineConfig.BindPort);

                    gs.Service.RegisterAndStartAgent(agentConfig);
                }
            }
        }

        public static void Execute(IpcClientContext client, XPacket packet)
        {
            IpcSlaveUpdateOp op = (IpcSlaveUpdateOp)packet.ReadValue<int>();

            switch(op)
            {
                case IpcSlaveUpdateOp.StartOrRestart:
                    {
                        OnRestartRequest(client, packet);
                    }
                    break;

                default:
                    {
                        Logger.Error($"Unknown IpcSlaveUpdateOp.");
                    }
                    break;
            }
           
        }
        #endregion

        //--------------------------------------------------------------------------
    }
}
