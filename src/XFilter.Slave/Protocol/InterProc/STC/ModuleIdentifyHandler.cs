﻿namespace XFilter.Slave.Protocol.InterProc
{
    using NLog;
    using XFilter.DataModel.Network.Ipc;
    using XFilter.Shared.Network;
    using XFilter.Slave.Services.InterProc;


    internal sealed class ModuleIdentifyHandler
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public static void Execute(IpcClientContext client, XPacket packet)
        {
            string servName = packet.ReadValue<string>();
            Logger.Info($"Module authenticated as {servName}. Starting handshake.");

            
            XPacket handshake = new XPacket((ushort)IpcOpcode.Authentication);
            handshake.WriteValue<int>(client.Settings.ServerID);
            handshake.WriteValue<string>(client.Settings.ApiKey);
            client.Send(handshake);
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
