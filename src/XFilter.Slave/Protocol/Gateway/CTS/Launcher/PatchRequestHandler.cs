﻿using NLog;
using System.Threading.Tasks;
using XFilter.DataModel.Protocol.Gateway;
using XFilter.Shared.Network;
using XFilter.Slave.Interfaces;
using XFilter.Slave.Services;
using XFilter.Slave.Services.Gateway;

namespace XFilter.Slave.Protocol.Gateway
{
    /// <summary>
    /// C->S CLIENT_GATEWAY_PATCH_REQUEST = 0x6100, Encrypted
    /// TESTME
    /// </summary>
    class PatchRequestHandler : IRelayPacketHandler
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public Task<bool> Execute(RelayClientContextBase context, XPacket packet) =>
            this.ExecuteEx(context as GatewayContext, packet);

        private Task<bool> ExecuteEx(GatewayContext context, XPacket packet)
        {
            var entity = new PatchRequestEntity();
            entity.ReadFromPacket(packet);

            Logger.Debug("Entity handled.");

            context.Send(PacketSource.Client, entity.WriteToPacket());
            return Task.FromResult<bool>(true);
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
