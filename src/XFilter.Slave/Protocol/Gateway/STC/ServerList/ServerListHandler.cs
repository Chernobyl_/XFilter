﻿using NLog;
using System.Threading.Tasks;
using XFilter.DataModel.Protocol.Gateway;
using XFilter.Shared.Network;
using XFilter.Slave.Interfaces;
using XFilter.Slave.Services;
using XFilter.Slave.Services.Gateway;

namespace XFilter.Slave.Protocol.Gateway
{
    /// <summary>
    /// S->C SERVER_GATEWAY_SHARD_LIST_RESPONSE = 0xA101
    /// TESTME
    /// </summary>
    class ShardListResponseHandler : IRelayPacketHandler
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public Task<bool> Execute(RelayClientContextBase context, XPacket packet) =>
            this.ExecuteEx(context as GatewayContext, packet);

        private Task<bool> ExecuteEx(GatewayContext context, XPacket packet)
        {
            var item = new ServerListEntity();
            item.ReadFromPacket(packet);

            Logger.Debug("Entity handled.");

            context.Send(PacketSource.Server, item.WriteToPacket());
            return Task.FromResult<bool>(true);
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
