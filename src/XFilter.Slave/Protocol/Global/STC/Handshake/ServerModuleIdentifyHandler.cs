﻿using NLog;
using System.Threading.Tasks;
using XFilter.DataModel.Protocol.Global;
using XFilter.Shared.Network;
using XFilter.Slave.Interfaces;
using XFilter.Slave.Services;


namespace XFilter.Slave.Protocol.Global
{
    /// <summary>
    /// GLOBAL_IDENTIFICATION = 0x2001
    /// TESTME
    /// </summary>
    class ServerModuleIdentifyHandler : IRelayPacketHandler
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public Task<bool> Execute(RelayClientContextBase context, XPacket packet) =>
            this.ExecuteEx(context, packet);

        private Task<bool> ExecuteEx(RelayClientContextBase context, XPacket packet)
        {
            var item = new ModuleIdentifyEntity();
            item.ReadFromPacket(packet);

            Logger.Debug("Entity handled.");

            context.Send(PacketSource.Server, item.WriteToPacket());
            return Task.FromResult<bool>(true);
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
