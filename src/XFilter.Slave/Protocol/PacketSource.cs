﻿namespace XFilter.Slave.Protocol
{
    //--------------------------------------------------------------------------

    /// <summary>
    /// The packet source.
    /// </summary>
    public enum PacketSource
    {
        Client,
        Server
    }

    //--------------------------------------------------------------------------
}
