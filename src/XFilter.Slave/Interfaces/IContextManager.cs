﻿using System.Net.Sockets;
using XFilter.Shared.Network;
using XFilter.Slave.Protocol;
using XFilter.Slave.Services;

namespace XFilter.Slave.Interfaces
{
    /// <summary>
    /// The context manager interface.
    /// </summary>
    public interface IContextManager<T> where T : IRelayClientContext
    {
        /// <summary>
        /// Gets the count of currently connected client contexts.
        /// </summary>
        int Count { get; }

        /// <summary>
        /// Enqueues startup of session for given socket.
        /// </summary>
        /// <param name="client">The client socket.</param>
        void EnqueueSessionStartup(Socket client);

        /// <summary>
        /// Starts the client context.
        /// </summary>
        /// <param name="client"></param>
        void StartContext(object client);

        /// <summary>
        /// Stops the client context.
        /// </summary>
        /// <param name="context"></param>
        void StopContext(T context);

        /// <summary>
        /// Checks if the server-wide connection limit is reached.
        /// </summary>
        /// <returns></returns>
        bool MaxConnectionsReached();

        /// <summary>
        /// Checks if the per-ip connection limit is reached for given client socket (address).
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        bool MaxConnectionsPerIpReached(Socket client);

        /// <summary>
        /// Sends packet to all currently connected user contexts.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="packet"></param>
        void BroadcastPacket(PacketSource source, XPacket packet);

        /// <summary>
        /// Stops all client contexts.
        /// </summary>
        void StopAllContexts();
    }
}
