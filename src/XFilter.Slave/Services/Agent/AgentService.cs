﻿using NLog;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using XFilter.DataModel.DTO.Entities;
using XFilter.DataModel.Protocol;
using XFilter.Shared.Network;
using XFilter.Slave.GameLogic.Agent.Chat;
using XFilter.Slave.GameLogic.Agent.Enchancement;
using XFilter.Slave.GameLogic.Agent.FRPVP;
using XFilter.Slave.GameLogic.Agent.Logout;
using XFilter.Slave.GameLogic.Agent.Movement;
using XFilter.Slave.GameLogic.Agent.Stall;
using XFilter.Slave.GameLogic.Agent.Timed;
using XFilter.Slave.Protocol;
using XFilter.Slave.Protocol.Agent;
using XFilter.Slave.Protocol.Global;
using XFilter.Slave.Services.Core;

namespace XFilter.Slave.Services.Agent
{
    internal sealed class AgentService : ServiceBase
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private Task _windowTopUpdaterTask;
        private CancellationTokenSource _cts;
        private CancellationToken _ct;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties / managers

        public AgentConfigEntity Configuration
        { get; private set; }

        public RelayPacketDispatcher<AgentContext> PacketDispatcher
        { get; private set; }

        public AgentContextManager ContextManager
        { get; private set; }


        public SplitPacketManager SplitPacketManager
        { get; private set; }

        public TimedEventManager TimedEventManager
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Game managers

        public LogoutManager LogoutManager
        { get; private set; }

        public ChatManager ChatManager
        { get; private set; }

        public StallManager StallManager
        { get; private set; }

        public FreePvpManager FreePvpManager
        { get; private set; }

        public MovementManager MovementManager
        { get; private set; }

        public AlchemyManager AlchemyManager
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public AgentService()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        //--------------------------------------------------------------------------

        void InfoReporterTaskWorker()
        {
            while (true)
            {
                if (_cts.IsCancellationRequested)
                    break;

                string str = string.Format("Pool usage:  [Available: {0} | Max: {1} | Peek: {2} | Users {3} | Processed {4} Overlap {5} Running {6}]",
                    gs.Service.BufferManager.ObjectsAvailable,
                    gs.Service.BufferManager.MaxObjects,
                    gs.Service.BufferManager.PeekObjCount,
                    this.ContextManager.Count,
                    this.PacketDispatcher.ProcessedCount,
                    this.PacketDispatcher.OverlapCount,
                    this.PacketDispatcher.RunningWorkCount);
                Console.Title = str;
                System.Threading.Thread.Sleep(100);
            }
        }

        //--------------------------------------------------------------------------

        protected override void SetupPacketHandlers()
        {
            //--------------------------------------------------------------------------

            #region Client -> Server

            //Client -> Server

            this.PacketDispatcher.SetHandler(
                PacketSource.Client,
                GameOpcode.CLIENT_PING_REQUEST,
                new PingRequestHandler());

            this.PacketDispatcher.SetHandler(
                  PacketSource.Client, 
                  GameOpcode.CLIENT_AGENT_AUTH_REQUEST,
                new AuthRequestHandler());

            this.PacketDispatcher.SetHandler(
                  PacketSource.Client, 
                  GameOpcode.CLIENT_AGENT_CHAT_REQUEST,
                new ChatRequestHandler());

            this.PacketDispatcher.SetHandler(
                  PacketSource.Client, 
                  GameOpcode.CLIENT_AGENT_CHARACTER_SELECTION_JOIN_REQUEST,
                new CharSelectionJoinRequestHandler());

            this.PacketDispatcher.SetHandler(
                  PacketSource.Client, 
                  GameOpcode.CLIENT_AGENT_CHARACTER_SELECTION_ACTION_REQUEST,
                new CharSelectionActionRequestHandler());

            this.PacketDispatcher.SetHandler(
                  PacketSource.Client, 
                  GameOpcode.CLIENT_AGENT_CHARACTER_SELECTION_RENAME_REQUEST,
                new CharSelectionRenameRequestHandler());

            this.PacketDispatcher.SetHandler(
                  PacketSource.Client, 
                  GameOpcode.CLIENT_AGENT_LOGOUT_REQUEST,
                new LogoutRequestHandler());

            this.PacketDispatcher.SetHandler(
                  PacketSource.Client, 
                  GameOpcode.CLIENT_AGENT_LOGOUT_CANCEL_REQUEST,
                new LogoutCancelRequestHandler());

            this.PacketDispatcher.SetHandler(
                  PacketSource.Client, 
                  GameOpcode.CLIENT_AGENT_GUIDE_REQUEST,
                new GuideRequestHandler());

            this.PacketDispatcher.SetHandler(
                  PacketSource.Client, 
                  GameOpcode.CLIENT_AGENT_FRPVP_UPDATE_REQUEST,
                new FRPVPRequestHandler());

            this.PacketDispatcher.SetHandler(
                  PacketSource.Client, 
                  GameOpcode.CLIENT_AGENT_STALL_CREATE_REQUEST,
                new StallCreateRequestHandler());

            this.PacketDispatcher.SetHandler(
                  PacketSource.Client, 
                  GameOpcode.CLIENT_AGENT_STALL_UPDATE_REQUEST,
                new StallUpdateRequestHandler());

            this.PacketDispatcher.SetHandler(
                  PacketSource.Client, 
                  GameOpcode.CLIENT_AGENT_STALL_DESTROY_REQUEST,
                new StallDestroyRequestHandler());

            this.PacketDispatcher.SetHandler(
                  PacketSource.Client, 
                  GameOpcode.CLIENT_AGENT_STALL_LEAVE_REQUEST,
                new StallLeaveRequestHandler());

            this.PacketDispatcher.SetHandler(
                  PacketSource.Client, 
                  GameOpcode.CLIENT_AGENT_STALL_TALK_REQUEST,
                new StallTalkRequestHandler());

            this.PacketDispatcher.SetHandler(
                  PacketSource.Client, 
                  GameOpcode.CLIENT_AGENT_STALL_BUY_REQUEST,
                new StallBuyRequestHandler());

            this.PacketDispatcher.SetHandler(
                  PacketSource.Client, 
                  GameOpcode.CLIENT_AGENT_MOVEMENT_REQUEST,
                new MovementRequestHandler());

            this.PacketDispatcher.SetHandler(
                  PacketSource.Client, 
                  GameOpcode.CLIENT_AGENT_ENFORCEMENT_REINFORCE_REQUEST,
                new AlchemyReinforceRequestHandler());
            
            this.PacketDispatcher.SetHandler(
                  PacketSource.Client, 
                  GameOpcode.CLIENT_AGENT_ENFORCEMENT_MAGIC_OPTION_GRANT_REQUEST,
                new MagicOptionGrantRequestHandler());

            this.PacketDispatcher.SetHandler(
                  PacketSource.Client, 
                  GameOpcode.CLIENT_AGENT_OBJECT_ACTION_REQUEST,
                new ObjectActionRequestHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Client, 
                GameOpcode.CLIENT_AGENT_MOVEMENT_DESIGNATE_AS_RECALL_POINT_REQUEST,
                new DesignateRecallPointRequestHandler());

            this.PacketDispatcher.SetHandler(
                  PacketSource.Client, 
                  GameOpcode.CLIENT_AGENT_OBJECT_INTERACTION_START_REQUEST,
                new ObjectInteractionStartRequestHandler());

            this.PacketDispatcher.SetHandler(
                  PacketSource.Client, 
                  GameOpcode.CLIENT_AGENT_FRIEND_ADD_REQUEST,
                new FriendAddRequestHandler());

            this.PacketDispatcher.SetHandler(
                  PacketSource.Client, 
                  GameOpcode.CLIENT_AGENT_OBJECT_INTERACTION_END_REQUEST,
                new ObjectInteractionEndRequestHandler());

            this.PacketDispatcher.SetHandler(
                  PacketSource.Client, 
                  GameOpcode.CLIENT_AGENT_MOVEMENT_TELEPORT_REQUEST,
                new TeleportRequestHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Client,
                GameOpcode.CLIENT_AGENT_GUILD_UPDATE_NOTICE,
                new GuildUpdateNoticeRequestHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Client, 
                GameOpcode.CLIENT_AGENT_PARTY_MATCHING_REQUEST,
                new PartyMatchingRequestHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Client,
                GameOpcode.CLIENT_AGENT_XTRAP_CHALLENGE_RESPONSE,
                new XtrapChallengeResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Client,
                GameOpcode.CLIENT_AGENT_PARTY_CREATE_REQUEST,
                new PartyCreateRequestHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Client,
                GameOpcode.CLIENT_AGENT_PARTY_LEAVE_REQUEST,
                new PartyLeaveRequestHandler());

            this.PacketDispatcher.SetHandler(
              PacketSource.Client,
              GameOpcode.CLIENT_AGENT_PARTY_KICK_REQUEST,
              new PartyKickRequestHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Client,
                GameOpcode.CLIENT_AGENT_PARTY_MATCHING_JOIN_REQUEST,
                new PartyMatchingJoinRequestHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Client,
                GameOpcode.CLIENT_AGENT_PARTY_MATCHING_DISBAND_REQUEST,
                new PartyMatchingDisbandRequestHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Client,
                GameOpcode.CLIENT_AGENT_PARTY_MATCHING_MODIFY_REQUEST,
                new PartyMatchingModifyRequestHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Client,
                GameOpcode.CLIENT_AGENT_PARTY_INVITE_REQUEST,
                new PartyInviteRequestHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Client,
                GameOpcode.SERVER_AGENT_EXCHANGE_START,
                new ExchangeStartRequestHandler());


            //NOTE: IMPORTANT: This causes server to give C10, no idea why tho.
            /*
            this.PacketDispatcher.SetHandler(
                 PacketSource.Client,
                 GameOpcode.GLOBAL_MODULE_IDENTIFY,
                 new ClientModuleIdentifyHandler());
            */

            #endregion

            //--------------------------------------------------------------------------

            #region Server -> Client

            //Server -> Client
            this.PacketDispatcher.SetHandler(
                  PacketSource.Server, 
                  GameOpcode.SERVER_AGENT_CHAT_UPDATE,
                new ChatUpdateHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server,
                GameOpcode.SERVER_AGENT_CHARACTER_SELECTION_RENAME_RESPONSE,
                new CharSelectionRenameResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_CHARACTER_SELECTION_JOIN_RESPONSE,
                new CharSelectionJoinResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_AUTH_RESPONSE,
                new AuthResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_CHAT_RESPONSE,
                new ChatResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_LOGOUT_SUCCESS,
                new LogoutSuccessHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_LOGOUT_RESPONSE,
                new LogoutResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_LOGOUT_CANCEL_RESPONSE,
                new LogoutCancelResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_GUIDE_RESPONSE,
                new GuideResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_ENVIRONMENT_WEATHER_UPDATE,
                new WeatherUpdateHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_ENVIRONMENT_CELESTIAL_UPDATE,
                new WorldCelestialUpdateHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_ENVIRONMENT_CELESTIAL_POSITION,
                new WorldCelestialPositionHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_PK_UPDATE_PENALTY,
                new PkPenaltyPointUpdateHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_PK_UPDATE_DAILY,
                new PkPenaltyDailyUpdateHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_PK_UPDATE_LEVEL,
                new PkPenaltyUpdateLevelHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_CHARACTER_SELECTION_RESPONSE,
                new CharSelectionActionResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_FRPVP_UPDATE_RESPONSE,
                new FRPVPResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_STALL_CREATE_RESPONSE,
                new StallCreateResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_STALL_DESTROY_RESPONSE,
                new StallDestroyResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_STALL_LEAVE_RESPONSE,
                new StallLeaveResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_STALL_TALK_RESPONSE,
                new StallTalkResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_STALL_BUY_RESPONSE,
                new StallBuyResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_UNION_INFO_RESPONSE,
                new UnionInfoResponseHandler());

            /* TODO: Bugged, permission change doesent work ... Need to re-parse.
            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_GUILD_UPDATE_RESPONSE,
                new GuildUpdateResponseHandler());
           */
            // this.PacketDispatcher.SetHandler(GameOpcode.SERVER_AGENT_STALL_UPDATE_RESPONSE,
            //    new StallUpdateResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server,
                GameOpcode.SERVER_AGENT_GUILD_UPDATE_NOTICE,
                new GuildNoticeUpdateResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_MOVEMENT_RESPONSE,
                new MovementResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_MOVEMENT_DESIGNATE_AS_RECALL_POINT_RESPONSE,
                new DesignateRecallPointResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_ALCHEMY_REINFORCE_RESPONSE,
                new AlchemyReinforceResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_OBJECT_INTERACTION_END_RESPONSE,
                new ObjectInteractionEndResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server,
                GameOpcode.SERVER_AGENT_FRIEND_ADD_REQUEST_RESPONSE,
                new FriendAddRequestResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_FRIEND_ADD_RESULT_RESPONSE,
                new FriendAddResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server,
                GameOpcode.SERVER_AGENT_XTRAP_CHALLENGE,
                new XtrapChallengeHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server,
                GameOpcode.GLOBAL_MODULE_IDENTIFY,
                new ServerModuleIdentifyHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server,
                GameOpcode.SERVER_AGENT_PARTY_MATCHING_DISBAND_RESPONSE,
                new PartyMatchingDisbandResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server,
                GameOpcode.SERVER_AGENT_PARTY_MATCHING_MODIFY_RESPONSE,
                new PartyMatchingModifyResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server,
                GameOpcode.SERVER_AGENT_PARTY_MATCHING_PAGE_RESPONSE,
                new PartyMatchingPageResponseHandler());

         
      

            #endregion

            //--------------------------------------------------------------------------

            #region Server -> Client split

            //Only end has to be handled
            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_AGENT_GUILD_INFO_END,
                new GuildInfoResponseHandler());

            #endregion

            //--------------------------------------------------------------------------

            Logger.Info($"Handler count: {this.PacketDispatcher.HandlerCount}.");
        }

        //--------------------------------------------------------------------------


        /// <summary>
        /// TODO: Verify config
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Start(AgentConfigEntity entity)
        {
            _cts = new CancellationTokenSource();
            _ct = _cts.Token;

            

            //Setup engine managers
            this.Configuration = entity;
            this.PacketDispatcher = new RelayPacketDispatcher<AgentContext>(this.Configuration.NetEngineConfig);
            this.PacketDispatcher.Initialize();
            this.ContextManager = new AgentContextManager(this);
            this.ContextManager.Initialize();
            this.TimedEventManager = new TimedEventManager(this);
            this.TimedEventManager.Initialize();


            //Setup game managers
            this.LogoutManager = new LogoutManager(this);
            this.StallManager = new StallManager(this);
            this.MovementManager = new MovementManager(this);
            this.AlchemyManager = new AlchemyManager(this);
            this.ChatManager = new ChatManager(this);
            this.RefServiceID = this.Configuration.ServerConfig.ID;

            SetupPacketHandlers();

            var netCfg = this.Configuration.NetEngineConfig;
            this.SocketListener = new SocketListener(new IPEndPoint(
                IPAddress.Parse(netCfg.BindAddress), netCfg.BindPort), 200, true);

            bool sockInitialized = base.SocketListener.Start();

            if (!sockInitialized)
            {
                Logger.Fatal("Could not initialize socket listener. Agent service wont run.");
                return false;
            }

            base.SocketListener.CreateNewSessions = true;

            Logger.Debug("Agent server is ready for relay service.");

            this.IsRunning = true;

            _windowTopUpdaterTask = Task.Run(() => InfoReporterTaskWorker());

            return true;
        }

        public void Stop()
        {
            Logger.Warn("Agent service is shutting down.");

            this.SocketListener.Deinitialize();
            Logger.Warn("Socket listener stopped.");

            this.ContextManager.Deinitialize();
            Logger.Warn("Context manager deinitialized.");

            this.PacketDispatcher.Deinitialize();
            Logger.Warn("Packet dispatcher deinitialized.");

            this.TimedEventManager.Deinitialize();
            Logger.Warn("Timed event manager deinitialized.");

            Logger.Warn("Agent service has been shut down.");

            this.IsRunning = false;

            _cts.Cancel();
        }

     

        #endregion
      
        //--------------------------------------------------------------------------
    }
}
