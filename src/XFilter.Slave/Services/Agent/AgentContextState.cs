﻿using System;
using XFilter.DataModel.Protocol.Agent;


namespace XFilter.Slave.Services.Agent
{
    public sealed class AgentContextState
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private DateTime _lastLogoutTime;
        private DateTime _lastStallTime;

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public uint UniqueId
        { get; set; }

        public uint UniqueHorseId
        { get; set; }

        public string IpAddress
        { get; set; }

        public int RefServiceID
        { get; private set; }

        public GamePosition Position
        { get; private set; }

        public bool CanMove
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Public managers & API

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// The agent context state ctor.
        /// </summary>
        public AgentContextState(int refServiceId, string ipStr)
        {
            _lastLogoutTime = DateTime.MinValue;
            _lastStallTime = DateTime.MinValue;

            this.RefServiceID = refServiceId;
            this.IpAddress = ipStr;
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Helper methods

        /// <summary>
        /// Has to be called once user tries to log out.
        /// </summary>
        public void MeterLogoutTime() => _lastLogoutTime = DateTime.Now;

        /// <summary>
        /// Has to be called once user tries to create a stall.
        /// </summary>
        public void MeterStallCreateTime() => _lastStallTime = DateTime.Now;

        /// <summary>
        /// Count of milliseconds since last logout attempt.
        /// </summary>
        /// <returns></returns>
        public int MsSinceLastLogoutAttempt()
        {
            //(DateTime.Now - _lastLogoutTime).TotalMilliseconds;
            if (!this.IsLogoutTimeSet)
                _lastLogoutTime = DateTime.Now;
            return (int)(DateTime.Now - _lastLogoutTime).TotalMilliseconds;
        }

        public int MsSinceLastStallCreateAttempt()
        {
            if (!IsStallTimeSet)
                _lastStallTime = DateTime.Now;
            return (int)(DateTime.Now - _lastStallTime).TotalMilliseconds;
        }

        public bool IsLogoutTimeSet => (_lastLogoutTime != DateTime.MinValue);
        public bool IsStallTimeSet => (_lastStallTime != DateTime.MinValue);

        public void SetPosition(GamePosition position) => this.Position = position;

        public void SetMoveState(bool canMove) => this.CanMove = canMove;

        #endregion

        //--------------------------------------------------------------------------
    }
}
