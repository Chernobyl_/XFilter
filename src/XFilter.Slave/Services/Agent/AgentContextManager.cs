﻿namespace XFilter.Slave.Services.Agent
{
    using NLog;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Sockets;
    using System.Threading;
    using System.Threading.Tasks;
    using XFilter.Shared.Helpers;
    using XFilter.Shared.Network;
    using XFilter.Slave.Protocol;


    /// <summary>
    /// The agent service user context manager.
    /// </summary>
    class AgentContextManager : IContextManager
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private CancellationTokenSource _cts;
        private CancellationToken _ct;
        private Task _pollTask;


        private List<AgentContext> _contexts;
        private AgentService _service;

        private readonly object _lock = new object();
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public AgentContextManager(AgentService service)
        {
            _service = service;
            _contexts = new List<AgentContext>();

        }

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public int Count => _contexts.Count;

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public void EnqueueSessionStartup(Socket client) => StartContext(client);

        public void StartContext(object sockObj)
        {
            var client = sockObj as Socket;

            if (MaxConnectionsReached())
            {
                //Logger.Debug("Connection limit reached.");
                return;
            }

            if (MaxConnectionsPerIpReached(client))
            {
                //Logger.Debug("Connection limit per IP reached.");
                return;
            }

            var relayCfg = _service.Configuration.AgentConfig;
            var netCfg = _service.Configuration.NetEngineConfig;


            try
            {
                lock (_lock)
                {
                    var context = new AgentContext(client, _service);

                    bool started = context.Start(relayCfg.ModuleAddress, relayCfg.ModulePort, netCfg.ConnTimeout);

                   
                    //So we are able to clean up!
                    _contexts.Add(context);
                }
            }
            catch
            {
                Logger.Warn("Error starting user context (exception).");
            }
        }

        public void Initialize()
        {
            _cts = new CancellationTokenSource();
            _ct = _cts.Token;
            _pollTask = Task.Run(() => PollWorker());
        }

        private void CheckContextActivity()
        {
            lock(_lock)
            {
                var toDrop = new List<AgentContext>();
                foreach(var context in _contexts)
                {
                    if (context.TimeSinceLastPulse > _service.Configuration.NetEngineConfig.ContextPulseTimeout)
                    {
                        toDrop.Add(context);
                    }
                }

                foreach(var context in toDrop)
                {
                    StopContext(context);
                }

                toDrop.Clear();
            }
        }

        private void PollWorker()
        {
            while(true)
            {
                try
                {
                    if (_cts.IsCancellationRequested)
                        break;

                    CheckContextActivity();

                    Thread.Sleep(5000);
                }
                catch { }
            }
        }

        public void Deinitialize()
        {
            this.StopAllContexts();
            _cts.Cancel();
        }

        public void BroadcastPacket(PacketSource source, XPacket packet)
        {
            lock (_lock)
            {
                foreach (var context in _contexts)
                    context.Send(source, packet, false);
            }
        }

        public bool MaxConnectionsPerIpReached(Socket client)
        {
            string clientIp = NetHelper.GetSockIpAddrStr(client);

            int connsFromIp;

            lock (_lock)
                connsFromIp = _contexts.Count(session => session.State.IpAddress == clientIp);

            //Assert
            bool reached = (connsFromIp > _service.Configuration.NetEngineConfig.MaxConnsPerIp);
            // if (reached)
            //    Logger.Warn($"User conns over limit ! IP: [{clientIp}]. Will be dropped.");

            return reached;
        }

        public bool MaxConnectionsReached() =>
            this.Count >= _service.Configuration.NetEngineConfig.MaxConnections;



        public void StopContext(RelayClientContextBase context)
        {
            lock (_lock)
            {
                context.Stop(true);
                _contexts.Remove(context as AgentContext);
            }

            //Logger.Warn("Agent context stopped.");
        }

        public void StopAllContexts()
        {
            lock (_lock)
            {
                foreach (var context in _contexts)
                    context.Stop(true);
            }
        }


        #endregion

        //--------------------------------------------------------------------------
    }
}
