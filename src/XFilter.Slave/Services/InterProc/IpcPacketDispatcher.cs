﻿namespace XFilter.Slave.Services.InterProc
{
    using NLog;
    using System;
    using System.Collections.Generic;
    using XFilter.DataModel.Network.Ipc;
    using XFilter.Shared.Helpers;
    using XFilter.Shared.Network;


    //SSA Packet/ result
    //public delegate PacketHandlerResult PacketHandler()
    public delegate void IpcPacketHandler(IpcClientContext client, XPacket packet);

    /// <summary>
    /// The inter-process packet dispatcher.
    /// </summary>
    public class IpcPacketDispatcher
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private readonly Dictionary<IpcOpcode, IpcPacketHandler> _handlers;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();


        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public IpcPacketDispatcher()
        {
            _handlers = new Dictionary<IpcOpcode, IpcPacketHandler>();
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        private void DefaultPacketHandler(IpcClientContext client, XPacket packet)
        {
            Logger.Warn("Unknown Server -> Client 0x{0:X}, Massive {1} Encrypted {2} {3}{4}",
                packet.Opcode, packet.Massive, packet.Encrypted,
                Environment.NewLine,
                DebugHelper.HexDump(packet.GetBytes()));
        }

        public IpcPacketHandler GetHandler(ushort opcode)
        {
            if (!Enum.IsDefined(typeof(IpcOpcode), (IpcOpcode)opcode))
                return this.DefaultPacketHandler;
            return _handlers[(IpcOpcode)opcode];
        }

        public void SetHandler(IpcOpcode opcode, IpcPacketHandler handler)
            => _handlers[opcode] = handler;

        #endregion

        //--------------------------------------------------------------------------
    }
}
