﻿using NLog;
using System.Collections.Generic;
using XFilter.DataModel;
using XFilter.DataModel.Network.Ipc;
using XFilter.Shared.Collections;
using XFilter.Shared.Network;
using XFilter.Slave.Configuration;
using XFilter.Slave.Protocol.InterProc;
using XFilter.Shared.Network;

namespace XFilter.Slave.Services.InterProc
{
    /// <summary>
    /// The inter-process client class.
    /// </summary>
    public class IpcClientContext : NetClientBase
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private ByteBufferManager _bufferManager;
        private IpcPacketDispatcher _packetDispatcher;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors
        
        /// <summary>
        /// The ctor.
        /// </summary>
        /// <param name="bufferManager">The buffer manager.</param>
        /// <param name="settings">The client settings.</param>
        public IpcClientContext(ByteBufferManager bufferManager, IpcClientSettings settings)
           : base(
                 bufferManager,
                 settings.Address,
                 settings.Port,
                 settings.ReconnectionInterval,
                 settings.ConnectionTimeout,
                 settings.PingInterval)
        {
            _bufferManager = bufferManager;
            this.Settings = settings;
            _packetDispatcher = new IpcPacketDispatcher();


            _packetDispatcher.SetHandler(IpcOpcode.ModuleIdentify, ModuleIdentifyHandler.Execute);
            _packetDispatcher.SetHandler(IpcOpcode.Authentication, AuthResponseHandler.Execute);
            _packetDispatcher.SetHandler(IpcOpcode.SlaveServerUpdate, ServerUpdateHandler.Execute);

            this.State = new FarmSlaveState();

            base.StateChanged += SocketStateChangedEventHandler;
            base.DataReceived += DataReceivedEventHandler;
        }

        /// <summary>
        /// Prevent invalid obj creation ctor.
        /// </summary>
        private IpcClientContext() : base(null, null, 0, 0, 0, 0)
        {

        }
        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public IpcClientSettings Settings
        { get; private set; }


        /// <summary>
        /// Slave server state.
        /// </summary>
        public FarmSlaveState State
        { get; private set; }


        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Fired once socket operation has to be processed.
        /// </summary>
        /// <param name="obj">The socket operation.</param>
        private void SocketStateChangedEventHandler(SocketOp obj)
        {
            //Send ping after handshake
            if(obj == SocketOp.Ping && this.State.IsHandshaked)
                PingEventHandler();
        }

        /// <summary>
        /// Called once <b>SocketStateChangedEventHandler</b> detects socket ping op.
        /// </summary>
        private void PingEventHandler()
        {
            var ping = new XPacket(0x2002);
            base.Send(ping);
        }

        /// <summary>
        /// Fired once <b>SocketStateChangedEventHandler</b> detects that data is received.
        /// </summary>
        /// <param name="packets">The list of received packets.</param>
        private void DataReceivedEventHandler(List<XPacket> packets)
        {
            foreach (var packet in packets)
                _packetDispatcher.GetHandler(packet.Opcode).Invoke(this, packet);
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
