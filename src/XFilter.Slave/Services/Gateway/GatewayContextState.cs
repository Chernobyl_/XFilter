﻿namespace XFilter.Slave.Services.Gateway
{
    public sealed class GatewayContextState
    {
        public int UniqueId
        { get; set; }

        public string IpAddress
        { get; set; }

        public int RefServiceID
        { get; set; }
    }
}
