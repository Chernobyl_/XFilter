﻿namespace XFilter.Slave.Services.Gateway
{
    using NLog;
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using XFilter.DataModel.DTO.Entities;
    using XFilter.DataModel.Protocol;
    using XFilter.Slave.GameLogic.Gateway.Launcher;
    using XFilter.Slave.GameLogic.Gateway.Login;
    using XFilter.Slave.Protocol;
    using XFilter.Slave.Protocol.Gateway;
    using XFilter.Slave.Protocol.Global;


    internal sealed class GatewayService : ServiceBase
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics
        private Task _windowTopUpdaterTask;
        private CancellationTokenSource _cts;
        private CancellationToken _ct;


        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties / managers

        public GatewayConfigEntity Configuration
        { get; private set; }

        public RelayPacketDispatcher<GatewayContext> PacketDispatcher
        { get; private set; }

        public GatewayContextManager ContextManager
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Game logic managers

        public LauncherManager LauncherManager
        { get; private set; }

        public LoginManager LoginManager
        { get; private set; }

        #endregion


        //--------------------------------------------------------------------------

        #region Constructors

        public GatewayService()
        {
          
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        //--------------------------------------------------------------------------

        void InfoReporterTaskWorker()
        {
            return;
            while (true)
            {
                if (_cts.IsCancellationRequested)
                    break;

                string str = string.Format("Pool usage:  [Available: {0} | Max: {1} | Peek: {2} | Users {3}]",
                    gs.Service.BufferManager.ObjectsAvailable,
                    gs.Service.BufferManager.MaxObjects,
                    gs.Service.BufferManager.PeekObjCount,
                    this.ContextManager.Count);
                Console.Title = str;
                System.Threading.Thread.Sleep(100);
            }
        }


        protected override void SetupPacketHandlers()
        {
            //--------------------------------------------------------------------------

            #region Server -> Client

            //Server -> Client
            this.PacketDispatcher.SetHandler(
                PacketSource.Server,
                GameOpcode.SERVER_GATEWAY_LOGIN_RESPONSE,
                new LoginResponseHandler());

            this.PacketDispatcher.SetHandler(
               PacketSource.Server,
                GameOpcode.SERVER_GATEWAY_DOWNLOAD_RESPONSE, 
                new PatchServerInfoResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server,
                GameOpcode.SERVER_GATEWAY_SHARD_LIST_RESPONSE, 
                new ShardListResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_GATEWAY_CAPTCHA_CHALLENGE,
                new CaptchaChallengeHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_GATEWAY_LAUNCHER_NEWS_RESPONSE,
                new LauncherNewsResponseHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server, 
                GameOpcode.SERVER_GATEWAY_CAPTCHA_RESULT,
                new CaptchaResultHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Server,
                GameOpcode.SERVER_GATEWAY_SHARD_LIST_PING_RESPONSE,
                new ShardListPingResponseHandler());

            
            this.PacketDispatcher.SetHandler(
                PacketSource.Server,
                GameOpcode.GLOBAL_MODULE_IDENTIFY,
                new ServerModuleIdentifyHandler());
            
          
            #endregion

            //--------------------------------------------------------------------------

            #region Client -> Server

            //Client -> Server
            this.PacketDispatcher.SetHandler(
                PacketSource.Client, 
                GameOpcode.CLIENT_GATEWAY_LOGIN_REQUEST,
                new LoginRequestHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Client,
                GameOpcode.CLIENT_GATEWAY_LAUNCHER_NEWS_REQUEST,
                new LauncherNewsRequestHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Client,
                GameOpcode.CLIENT_GATEWAY_SERVERLIST_REQUEST,
                new ServerListRequestHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Client, 
                GameOpcode.CLIENT_GATEWAY_CAPTCHA_ANSWER,
                new CaptchaAnswerHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Client,
                GameOpcode.CLIENT_PING_REQUEST,
                new PingRequestHandler());

            this.PacketDispatcher.SetHandler(
                PacketSource.Client,
                GameOpcode.CLIENT_GATEWAY_SHARD_LIST_PING_REQUEST,
                new ShardListPingRequestHandler());

            //NOTE: IMPORTANT: This causes server to give C10, no idea why tho.
            /*
            this.PacketDispatcher.SetHandler(
                 PacketSource.Client,
                 GameOpcode.GLOBAL_MODULE_IDENTIFY,
                 new ClientModuleIdentifyHandler());
            */

            this.PacketDispatcher.SetHandler(
                PacketSource.Client,
                GameOpcode.CLIENT_GATEWAY_PATCH_REQUEST,
                new PatchRequestHandler());


            #endregion

            //--------------------------------------------------------------------------

            Logger.Info($"Handler count: {this.PacketDispatcher.HandlerCount}.");
        }


        /// <summary>
        /// TODO: Verify config
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Start(GatewayConfigEntity entity)
        {
            _cts = new CancellationTokenSource();
            _ct = _cts.Token;

            //Service manager
            this.Configuration = entity;
            this.PacketDispatcher = new RelayPacketDispatcher<GatewayContext>(this.Configuration.NetEngineConfig);
            this.PacketDispatcher.Initialize();
            this.ContextManager = new GatewayContextManager(this);
            this.ContextManager.Initialize();


            //Setup game logic managers
            this.LauncherManager = new LauncherManager(this);
            this.LoginManager = new LoginManager(this);
            

            this.RefServiceID = this.Configuration.ServerConfig.ID;

            SetupPacketHandlers();

            var netCfg = this.Configuration.NetEngineConfig;
            bool sockInitialized = base.SocketListener.Initialize(
                netCfg.BindAddress,
                netCfg.BindPort,
                this.ContextManager.EnqueueSessionStartup);

            if(!sockInitialized)
            {
                Logger.Fatal("Could not initialize socket listener. Gateway service wont run.");
                return false;
            }

            base.SocketListener.AcceptNewClients = true;

            Logger.Debug("Gateway server is ready for relay service.");

            this.IsRunning = true;

            _windowTopUpdaterTask = Task.Run(() => InfoReporterTaskWorker());

            return true;
        }


        public void Stop()
        {
            Logger.Warn("Gateway service is shutting down.");

            this.SocketListener.Deinitialize();
            Logger.Warn("Socket listener stopped.");

            this.PacketDispatcher.Deinitialize();
            Logger.Warn("Packet dispatcher deinitialized.");

            this.PacketDispatcher.Deinitialize();
            Logger.Warn("Packet dispatcher deinitialized.");

            Logger.Warn("Gateway service has been shut down.");

            this.IsRunning = false;
            _cts.Cancel();
        }


        #endregion

        //--------------------------------------------------------------------------
    }
}
