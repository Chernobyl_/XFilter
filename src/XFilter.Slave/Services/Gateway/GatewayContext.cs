﻿namespace XFilter.Slave.Services.Gateway
{
    using NLog;
    using System.Net.Sockets;
    using XFilter.Shared.Helpers;
    using XFilter.Slave.Protocol;


    class GatewayContext : RelayClientContextBase
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private GatewayService _service;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// The gateway user context.
        /// </summary>
        /// <param name="client">The client socket.</param>
        /// <param name="service">The service.</param>
        public GatewayContext(Socket client, GatewayService service) :
            base(client, service, service.ContextManager)
        {
            _service = service;
            this.State = new GatewayContextState();

            this.State.RefServiceID = service.RefServiceID;
            this.State.UniqueId = NetHelper.GetSockAddrHash(client);
            this.State.IpAddress = NetHelper.GetSockIpAddrStr(client);

            base.OnDataReceived = this.DataReceivedHandler;

            base.PulseActivity();
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        /// <summary>
        /// The gateway user context state.
        /// </summary>
        public GatewayContextState State
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Fired once some data is received from client or server.
        /// </summary>
        /// <param name="source">The data source.</param>
        /// <param name="data">The data source.</param>
        /// <param name="count">The count of bytes received.</param>
        private void DataReceivedHandler(PacketSource source, byte[] data, int count)
        {
            //Work queue stuff here
            var securityObj = (source == PacketSource.Client) ? _clientSecurity : _moduleSecurity;
            try
            {
                securityObj.Recv(data, 0, count);

                var packets = securityObj.TransferIncoming();
                if (packets != null)
                {
                    foreach (var packet in packets)
                    {
                            _service.PacketDispatcher.ProcessWorkItem(
                                new RelayPacketWorkItem(source, this, packet)
                                );
                    }
                }

                this.FlushToNet(source);
            }
            catch (System.Exception ex)
            {
                Logger.Warn($"DataReceivedHandler (ssa internal ex?). {ex}");
            }
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
