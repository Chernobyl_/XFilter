﻿using System;
using System.Collections.Generic;
using XFilter.DataModel.Protocol;
using XFilter.Shared.Network;
using XFilter.Slave.Protocol;


namespace XFilter.Slave.Services.Core
{
    public delegate void ProcessWorkItemHandler(RelayPacketWorkItem workItem);

    class SplitPacketManager
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static List<SplitPacketSet> _splitPackets;
        private ProcessWorkItemHandler _onEnd;
        private SplitPacketStage _stage;

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public XPacket StartPacket
        { get; private set; }

        public List<XPacket> DataPackets
        { get; private set; }

        public XPacket EndPacket
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public SplitPacketManager(ProcessWorkItemHandler onEnd)
        {
            _onEnd = onEnd;
            _stage = SplitPacketStage.Idle;
            
            this.DataPackets = new List<XPacket>();
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public static void AddSplitPacket(SplitPacketSet item) => 
            _splitPackets.Add(item);

        public static void Initialize()
        {
            _splitPackets = new List<SplitPacketSet>();
        }


        /// <summary>
        /// TODO: Handles splitted packet logic. 
        /// </summary>
        /// <param name="packet">Packet.</param>
        /// <param name="context">Client context.</param>
        /// <returns>bool if packet is handled as split</returns>
        public bool HandlePacket(XPacket packet, RelayClientContextBase context)
        {
            bool result = false;

            if (!Enum.IsDefined(typeof(GameOpcode), packet.Opcode))
                return result;

            foreach (var item in _splitPackets)
            {
                var packetGameOp = (GameOpcode)packet.Opcode;

                if(item.StartOpcode == packetGameOp)
                {
                    _stage = SplitPacketStage.StartReceived;
                    this.StartPacket = packet;
                    result = true;
                }

                if(item.DataOpcode == packetGameOp)
                {
                    _stage = SplitPacketStage.BodyReceived;
                    this.DataPackets.Add(packet);
                    result = true;
                }

                if(item.EndOpcode == packetGameOp)
                {
                    _stage = SplitPacketStage.EndReceived;
                    this.EndPacket = packet;

                    var final = new XPacket((ushort)this.EndPacket.Opcode);
                    final.WriteByteArray(this.StartPacket.GetBytes());

                    foreach(var pck in this.DataPackets)
                        final.WriteByteArray(pck.GetBytes());

                    final.WriteByteArray(this.EndPacket.GetBytes());

                    _onEnd(new RelayPacketWorkItem(PacketSource.Server, context, final));
                    result = true;
                }
            }
            return result;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
