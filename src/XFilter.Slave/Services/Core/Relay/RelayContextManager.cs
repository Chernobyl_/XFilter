﻿using NLog;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using XFilter.Shared.Network;
using XFilter.Slave.Interfaces;
using XFilter.Slave.Protocol;
using System.Threading;
using XFilter.Shared.Extensions;


namespace XFilter.Slave.Services.Core.Relay
{
    public class RelayContextManager<T> : IContextManager<T> where T: IRelayClientContext
    {
        #region Private properties & static

        private readonly ICollection<T> _contexts;
        private readonly object _lock = new object();
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Constructors

        public RelayContextManager()
        {
            _contexts = new List<T>();
        }

        #endregion

        #region IContextManager - implementation

        /// <summary>
        /// Gets the count of currently connected client contexts.
        /// </summary>
        public int Count => _contexts.Count;

        /// <summary>
        /// Enqueues startup of session for given socket.
        /// </summary>
        /// <param name="client">The client socket.</param>
        public void EnqueueSessionStartup(Socket client)
        {

        }

        /// <summary>
        /// Starts the client context.
        /// </summary>
        /// <param name="client"></param>
        public void StartContext(object client)
        {
            var sock = client as Socket;
            if(MaxConnectionsReached())
            {
                Logger.Debug("Trying to start context while server-wide max connection limit reached.");
                return;
            }

            if(MaxConnectionsPerIpReached(sock))
            {
                Logger.Debug($"Max connections per IP reached for client IP {sock.GetAddressString()}.");
                return;
            }


        }
    
        /// <summary>
        /// Stops the client context.
        /// </summary>
        /// <param name="context"></param>
        public void StopContext(T context)
        {
            try
            {
                if(Monitor.TryEnter(_lock))
                {
                    context.Stop(true);
                    
                    _contexts.Remove(context);
                    Monitor.Exit(_lock);
                }
            }
            catch { }
        }

        /// <summary>
        /// Checks if the server-wide connection limit is reached.
        /// </summary>
        /// <returns></returns>
        public bool MaxConnectionsReached()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Checks if the per-ip connection limit is reached for given client socket (address).
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        public bool MaxConnectionsPerIpReached(Socket client)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Sends packet to all currently connected user contexts.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="packet"></param>
        public void BroadcastPacket(PacketSource source, XPacket packet)
        {

        }

        /// <summary>
        /// Stops all client contexts.
        /// </summary>
        public void StopAllContexts()
        {

        }

        #endregion

    }
}
