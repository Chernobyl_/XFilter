﻿using NLog;
using System.Collections.Generic;
using System.Linq;
using XFilter.DataModel.DTO.Entities;
using XFilter.Shared.Collections;
using XFilter.Shared.Network;
using XFilter.Slave.Protocol;
using XFilter.Slave.Services.Agent;
using XFilter.Slave.Services.Gateway;

namespace XFilter.Slave.Services
{

    internal sealed class ServiceManager
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private List<GatewayService> _gatewayServices;
        private List<DownloadService> _downloadServices;
        private List<AgentService> _agentServices;


        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public ByteBufferManager BufferManager
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public ServiceManager()
        {
            _gatewayServices = new List<GatewayService>();
            _downloadServices = new List<DownloadService>();
            _agentServices = new List<AgentService>();
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties & getters

        public int GatewayCount => _gatewayServices.Count;
        public int DownloadCount => _downloadServices.Count;
        public int AgentCount => _agentServices.Count;

        public GatewayService GetGatewayByRefID(int refId) => _gatewayServices.FirstOrDefault(
            service => service.RefServiceID == refId);

        public DownloadService GetDownloadByRefID(int refId) => _downloadServices.FirstOrDefault(
            service => service.RefServiceID == refId);

        public AgentService GetAgentByRefID(int refId) => _agentServices.FirstOrDefault(
            service => service.RefServiceID == refId);

        public List<GatewayService> GetGatewayServices() => _gatewayServices;
        public List<DownloadService> GetDownloadServices() => _downloadServices;
        public List<AgentService> GetAgentServices() => _agentServices;

        #endregion

        //--------------------------------------------------------------------------

        #region Logic - basic service interaction layer
        
        /// <summary>
        /// Registers and starts gateway relay service. If its already running, it gets restarted.
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        public bool RegisterAndStartGateway(GatewayConfigEntity config)
        {
            var service = _gatewayServices.FirstOrDefault(svc => svc.RefServiceID == config.ServerConfig.ID);

            //Need to replace old service...
            if (service != null)
            {
                Logger.Warn("Service already exists and need to be replaced (updated).");
                UnregisterAndStopGateway(config.ServerConfig.ID);
            }
            else Logger.Warn("Service is not registered yet.");

            service = new GatewayService();
            bool started = service.Start(config);

            if (!started)
            {
                Logger.Fatal("Could not start gateway service.");

                //Cleanup
                service.Stop();

                return false;
            }


            Logger.Info("Gateway service started.");
            _gatewayServices.Add(service);

            return true;
        }

        /// <summary>
        /// Registers and starts download relay service. If its already running, it gets restarted.
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        public bool RegisterAndStartDownload(DownloadConfigEntity config)
        {
            var service = _downloadServices.FirstOrDefault(svc => svc.RefServiceID == config.ServerConfig.ID);

            //Need to replace old service...
            if (service != null)
            {
                Logger.Warn("Service already exists and need to be replaced (updated).");
                UnregisterAndStopDownload(config.ServerConfig.ID);
            }
            else Logger.Warn("Service is not registered yet.");

            service = new DownloadService();
            bool started = service.Start(config);

            if (!started)
            {
                Logger.Fatal("Could not start download service.");

                //Cleanup
                service.Stop();

                return false;
            }


            Logger.Info("Download service started.");
            _downloadServices.Add(service);

            return true;
        }

        /// <summary>
        /// Registers and starts agent relay service. If its already running, it gets restarted.
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        public bool RegisterAndStartAgent(AgentConfigEntity config)
        {

            var service = _agentServices.FirstOrDefault(svc => svc.RefServiceID == config.ServerConfig.ID);

            //Need to replace old service...
            if (service != null)
            {
                Logger.Warn("Service already exists and need to be replaced (updated).");
                UnregisterAndStopAgent(config.ServerConfig.ID);
            }
            else Logger.Warn("Service is not registered yet.");

            service = new AgentService();
            bool started = service.Start(config);

            if (!started)
            {
                Logger.Fatal("Could not start agent service.");

                //Cleanup
                service.Stop();

                return false;
            }


            Logger.Info("Agent service started.");
            _agentServices.Add(service);

            return true;
        }

        public void BroadcastToGatewayClients(XPacket packet)
        {
            foreach (var svc in _gatewayServices)
            {
                svc.ContextManager.BroadcastPacket(PacketSource.Server, packet);
            }
        }

        public void BroadcastToAgentClients(XPacket packet)
        {
            foreach (var svc in _agentServices)
            {
                svc.ContextManager.BroadcastPacket(PacketSource.Server, packet);
            }
        }

        public bool UnregisterAndStopGateway(int refId)
        {
            var gateway = GetGatewayByRefID(refId);
            if (gateway == null)
                return false;

            gateway.Stop();

            _gatewayServices.Remove(gateway);
            return true;
        }

        public bool UnregisterAndStopDownload(int refId)
        {
            var download = GetDownloadByRefID(refId);
            if (download == null)
                return false;

            download.Stop();

            _downloadServices.Remove(download);
            return true;
        }

        public bool UnregisterAndStopAgent(int refId)
        {
            var agent = GetAgentByRefID(refId);
            if (agent == null)
                return false;

            agent.Stop();

            _agentServices.Remove(agent);
            return false;
        }


        #endregion

        //--------------------------------------------------------------------------

        #region Logic - init

        public void Initialize()
        {
            this.BufferManager = new ByteBufferManager(4096, 0, 4, 60000, 10000);
            this.BufferManager.Initialize();
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
