﻿using NLog;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using XFilter.Shared.Helpers;
using XFilter.Shared.Interfaces;
using XFilter.Shared.Network;
using XFilter.Slave.Interfaces;
using System;

namespace XFilter.Slave.Services.Core.Relay
{
    public class RelayClientContext  : IRelayClientContext
    {
        #region Private properties & statics

        private SrSecurity _clientSecurity;
        private SrSecurity _moduleSecurity;
        private INetworkConnection _clientConnection;
        private INetworkConnection _moduleConnection;
        private IContextManager<RelayClientContext> _contextManager;
        

        private readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Constructors

        public RelayClientContext(IContextManager<RelayClientContext> contextManager)
        {
            _contextManager = contextManager;
        }

        #endregion

        #region IRelayClientContext

        /// <summary>
        /// Starts the relay connection.
        /// </summary>
        /// <param name="clientSocket">The client socket.</param>
        /// <param name="moduleEp">The module address.</param>
        /// <param name="connectionTimeout">The timeout for establishing connection to server module.</param>
        /// <returns></returns>
        public bool Start(Socket clientSocket, IPEndPoint moduleEp, int connectionTimeout)
        {
            _clientSecurity = new SrSecurity();
            _moduleSecurity = new SrSecurity();
            _clientSecurity.GenerateSecurity(true, true, true);

            //Module unreachable or timeout reached.
            Socket moduleSocket = NetHelper.ConnectWithTimeout(moduleEp, connectionTimeout);
            if (moduleSocket == null)
                return false;

            _clientConnection = new NetworkConnection(clientSocket);
            _moduleConnection = new NetworkConnection(moduleSocket);

            _clientConnection.Disconnected += (s, e) => { this.Stop(false); };
            _moduleConnection.Disconnected += (s, e) => { this.Stop(false); };

            _clientConnection.StartReceivingData();
            _moduleConnection.StartReceivingData();

            return true;
        }


        /// <summary>
        /// Stops the relay connection and fires Disconnected event.
        /// </summary>
        /// <param name="doSessionManagerCallback"><b>Must</b> be set to true if called from session manager.</param>
        public void Stop(bool calledFromSessionManager)
        {
            if(!calledFromSessionManager)
                _contextManager.StopContext(this);

            if (_clientConnection.IsConnected)
                _clientConnection.Disconnect();

            if (_moduleConnection.IsConnected)
                _moduleConnection.Disconnect();
        }

        /// <summary>
        /// Starts reciving data from player or module socket.
        /// </summary>
        /// <param name="connectionType"></param>
        /// <returns></returns>
        public Task StartReceivingFrom(RelayConnectionType connectionType) =>
            (connectionType == RelayConnectionType.Client) ? _clientConnection.StartReceivingData() : _moduleConnection.StartReceivingData();

        /// <summary>
        /// Starts sending packet(s) to player or module socket.
        /// </summary>
        /// <param name="connectionType">The target connection to send data to.</param>
        /// <param name="packets">The packet(s).</param>
        public void StartSendingTo(RelayConnectionType connectionType, params XPacket[] packets)
        {
            SrSecurity securityObject =
                (connectionType == RelayConnectionType.Client) ? _clientSecurity : _moduleSecurity;
            INetworkConnection targetConnection =
                (connectionType == RelayConnectionType.Client) ? _moduleConnection : _clientConnection;

            foreach (var packet in packets)
                securityObject.Send(packet);

            var outgoing = securityObject.TransferOutgoing();
            if (outgoing == null)
                return;

            
            foreach(var buffer in outgoing)
                targetConnection.StartSendingData(buffer.Key.Buffer, buffer.Key.Offset, buffer.Key.Size);
        }

        #endregion

    }
}
