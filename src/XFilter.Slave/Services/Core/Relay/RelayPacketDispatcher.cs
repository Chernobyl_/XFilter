﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using XFilter.DataModel.DTO.TableObjects;
using XFilter.DataModel.Protocol;
using XFilter.Shared.Helpers;
using XFilter.Shared.Network;
using XFilter.Slave.Interfaces;
using XFilter.Slave.Protocol;


namespace XFilter.Slave.Services
{

    /// <summary>
    /// SRO relay server packet dispatcher.
    /// </summary>
    /// <typeparam name="T">Type of user context. (Gateway, Download, Agent). 
    /// See XFilter.DataModel.Models.FarmModel for enum.</typeparam>
    public sealed class RelayPacketDispatcher<T> where T: RelayClientContextBase
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private List<RelayPacketHandlerInfo> _handlers;

        private Task _workPollTask;
        private CancellationTokenSource _workPollCTS;
        private CancellationToken _workPollCT;
        private NetEngineConfigDTO _netCfg;

        //Lag detection sys.
        private List<RelayPacketWorkItem> _workRunning;

        private readonly object _pollLock = new object();

        public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// The packet dispatcher ctor.
        /// </summary>
        /// <param name="netCfg">Net engine configuration.</param>
        public RelayPacketDispatcher(NetEngineConfigDTO netCfg)
        {
            _handlers = new List<RelayPacketHandlerInfo>();
           
            _workPollCTS = new CancellationTokenSource();
            _workPollCT = _workPollCTS.Token;
            _netCfg = netCfg;

            _workRunning = new List<RelayPacketWorkItem>();

            this.ProcessedCount = 0;
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        /// <summary>
        /// Count of work items currently executing.
        /// </summary>
        public int RunningWorkCount => _workRunning.Count;

        /// <summary>
        /// Count of processed packet work items.
        /// </summary>
        public long ProcessedCount
        { get; private set; }

        /// <summary>
        /// Count of handlers that are currently "lagged".
        /// </summary>
        public int OverlapCount
        { get; private set; }

        /// <summary>
        /// Count of registered handlers.
        /// </summary>
        public int HandlerCount => _handlers.Count;

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// The default packet handler for unknown opcodes / packet sources.
        /// </summary>
        /// <param name="source">The packet source.</param>
        /// <param name="context">The user context.</param>
        /// <param name="packet"The packet.></param>
        private void DefaultPacketHandler(PacketSource source, RelayClientContextBase context, XPacket packet)
        {
            
            Logger.Warn("Unknown {0} packet 0x{1:X}, Massive {2} Encrypted {3} {4}{5}",
                source,
                packet.Opcode, packet.Massive, packet.Encrypted,
                Environment.NewLine,
                DebugHelper.HexDump(packet.GetBytes()));
            
            if (source == PacketSource.Client)
            {
                if (packet.Opcode == (ushort)GameOpcode.GLOBAL_HANDSHAKE
                    || packet.Opcode == (ushort)GameOpcode.CLIENT_HANDSHAKE_RESPONSE
                    || packet.Opcode == (ushort)GameOpcode.GLOBAL_MODULE_IDENTIFY
                    )
                {
                    context.FlushToNet(source);
                    return;
                }
            }

            if(source == PacketSource.Server)
            {
                if (packet.Opcode == (ushort)GameOpcode.GLOBAL_HANDSHAKE 
                 || packet.Opcode == (ushort)GameOpcode.CLIENT_HANDSHAKE_RESPONSE)
                {
                    context.FlushToNet(source);
                    return;
                }
            }

            context.Send(source, packet, true);
        }

        private void OverlapAndLagDetector()
        {
            while(!_workPollCTS.IsCancellationRequested)
            {
                try
                {
                    Monitor.Enter(_pollLock);

                    var toRemove = new List<RelayPacketWorkItem>();
                    foreach (var item in _workRunning)
                    {
                        if (item.ExecutionTime > 5000 && !item.IsFinished)
                        {
                            var builder = new StringBuilder();
                            builder.AppendFormat("Executing handler for opcode 0x{0:X2} aka GameOpcode {1} taking over 5 seconds. {2}",
                                item.Packet.Opcode,
                                (GameOpcode)item.Packet.Opcode,
                                Environment.NewLine);

                            Logger.Warn(builder.ToString());

                            item.IsFinished = true;
                            toRemove.Add(item);
                            ++this.OverlapCount;
                        }
                    }

                    foreach (var item in toRemove)
                        _workRunning.Remove(item);
                }
                catch (Exception ex)
                {
                    Logger.Error($"Exception: {ex}.");
                }
                finally { Monitor.Exit(_pollLock); }

                Thread.Sleep(1000);
            }
        }

        /// <summary>
        /// Starts all work processing tasks.
        /// </summary>
        public void Initialize()
        {
            _workPollTask = Task.Run(() => OverlapAndLagDetector());
        }

        /// <summary>
        /// Stops all currently working processing tasks.
        /// </summary>
        public void Deinitialize()
        {
            _workPollCTS.Cancel();
        }

        /// <summary>
        /// The actual packet/context processing callee.
        /// TODO: Serious lag/overlap detection & task restart.
        /// </summary>
        /// <param name="workItem">The work item.</param>
        public void ProcessWorkItem(RelayPacketWorkItem workItem)
        {
            workItem.OnProcessingStart();

            workItem.Packet.SeekToBeginning();
            workItem.Packet.Lock();

            lock (_pollLock)
                _workRunning.Add(workItem);

            
            
            var handler = GetHandler(workItem.Source, workItem.Packet.Opcode);

            // Logger.Debug($"Executing from thread id {Thread.CurrentThread.ManagedThreadId}");
            if (handler != null)
            {
                try
                {
                    handler.Execute(workItem.ClientContext, workItem.Packet);

                    if(workItem.Packet.RemainRead != 0)
                    {
                        Logger.Warn(
                            "{0} packet RemainRead != 0. RemainRead: {1}" +
                            "{2}Opcode: 0x{3:X}, GameOpcode: {4}, Massive: {5}, Encrypted: {6}" +
                            "{7}{8}",
                            workItem.Source, 
                            workItem.Packet.RemainRead,
                            Environment.NewLine,
                            workItem.Packet.Opcode,
                            (GameOpcode)workItem.Packet.Opcode,
                            workItem.Packet.Massive,
                            workItem.Packet.Encrypted,
                            Environment.NewLine,
                            XFilter.Shared.Helpers.DebugHelper.HexDump(workItem.Packet.GetBytes()));
                    }
                }
                catch
                {
                    Logger.Error("Failed to execute handler. Opcode: {0:X}", workItem.Packet.Opcode);
                }
            }
            else
            {
                DefaultPacketHandler(workItem.Source, workItem.ClientContext, workItem.Packet);
            }

            lock (_pollLock)
                _workRunning.Remove(workItem);

            workItem.OnProcessingEnd();

            workItem.IsFinished = true;

            ++this.ProcessedCount;

            //TODO: Do more checks here?
            //Logger.Info($"Work item process time: {workItem.ExecutionTime}");
        }

        /// <summary>
        /// Gets the appropriate handler for given opcode.
        /// </summary>
        /// <param name="opcode">The opcode.</param>
        /// <returns>Default handler if none found.</returns>
        public IRelayPacketHandler GetHandler(PacketSource source, ushort opcode)
        {

            if (!Enum.IsDefined(typeof(GameOpcode), (GameOpcode)opcode))
                return null;

            var handler = _handlers.FirstOrDefault(handlerInfo => 
            handlerInfo.Source == source &&
            (ushort)handlerInfo.Opcode == opcode);

            if(handler == null)
            {
                Logger.Debug("Programmer error. Opcode 0x{0:X}, Source: {1} is known (defined in GameOpcodes enum), but no handler assigned.",
                    opcode,
                    source);
                return null;
            }

            //return this.DefaultHandler;
            return handler.Handler;
        }

        /// <summary>
        /// Sets the handler.
        /// </summary>
        /// <param name="opcode">The opcode.</param>
        /// <param name="handler">The handler.</param>
        public void SetHandler(PacketSource source, GameOpcode opcode, IRelayPacketHandler handler)
        {
            var item = _handlers.FirstOrDefault(handlerInfo =>
                handlerInfo.Source == source && 
                handlerInfo.Opcode == opcode);

           
            for (int i = 0; i < _handlers.Count; i++)
            {
                if (_handlers[i] == item)
                {
                    _handlers[i] = new RelayPacketHandlerInfo(opcode, handler, source);
                    return;
                }
            }

            _handlers.Add(new RelayPacketHandlerInfo(opcode, handler, source));
        }
    
        #endregion

        //--------------------------------------------------------------------------
    }
}
