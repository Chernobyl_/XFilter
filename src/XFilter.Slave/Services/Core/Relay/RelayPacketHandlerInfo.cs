﻿using XFilter.DataModel.Protocol;
using XFilter.Slave.Protocol;
using XFilter.Slave.Interfaces;



namespace XFilter.Slave.Services
{
    class RelayPacketHandlerInfo
    {
        #region Public properties

        public GameOpcode Opcode
        { get; private set; }

        public IRelayPacketHandler Handler
        { get; private set; }

        public PacketSource Source
        { get; private set; }

        #endregion

        #region Constructors

        public RelayPacketHandlerInfo(GameOpcode opcode, IRelayPacketHandler handler, PacketSource source)
        {
            this.Opcode = opcode;
            this.Handler = handler;
            this.Source = source;
        }

        private RelayPacketHandlerInfo()
        {

        }

        #endregion
    }
}
