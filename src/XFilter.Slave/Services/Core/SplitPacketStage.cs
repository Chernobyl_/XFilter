﻿namespace XFilter.Slave.Services.Core
{
    public enum SplitPacketStage
    {
        Idle,
        StartReceived,
        BodyReceived,
        EndReceived,
    }
}
