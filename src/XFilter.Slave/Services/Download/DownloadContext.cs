﻿namespace XFilter.Slave.Services.Gateway
{
    using NLog;
    using System.Net.Sockets;
    using XFilter.Shared.Helpers;
    using XFilter.Slave.Protocol;


    class DownloadContext : RelayClientContextBase
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private DownloadService _service;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// The download user context.
        /// </summary>
        /// <param name="client">The client socket.</param>
        /// <param name="service">The service.</param>
        public DownloadContext(Socket client, DownloadService service) :
            base(client, service, service.ContextManager)
        {
            _service = service;
            base.OnDataReceived = this.DataReceivedHandler;

            base.PulseActivity();
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Fired once some data is received from client or server.
        /// </summary>
        /// <param name="source">The data source.</param>
        /// <param name="data">The data source.</param>
        /// <param name="count">The count of bytes received.</param>
        private void DataReceivedHandler(PacketSource source, byte[] data, int count)
        {
            //Work queue stuff here
            var securityObj = (source == PacketSource.Client) ? _clientSecurity : _moduleSecurity;
            try
            {
                securityObj.Recv(data, 0, count);

                var packets = securityObj.TransferIncoming();
                if (packets != null)
                {
                    foreach (var packet in packets)
                    {
                        _service.PacketDispatcher.ProcessWorkItem(
                            new RelayPacketWorkItem(source, this, packet)
                            );
                    }
                }

                this.FlushToNet(source);
            }
            catch (System.Exception ex)
            {
                Logger.Warn($"DataReceivedHandler (ssa internal ex?). {ex}");
            }
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
