﻿namespace XFilter.Slave.GameLogic.Agent.Stall
{
    using XFilter.DataModel.DTO.TableObjects;
    using XFilter.Slave.Services.Agent;


    class StallManager
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private AgentDelaysDTO _delays;
        private readonly AgentService _service;

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public StallManager(AgentService service)
        {
            _service = service;
            _delays = _service.Configuration.Delays;
        }

        private StallManager()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic - delays

        public bool CreationDelayEnabled => _delays.EnableStallDelay;

        /// <summary>
        /// Returns the amount of milliseconds until next stall creation is possible
        /// for user. If stall creation is possible, return 0.
        /// </summary>
        /// <param name="context">User context.</param>
        /// <returns>0 if stall creation possible, milliseconds if delay.</returns>
        public int GetTimeTillNextCreatePossible(AgentContext context)
        {
            if (!CreationDelayEnabled || _delays.StallDelay <= 0)
                return 0;

            if (!context.State.IsStallTimeSet)
                return 0;

            int sinceLastCreation = context.State.MsSinceLastStallCreateAttempt();

            if (sinceLastCreation > _delays.StallDelay)
                return 0;

            return (_delays.StallDelay - sinceLastCreation);
        }

        public string GetCreateDelayNoticeMsg(AgentContext context, int delay)
        {
            //Logout delay is not enabled. WTF?
            if (!CreationDelayEnabled)
                return null;

            return _delays.StallDelayMsg.Replace("{delay}", delay.ToString());
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
