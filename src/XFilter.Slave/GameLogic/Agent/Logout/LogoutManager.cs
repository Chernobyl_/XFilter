﻿namespace XFilter.Slave.GameLogic.Agent.Logout
{
    using XFilter.DataModel.DTO.TableObjects;
    using XFilter.Slave.Services.Agent;


    class LogoutManager
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private AgentDelaysDTO _delays;
        private readonly AgentService _service;

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public LogoutManager(AgentService service)
        {
            _service = service;
            _delays = _service.Configuration.Delays;
        }

        private LogoutManager()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic - delays

        public bool IsLogoutDelayEnabled => _delays.EnableLogoutDelay;

        /// <summary>
        /// Returns the amount of milliseconds until next logout is possible
        /// for user. If logout is possible, return 0.
        /// </summary>
        /// <param name="context">User context.</param>
        /// <returns>0 if logout possible, milliseconds if delay.</returns>
        public int GetTimeTillNextLogoutPossible(AgentContext context)
        {
            if (!IsLogoutDelayEnabled || _delays.LogoutDelay <= 0)
                return 0;

            //User did not try to log out yet - time is not set. 
            //Meaning user is allowed to logout right after he entered game.
            if (!context.State.IsLogoutTimeSet)
                return 0;

            int sinceLastLogout = context.State.MsSinceLastLogoutAttempt();

            if (sinceLastLogout > _delays.LogoutDelay)
                return 0;

            return (_delays.LogoutDelay - sinceLastLogout);
        }

        public string GetLogoutDelayNoticeMsg(AgentContext context, int delay)
        {
            //Logout delay is not enabled. WTF?
            if (!IsLogoutDelayEnabled)
                return null;

            return _delays.LogoutDelayMsg.Replace("{delay}", delay.ToString());
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
