﻿namespace XFilter.Slave.GameLogic.Agent.Timed
{
    using NLog;
    using System.Collections.Generic;
    using XFilter.DataModel.DTO.TableObjects;
    using XFilter.DataModel.Protocol.Agent;
    using XFilter.Shared.Helpers;
    using XFilter.Slave.Protocol;
    using XFilter.Slave.Services.Agent;

    /// <summary>
    /// Stuff that needs to be executed each period of time... for ex: auto notice.
    /// </summary>
    class TimedEventManager
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private readonly AgentService _service;
        private readonly TimerManager _timerManager;
        private readonly List<AutoNoticeDTO> _notices;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public TimedEventManager(AgentService service)
        {
            _service = service;
            _timerManager = new TimerManager();
            _notices = service.Configuration.AutoNotices.Notices;
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic - auto notice

        private void OnAutoNoticeTick(object noticeObj)
        {
            var notice = noticeObj as AutoNoticeDTO;
            var entity = new ChatUpdateEntity(ChatType.Notice, notice.Text);

            _service.ContextManager.BroadcastPacket(PacketSource.Server, entity.WriteToPacket());
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic - init/deinit

        public void Initialize()
        {
            if ((bool)_service.Configuration.AgentConfig.EnableAutoNotices)
            {
                foreach (var notice in _notices)
                    _timerManager.Create(() => OnAutoNoticeTick(notice), (int)notice.Interval);

                Logger.Info($"Loaded {_notices.Count} auto-notice records.");
            }
        }

        public void Deinitialize()
        {
            if ((bool)_service.Configuration.AgentConfig.EnableAutoNotices)
            {
                _timerManager.DestroyAll();
            }
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
