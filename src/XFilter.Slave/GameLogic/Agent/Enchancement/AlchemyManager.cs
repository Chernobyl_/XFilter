﻿namespace XFilter.Slave.GameLogic.Agent.Enchancement
{
    using NLog;
    using XFilter.DataModel.DTO.TableObjects;
    using XFilter.DataModel.Protocol.Agent;
    using XFilter.Slave.Services.Agent;

    class AlchemyManager
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private AgentAlchemyConfigDTO _config;
        private readonly AgentService _service;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public AlchemyManager(AgentService service)
        {
            _service = service;
            _config = _service.Configuration.AlchemyConfig;
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic


        /// <summary>
        /// Client -> server alchemy reinforcement request handler.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="request"></param>
        /// <returns>true if packet must be ignored</returns>
        public bool HandleAlchemyReinforceRequest(AgentContext context, AlchemyReinforceRequestEntity request)
        {
            if(_config.MaxPlusLimitEnabled && request.FuseType == AlchemyFuseType.Elixir)
            {
                byte itemSlot = request.GetMainItemSlot();
                //TODO: Get current item plus from DB, check against max
                Logger.Debug($"Plus item limit not implemented.");
            }

            return false;
        }

        public void HandleAlchemyReinforceResponse(AgentContext context, AlchemyReinforceResponseEntity response)
        {
            /*
            if (response.Action == AlchemyAction.Fusing)
            {
                if (_config.PlusSuccessNoticeEnabled && response.Success)
                {

                    string msg = _config.PlusSuccessNoticeMsg.
                        Replace("{charname}", context.State.CharName).
                        Replace("{item_name}", GetItemNameByRefId(response).
                        Replace("{new_item_level}", GetNextItemLevel());
                    return;
                }

                if(_config.PlusFailureNoticeEnabled && !response.Success)
                {
                    string msg = _config.PlusSuccessNoticeMsg.
                       Replace("{charname}", context.State.CharName).
                       Replace("{item_name}", GetItemNameByRefId(response).
                       Replace("{new_item_level}", GetNextItemLevel());
                    return;
                }

            }
            */
            
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
