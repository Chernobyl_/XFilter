﻿namespace XFilter.Slave.GameLogic.Agent.Movement
{
    using NLog;
    using XFilter.DataModel.Protocol.Agent;
    using XFilter.Slave.Services.Agent;


    class MovementManager
    {
        private readonly AgentService _service;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public MovementManager(AgentService service)
        {
            _service = service;
        }

        /// <summary>
        /// Fired once client sends movement request to server.
        /// </summary>
        /// <param name="context">User context.</param>
        /// <param name="request">Movement request entity.</param>
        public void HandleClientMovement(AgentContext context, MovementRequestEntity request)
        {
            /*
            Logger.Warn(
                "Unique ID {0} X: {1} Y: {2} Z: {3}",
                context.State.UniqueId,
                context.State.Position.X,
                context.State.Position.Y,
                context.State.Position.Z);*/
        }

        /// <summary>
        /// Fired when server sends msg that someone is moving to clients.
        /// </summary>
        /// <param name="context">User context.</param>
        /// <param name="response">Movement response entity.</param>
        public void HandleServerMovement(AgentContext context, MovementResponseEntity response)
        {
            if (response.HasDestination && response.TargetID == context.State.UniqueId)
            {
                context.State.SetPosition(response.DestPos);

                Logger.Warn(
                    "Unique ID {0} X: {1} Y: {2} Z: {3}",
                    context.State.UniqueId,
                    context.State.Position.X,
                    context.State.Position.Y,
                    context.State.Position.Z);
            }
            else
            {
                Logger.Warn("Movement obj id is not myself, or no destination.");
            }
        }
    }
}
