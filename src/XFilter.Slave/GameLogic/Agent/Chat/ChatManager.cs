﻿namespace XFilter.Slave.GameLogic.Agent.Chat
{
    using NLog;
    using XFilter.DataModel.Protocol.Agent;
    using XFilter.Slave.Services.Agent;


    /// <summary>
    /// TODO: Chat logger
    /// </summary>
    class ChatManager
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private bool? _logEnabled;
        private readonly AgentService _service;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public ChatManager(AgentService service)
        {
            _service = service;
            _logEnabled = _service.Configuration.AgentConfig.EnableChatLog;
        }

        #endregion

        //--------------------------------------------------------------------------

        /// <summary>
        /// Client -> Server chat request.
        /// </summary>
        /// <param name="context">The user context.</param>
        /// <param name="entity">The parsed chat entity.</param>
        /// <returns>true if packet must be ignored</returns>
        public bool HandleChatRequest(AgentContext context, ChatRequestEntity entity)
        {
            //TODO: chat caching etc, timer for flush?
            return false;
        }

    }
}
