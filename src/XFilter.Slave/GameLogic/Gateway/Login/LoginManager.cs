﻿namespace XFilter.Slave.GameLogic.Gateway.Login
{
    using NLog;
    using System.Linq;
    using XFilter.DataModel.Protocol.Gateway;
    using XFilter.Shared.Network;
    using XFilter.Slave.Protocol;
    using XFilter.Slave.Services.Gateway;

    class LoginManager
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private readonly GatewayService _service;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public LoginManager(GatewayService service)
        {
            _service = service;
        }

        private LoginManager()
        {

        }
        #endregion

        //--------------------------------------------------------------------------

        #region Logic - login

        public bool HandleLoginResponse(GatewayContext context, XPacket packet)
        {
            var entity = new LoginResultEntity();
            entity.ReadFromPacket(packet);

            //Error logging in... nothing to do here.
            if (entity.Code != LoginResultCode.Success)
            {
                context.Send(PacketSource.Server, packet);
                return true;
            }

            var allRules = gs.Service.GetGatewayByRefID(context.State.RefServiceID).Configuration.RedirectionRules;
            var ruleRec = allRules.Rules.FirstOrDefault(
                item => item.SrcAddr == entity.Address && item.SrcPort == entity.Port);

            //No redirection rule found... nothing to do here.
            if (ruleRec == null)
            {
                context.Send(PacketSource.Server, packet);
                return true;
            }

            entity.Address = ruleRec.DestAddr;
            entity.Port = (ushort)ruleRec.DestPort;

            context.Send(PacketSource.Server, entity.WriteToPacket());

            return true;
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic - Captcha

        public bool HandleCaptchaChallenge(GatewayContext context, XPacket packet)
        {
            bool autoCaptchaEnabled = gs.Service.GetGatewayByRefID(
             context.State.RefServiceID).Configuration.GatewayConfig.EnableAutoCaptcha;

            if (!autoCaptchaEnabled)
            {
                Logger.Debug("Entity handled.");

                context.Send(PacketSource.Server, packet);
                return true;
            }


            string captchaStr = gs.Service.GetGatewayByRefID(
                context.State.RefServiceID).Configuration.GatewayConfig.AutoCaptchaValue;

            var answerPacket = new CaptchaAnswerEntity(captchaStr);

            context.Send(PacketSource.Client, answerPacket.WriteToPacket());
            return true;

        }
        #endregion

        //--------------------------------------------------------------------------
    }
}
