﻿namespace XFilter.Slave
{
    using System;
    using System.IO;
    using XFilter.Shared.Helpers;
    using XFilter.Slave.Configuration;
    using XFilter.DataModel.Protocol;
    using XFilter.Slave.Services.Core;
    using XFilter.Slave.Services.InterProc;

    class Program
    {
    
        static void Main(string[] args)
        {
            SplitPacketManager.Initialize();

            SplitPacketManager.AddSplitPacket(
               new SplitPacketSet(
                   GameOpcode.SERVER_AGENT_GUILD_INFO_BEGIN,
                   GameOpcode.SERVER_AGENT_GUILD_INFO_DATA,
                   GameOpcode.SERVER_AGENT_GUILD_INFO_END
               )
               );

            /*
            SplitPacketManager.AddSplitPacket(
                new SplitPacketSet(
                    GameOpcode.SERVER_AGENT_CHARACTER_INFO_BEGIN,
                    GameOpcode.SERVER_AGENT_CHARACTER_INFO_DATA,
                    GameOpcode.SERVER_AGENT_CHARACTER_INFO_END));
            */

            gs.Service = new Services.ServiceManager();
            gs.Service.Initialize();

            IpcClientSettings ipcConfig = null;

            if (!File.Exists("slave.json"))
            {
                ipcConfig = new IpcClientSettings();
                ipcConfig.GenerateDefaults();
                PersistenceHelper.WriteFile<IpcClientSettings>(ipcConfig, "slave.json");
            }

            ipcConfig = PersistenceHelper.ReadFile<IpcClientSettings>("slave.json");
            gs.Ipc = new IpcClientContext(gs.Service.BufferManager, ipcConfig);
            gs.Ipc.Start();
            gs.Ipc.FlushOutgoing();


            while (true)
            {
                Console.ReadLine();
            }
        }
    }
}
