﻿namespace XFilter.Slave.Configuration
{
    using System.Runtime.Serialization;
    using XFilter.DataModel;


    [DataContract]
    public sealed class IpcClientSettings : NetClientSettings
    {
        //--------------------------------------------------------------------------

        #region Public properties
        [DataMember(Name = "Server ID", IsRequired = true)]
        public int ServerID
        { get; private set; }

        [DataMember(Name = "API key", IsRequired = true)]
        public string ApiKey
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public new void GenerateDefaults()
        {
            base.GenerateDefaults();
            this.ServerID = 1;
            this.ApiKey = "123123";
            
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
